@extends('cliente.layout')

@section('conteudoPainel')

<div class="container">
    <div class='row'>
    <div class="col-md-8">
<br><br><br>
<fieldset>

<!-- Form Name -->

<legend><h3>Criar Conta</h3></legend>

  @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

{{ Form::open(['role' => 'form', 'class' => 'form-horizontal','method' => 'POST', 'route' => ['cliente.store']]) }}

{{ Form::hidden('cod_tpo_pessoa', $tpoPessoa) }}


<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="email"> E-MAIL</label>  
  <div class="col-md-6">
  {{ Form::text('username', Input::old('username'),['class' => 'form-control input-md']) }} {{ $errors->first('username') }} 

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="email"> SENHA</label>  
  <div class="col-md-6">
  {{ Form::password('password',array('class' => 'form-control input-md')) }} 

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="nom_cliente"> NOME COMPLETO</label>  
  <div class="col-md-6">
  {{ Form::text('nom_cliente', Input::old('nom_cliente'),['class' => 'form-control input-md']) }} {{ $errors->first('nom_cliente') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="nom_cliente">APELIDO</label>  
  <div class="col-md-6">
  {{ Form::text('nickname', Input::old('nickname'),['class' => 'form-control input-md','placeholder' => 'Apelido é o seu nome de Usuário no site.']) }} {{ $errors->first('nickname') }} 
  </div>
</div>

@if($tpoPessoa == "1")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="nom_mae">NOME DA MÃE</label>  
  <div class="col-md-6">
  {{ Form::text('nom_mae', Input::old('nom_mae'),['class' => 'form-control input-md']) }} {{ $errors->first('nom_mae') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">RG</label>  
  <div class="col-md-6">
  {{ Form::text('num_rg', Input::old('num_rg'),['class' => 'form-control input-md']) }} {{ $errors->first('num_rg') }} 
  </div>
</div>
@endif

@if($tpoPessoa == "1")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">CPF</label>  
  <div class="col-md-6">
  {{ Form::text('num_cpf', Input::old('num_cpf'),['class' => 'form-control input-md' , 'id' => 'cpf']) }} {{ $errors->first('num_cpf') }} 
  </div>
</div>
@endif

@if($tpoPessoa == "2")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">CNPJ</label>  
  <div class="col-md-6">
  {{ Form::text('cnpj', Input::old('cnpj'),['class' => 'form-control input-md' , 'id' => 'cnpj']) }} {{ $errors->first('cnpj') }} 
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">INSCRIÇÃO ESTADUAL</label>  
  <div class="col-md-6">
  {{ Form::text('insc_estadual', Input::old('insc_estadual'),['class' => 'form-control input-md' , 'id' => 'insc_estadual']) }} 
  {{ $errors->first('insc_estadual') }} 
  </div>
</div>
@endif


<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">DATA NASCIMENTO</label>  
  <div class="col-md-6">
  {{ Form::text('dat_nasc', Input::old('dat_nasc'),['class' => 'form-control input-md' ,'id' => 'data']) }} {{ $errors->first('dat_nasc') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">ATIVIDADE PROFISSIONAL</label>  
  <div class="col-md-6">
  {{ Form::text('atv_profissional', Input::old('atv_profissional'),['class' => 'form-control input-md']) }} {{ $errors->first('atv_profissional') }} 
  </div>
</div>

@if($tpoPessoa == "2")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">NOME EMPRESA</label>  
  <div class="col-md-6">
  {{ Form::text('nom_empresa', Input::old('nom_empresa'),['class' => 'form-control input-md']) }} {{ $errors->first('nom_empresa') }} 

  </div>
</div>

@endif

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">CEP</label>  
  <div class="col-md-3">
  {{ Form::text('num_cep', Input::old('num_cep'),['class' => 'form-control input-md' , 'id' => 'num_cep']) }}
  {{ $errors->first('num_cep') }}
  </div>
  <div class="col-md-3">
      <a style="padding:7px 10px; background:#002b5e; color: #fff; font-size:12px;" href="#." id="busca_cep"><i class="fa fa-share" aria-hidden="true"></i> Buscar Endereço</a>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">ESTADO</label>  
  <div class="col-md-6">
  {{ Form::text('estado', Input::old('estado'),['class' => 'form-control input-md' , 'id' => 'estado', 'readonly' => 'readonly']) }}
  {{ $errors->first('estado') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">CIDADE</label>  
  <div class="col-md-6">
  {{ Form::text('cidade', Input::old('cidade'),['class' => 'form-control input-md' , 'id' => 'cidades', 'readonly' => 'readonly']) }}
  {{ $errors->first('cidade') }}
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">ENDEREÇO</label>  
  <div class="col-md-6">
  {{ Form::text('end', Input::old('end'),['class' => 'form-control input-md' , 'id' => 'end']) }}
  {{ $errors->first('end') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">NÚMERO</label>  
  <div class="col-md-6">
  {{ Form::text('num_end', Input::old('num_end'),['class' => 'form-control input-md']) }} {{ $errors->first('num_end') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">COMPLEMENTO</label>  
  <div class="col-md-6">
  {{ Form::text('complemento', Input::old('complemento'),['class' => 'form-control input-md']) }} {{ $errors->first('complemento') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="num_rg">BAIRRO</label>  
  <div class="col-md-6">
  {{ Form::text('bairro', Input::old('bairro'),['class' => 'form-control input-md' , 'id' => 'bairro']) }}
  {{ $errors->first('bairro') }}
  </div>
</div>




<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="cel">TELEFONE FIXO</label>  
  <div class="col-md-4">
  {{ Form::text('num_telefone', Input::old('num_telefone'),['class' => 'form-control input-md', 'id' => 'num_telefone']) }} {{ $errors->first('num_telefone') }}  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="cel">TELEFONE CELULAR</label>  
  <div class="col-md-4">
  {{ Form::text('num_celular', Input::old('num_celular'),['class' => 'form-control input-md', 'id' => 'num_celular']) }} {{ $errors->first('num_celular') }}  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="cel">ESTADO CIVIL</label>  
  <div class="col-md-4">
      {{Form::select('cod_estado_civil', array('' => 'Selecione') + $civil, Input::old('cod_estado_civil'),['class' => 'form-control'])}}
      {{ $errors->first('cod_estado_civil') }}
  </div>
</div>





<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="SALVAR"></label>
  <div class="col-md-4">
   {{ Form::submit('Salvar',['class' => 'btn btn-success']) }}
  </div>
</div>

</fieldset>
{{ Form::close() }}
    </div>
  </div>
</div>

@stop







  

@stop
