@extends('cliente.layout')
@section('conteudoPainel')

<br>


<div class="container">

@if( empty($listaAnunciosCategoria) )

<div class="alert alert-danger text-center" role="alert">
<span><h4> Desculpe, mas não temos anúncios para categoria pesquisada por você. <i class="fa fa-frown-o" aria-hidden="true"></i>
 <br>
Deseja fazer uma nova busca? <a href="/">Clique aqui.</a> </h4> </span></div>

@else

<div class="alert alert-success text-center" role="alert"><span><h4> {{ $nome_servico }}</h4> </span></div>


<div class="col-md-3">
   <div class="well"><strong><i class="fa fa-search" aria-hidden="true"></i> Busca avançada</strong>

<form method="post" id="form_busca_avancada">
  <div class="form-group">
    <label for="exampleInputEmail1">Estado</label>
    <select class="form-control" name="estado" id="estado">
      <option value=""></option>
       @foreach($estados as $estado)
          <option value="{{ $estado->cod_estado}}">{{ $estado->sigla_uf}} - {{ $estado->nome_estado }}</option>
         @endforeach
    </select>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Cidade</label>
    <select id="cidade" name="cidade" class="form-control" disabled="disabled">
    </select>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Categoria</label>
    <select class="form-control" name="categoria" id="categoria">
      <option value=""></option>
       @foreach($categoria as $categ)
          <option value="{{ $categ->cod_tipo_servico}}">{{ $categ->nome_tipo_servico}} </option>
         @endforeach
    </select>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Serviço</label>
    <select id="servico" name="servico" class="form-control" disabled="disabled">
    </select>
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Tipo</label>
    <select class="form-control" name="tipo_pessoa" id="tipo_pessoa">
      <option value=""></option>
      <option value="1">Autônomo</option>
      <option value="2">Empresa</option>
    </select>
  </div>

<!--   <div class="checkbox">
    <label>
      <input type="checkbox"> Possui referências de serviços anteriores
    </label>
  </div> -->
<br>

  <button type="reset" class="btn btn-danger">Limpar</button>
  <button type="submit" class="btn btn-success">Buscar</button>

</form>

   </div>


</div>



<!-- Carrega via ajax e da um display none no filtro_fixo -->
<div class="col-md-9" id="resultado_filtro_avancado">

</div>



<div class="col-md-9" id="filtro_fixo">

@foreach($listaAnunciosCategoria as $listaAnunciosCateg)

  <div class="well">
      <div class="media">
         <a class="pull-left" href="#">
         <img class="media-object" src="http://placekitten.com/150/150">
      </a>
      <div class="media-body">
         <h4 class="media-heading">{{ $listaAnunciosCateg->nome_anuncio }}</h4>
          <p class="text-right"> 
        <a href="{{ URL('detalhes/anuncio/'.$listaAnunciosCateg->cod_anuncio.'')}}" role="button" class="btn btn-success">
              <span class="fa fa-hand-o-right" aria-hidden="true"></span> 
              Ver detalhes
          </a></p>

          <p>{{ $listaAnunciosCateg->dsc_anuncio }}</p>
          <ul class="list-inline list-unstyled">
         <li><span><i class="fa fa-calendar"></i> {{ date('d/m/Y', strtotime($listaAnunciosCateg->created_at)) }} </span></li>
            <li>|</li>
            <span><i class="fa fa-comment"></i> 2 comentários</span>
            <li>|</li>
            <li>
               <span><i class="fa fa-eye"></i> {{ $listaAnunciosCateg->visitado }} visitas</span>
            </li>
            <li>|</li>
            <li>
            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
              <span><a href="{{ $listaAnunciosCateg->facebook }}"><i class="fa fa-facebook fa-1x"></a></i></span>
              <span><a href="{{ $listaAnunciosCateg->twitter }}"><i class="fa fa-twitter fa-1x"></i></a></span>
              <span><a href="{{ $listaAnunciosCateg->site }}"><i class="fa fa-globe fa-1x"></i></a></span>
              <span><i class="fa fa-whatsapp fa-1x"> {{ $listaAnunciosCateg->watsapp }}</i></span>
            </li>
         </ul>
       </div>
    </div>
  </div>
@endforeach

</div>
</div>

@endif

<br>

@stop



