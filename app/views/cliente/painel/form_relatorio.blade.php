@extends('cliente.layout')

@section('conteudoPainel')

<div class="container">
    <div class='row'>
      <br>
	<form class = "form-horizontal">
	  <fieldset>

   <!-- Form Name -->
   <legend>Relatórios</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif


<!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Tipo de Relatório</label>
     <div class="col-md-5">
 
 <select name="tpo_relatorio" class="form-control" required="">
 	  <option value=""> Selecione </option>
      <option value="E"> Diário de entrada </option>
      <option value="S"> Diário de saída </option>
      <option value="L"> Diário de leilões </option>
  </select>

     </div>
   </div>

<table class="table table-striped" id="tabela_relatorio">
                  <thead>
                     <tr>
                        <th>Selecionar</th>
                        <th>1º Data Final</th>
                        <th>2º Data Final</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($lotes as $lote)
                     <tr>
                        <td>
                        {{ Form::checkbox('cod_lote[]', $lote->cod_lote) }}
                       {{ $lote->titulo_lote }}
                        </td>

                        <td>{{ date('d/m/Y', strtotime($lote->dat_final_1)) }}</td>
                         <td>{{ date('d/m/Y', strtotime($lote->dat_final_2)) }}</td>

                     </tr>
                     @endforeach
                  </tbody>

               </table>

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        <!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Gerar PDF
</button>
     </div>
   </div>


   </fieldset>
</form>

<script>
  $( function() {
    $( ".datas" ).datepicker();
  } );
  </script>


  </div>
</div>


@stop
