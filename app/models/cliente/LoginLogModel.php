<?php

namespace app\models\cliente;

class LoginLogModel extends \Eloquent{

	protected $table = 'tb_login_log';
	protected $guarded = [];
	protected $primaryKey = 'cod_login_log';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    
}

