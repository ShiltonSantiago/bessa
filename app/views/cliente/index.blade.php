@extends('cliente.layout')

@section('conteudoPainel')

 <!--======= SLIDER =========-->
   <div class="section2">
      <div class="slide-principal">
         <div class="carousel fade-carousel slide" id="myCarousel" data-ride="carousel" data-interval="5000" id="bs-carousel"> 
           <!-- Indicators 
           <ol class="carousel-indicators">
             <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
             <li data-target="#myCarousel" data-slide-to="1"></li>
             <li data-target="#myCarousel" data-slide-to="2"></li>
           </ol>
       -->
           <!-- Wrapper for slides -->
           <div class="carousel-inner" role="listbox">
             <div class="item active">
               <img src="{{ URL('frameworks/cliente/images/slide1.jpg')}}"  alt="Chania">
             </div>

          
      @foreach($slides = Session::get('ListaSlides') as $slide)   
             <div class="item">
               <a href="http://www.bessaleiloes.com.br/detalhes/lote/16" target="blank"><img src="{{ URL('public/imagens/'.$slide->img_texto.'')}}"  alt=""></a>
             </div>
       @endforeach


           </div>
           <!-- Left and right controls -->
           <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
           </a>
           <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
           </a>
         </div>
      </div>
   </div>

 <!--======= LEILÕES =========-->
   <div class="section4">
      <div class="leiloes">
         <div class="container">
            <div class="col-xs-12">
               <div class="row">
                  <div class="filtro-leiloes-home">
                     <ul>
                        <li id="leilao-todos">Todos</li>
                        <li id="leilao-liberado">Liberados</li>
                        <li id="leilao-embreve">Em Breve</li>
                        <li id="leilao-suspenso">Suspensos</li>
                        <li id="leilao-encerrado">Encerrados</li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>

            <div class="row todos-leiloes">

            @foreach($lotes as $lote)
              <center>
                <h4></h4>
              </center>

               <div class="col-sm-4 col-md-3 col-xs-12 retiraleilao-{{ $lote->status->dsc_icon}}"> 
                  <div class="single-leilao-home">
                  <div class="estado-leilao-{{ $lote->status->dsc_icon}}">{{ $lote->status->dsc_status}} </div>
                  <div class="estado-leilao-icon-{{ $lote->status->dsc_icon}}">
                    <i class="fa fa-gavel" aria-hidden="true"></i>
                  </div>
                     <div class="clearfix"></div>
                     <a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}">
                        <div class="imagem-leilao" id="imagem-leilao">
                          <div class="image-destaque-lote">

                           <img class="center-block" src="{{ URL('public/imagens/'.$lote->img_lote.'')}}"  alt="">
                           </div>
                           <div class="leilao-hover">
                              <i id="flutuar-lei" class="fa fa-gavel" aria-hidden="true"></i>
                           </div>
                        </div>
                     </a>
                     <div class="title-leilao tilte-leilao-lotes">
                        <h3><a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}"><br/>
                        {{ $lote->titulo_lote}}<br><small>Nº de bens: {{ $lote->produtos->count() }}</small><br> 
                        <hr style="margin-top:10px;margin-bottom:10px;">
                        <small>
                         
                      @if($lote->ind_exibir_praca == "S")
                          1º Praça: 
                         <span style="background:#aecfe9; padding:2px 5px; border-radius:2px; color:#000;">
                         <b><big>{{ date('d/m/Y', strtotime($lote->dat_final_1)) }}</big></b> às {{ date('H:i', strtotime($lote->dat_final_1)) }}h</span>
                         <br>
                         2º Praça:  
                         
                        {{ date('d/m/Y', strtotime($lote->dat_final_2)) }} às {{ date('H:i', strtotime($lote->dat_final_2)) }}h
                      @else
                       <span style="background:#aecfe9; padding:2px 5px; border-radius:2px; color:#000;">
                          Dia: 
                          <b><big>{{ date('d/m/Y', strtotime($lote->dat_final_1)) }}</big></b> às {{ date('H:i', strtotime($lote->dat_final_1)) }}h</span>
                       @endif

                      <br/>Modalidade: <b><big>{{ $lote->modalidade->dsc_tpo_leilao }} </big></b></small>
                           </a>
                         </h3> 
                     </div>
                     <div class="botao-leilao-home">
                        <a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}" name="botao-ok" id="botao-leilao-home">Mais Detalhes</a>
                     </div>

                  </div>
               </div>
               @endforeach

            </div>

         </div>
      </div>
   </div>

   
   <!--======= ANÚNCIO BODY =========-->
   <div class="section5">
      <div class="anuncio-body">
         <div class="container">
            <div class="col-xs-12">
               <div class="row">
                  <div class="ad-body">
                     @foreach($anuncioHome = Session::get('ListaAnuncioHome') as $anuncHome)
                        <a href="{{URL('contato')}}" target="blank">
                          <img src="{{ URL('public/imagens/'.$anuncHome->img_texto.'')}}"  alt="" />
                        </a>
          @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!--======= NOTÍCIAS =========-->

   <div class="section6">
      <div class="noticias">
         <div class="leiloes">
            <div class="container">
               <div class="col-xs-12">
                  <div class="row">
                     <div class="noticias-home">
                        <h3>Notícias</h3>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="row todas-noticias">  
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  @foreach($noticiasCombo = Session::get('ListaNoticias') as $noticia)
                  <div class="col-md-4 col-sm-6 noticia-page-single">
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $noticia->cod_noticia}}" aria-expanded="false" aria-controls="{{ $noticia->cod_noticia}}">
                          <div class="imagem-noticia" id="imagem-noticia">
                                  <img src="{{ URL('public/imagens/'.$noticia->img_noticia.'')}}" alt="">
                                  <div class="noticia-hover">
                                    <i id="flutuar-not" class="fa fa-plus-square-o" aria-hidden="true"></i>
                                  </div>
                                </div>
                                <h3>
                            {{ $noticia->nom_noticia}}
                            </h3>
                          </a>
                        </h4>
                      </div>
                      <div id="{{ $noticia->cod_noticia}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                         {{ $noticia->dsc_noticia}}
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


@stop