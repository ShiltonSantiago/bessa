<?php

namespace app\controllers\cliente;
use \app\models\cliente\UserModel as UserModel;

class PasswordClienteController extends \BaseController {

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//verifica se o id que vc quer alterar a senha é o seu id meu, evitando assim que o usuario
		//altere via url

		$idUser = \Auth::user()->cod_cliente;

		if($idUser != $id){
			return \Redirect::to('/senha/'.$idUser.'/edit');
		}

		$data = ['idUser' => $idUser];

		return \View::make('cliente.painel.userPassword')->with($data);

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//recupera os dados

		$rules = [
			'atual' => 'required',
			'nova' => 'required',
			'confirmNova' =>'required'
		];

		$messages = \app\acme\MessagesAcme::Messages();

		$validator = \Validator::make(\Input::all(),$rules,$messages);

		if($validator->fails()){
			return \Redirect::back()->withErrors($validator->messages());
		}else{

			if(\Hash::check(\Input::get('atual'), \Auth::user()->password )){

				if(\Input::get('nova') == \Input::get('confirmNova')){
					
					$attributes = [
						'password' => \Hash::make(\Input::get('nova'))
					];

					$atualizado = UserModel::where('cod_cliente',$id)->update($attributes);

					if($atualizado){
						return \Redirect::back()->with('mensagem','<div class="text-success">Senha atualizada com sucesso !</div>');;
					}else{
						return \Redirect::back()->with('mensagem','<div class="text-danger">Erro ao atualizar senha !</div>');
					}

				}else{
					return \Redirect::back()
					->withInput(\Input::all())
					->with('mensagem','<div class="text-danger">As senhas não combinam</div>');
				}

			}else{
				return \Redirect::back()
				->withInput(\Input::all())
				->with('mensagem','<div class="text-danger">Digite sua senha atual novamente</div>');
			}
			

		}
	}
}

























