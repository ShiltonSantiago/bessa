<?php

namespace app\controllers\cliente;

use \app\models\cliente\EstadoModel as EstadoModel;
use \app\models\cliente\CidadeModel as CidadeModel;
use \app\models\cliente\TipoModel as TipoModel;
use \app\models\cliente\CategoriaModel as CategoriaModel;
use \app\models\cliente\NoticiaModel as NoticiaModel;
use \app\models\cliente\AnuncioModel as AnuncioModel;
use \app\models\cliente\LoteModel as LoteModel;
use \app\models\cliente\ModalidadeModel as ModalidadeModel;


class HomeController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	
		$lotes = LoteModel::where('ind_exibir', '=', 'S')
		->orderBy('cod_status','ASC')
		->orderBy('dat_final_1','ASC')
		->get();

		 $data = [ 'lotes' => $lotes ];

        return \View::make('cliente.index', $data);

	}



 //Salvar mensagem de contato
	public function getSalvarContato(){

		$attributes = \Input::all();

			$addContato = new ContatoModel($attributes);
			$add = $addContato->save();

			if($add){
				$retorno = array('msg' => 'Mensagem enviada com sucesso, aguarde resposta !','tipo' => 'salvo');
            	return $retorno;
			}else{
				$retorno = array('msg' => 'Erro ao enviar mensagem, tente novamente !','tipo' => 'erro');
            	return $retorno;
			}

			
	}


	


}
