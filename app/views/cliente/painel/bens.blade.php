@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Listagem de bens</h1>

<hr>

<a href="{{ URL('bens')}}"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>Cadastrar novo</a>

<hr>

<table class="table table-striped" id="tabela_bens">
                  <thead>
                     <tr>
                        <th>Codigo</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                         <th>Lote</th>
						 <th>Imagens</th>
						 <th>Pdf</th>
                        <th>Editar</th>
                        <th>Visualizar</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($anuncio as $anuncios)
                     <tr>
                        <td>{{ $anuncios->cod_produto}}</td>
                        <td>{{ $anuncios->nom_produto}}</td>
                        <td>{{ $anuncios->tipoLeilao->dsc_tpo_leilao}}</td>
                        <td>{{ $anuncios->lote->titulo_lote}}</td>
                        <!--<td><img width="100" height="100" src="{{ URL('public/imagens/'.$anuncios->img_produto.'')}}"></td>-->
						<td>
			              <a href="{{ URL('fotos/'.$anuncios->cod_produto.'/edit')}}">
									Nº <span class="badge">{{ $anuncios->fotos->count()}}</span>
							  </a>
			          	</td> 
						<td>
							@if(empty($anuncios->pdf_lote))
								<i class="fa fa-ban" aria-hidden="true"></i>
							@else
								<a href="{{ URL('public/imagens/'.$anuncios->pdf_lote.'')}}" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
							@endif
						</td>
                        <td>
			              <a href="{{ URL('bens/'.$anuncios->cod_produto.'/edit')}}">
			              <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
			          	</td> 
                        <td>
			              <a href="{{ URL('detalhes/anuncio/'.$anuncios->cod_produto.'')}}">
			              <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
			          	</td>
                     </tr>
                     @endforeach
                  </tbody>

               </table>


   </div>
  </div>



  

@stop
