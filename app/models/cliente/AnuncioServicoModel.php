<?php

namespace app\models\cliente;

class AnuncioServicoModel extends \Eloquent{

	protected $table = 'tb_anuncio_servico';
	protected $guarded = [];
	protected $primaryKey = 'cod_anuncio_servico';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
}

