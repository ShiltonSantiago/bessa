<?php

namespace app\controllers\cliente;

use \app\models\cliente\ServicoModel as ServicoModel;
use \app\models\cliente\AnuncioModel as AnuncioModel;
use \app\models\cliente\AnuncioServicoModel as AnuncioServicoModel;
use \app\models\cliente\MultimidiaModel as MultimidiaModel;
use \app\models\cliente\ComentarioModel as ComentarioModel;
use \app\models\cliente\EstadoModel as EstadoModel;
use \app\models\cliente\CidadeModel as CidadeModel;
use \app\models\cliente\CategoriaModel as CategoriaModel;
use \app\models\cliente\TipoModel as TipoModel;
use \app\models\cliente\StatusModel as StatusModel;
use \app\models\cliente\PagamentoModel as PagamentoModel;
use \app\models\cliente\lanceModel as lanceModel;
use \app\models\cliente\LoteModel as LoteModel;
use \app\models\cliente\BancoModel as BancoModel;
use \app\models\cliente\UserModel as UserModel;
use \app\models\cliente\FotoModel as FotoModel;
use OpenBoleto\Banco\BancoDoBrasil;
use OpenBoleto\Agente;




class AnuncioController extends \BaseController {


	//listar anuncios por categoria
	public function getListAnuncioCategoria($valor)
	{
		//categoria de servico escolhida 
		$servico = ServicoModel::where('cod_servico', '=', $valor)->get();
		$nome_servico = $servico[0]->nome_servico;

		$anuncios = \DB::table('tb_anuncio_servico as sea')
			    ->leftJoin('tb_servico as ser', 'ser.cod_servico', '=', 'sea.cod_servico')
			    ->leftJoin('tb_anuncio as anu', 'anu.cod_anuncio', '=', 'sea.cod_anuncio')
			    ->where('sea.cod_servico', '=', $valor)
     			->get();

     	//Listar estados para carregar combos
			$estados = EstadoModel::orderBy('nome_estado')->get();

		//Listar tipos de categorias para carregar combos
			$categoria = CategoriaModel::orderBy('nome_tipo_servico')->get();

         $data = [ 'listaAnunciosCategoria' => $anuncios, 'nome_servico' => $nome_servico ,
         		   'estados' => $estados , 'categoria' => $categoria];

        return \View::make('cliente.listaAnuncioCategoria', $data);
	
		

	}


	//listar todos  leiloes do lote  selecionado
	public function getListaPorLote($valor)
	{

		$anuncio = AnuncioModel::where('cod_lote', '=', $valor)
		->where('ind_ativo', '=', "S")
		->get();		

        $data = [ 'anuncios' => $anuncio  ];

        return \View::make('cliente.listaLeiloesLote', $data);
	 
	}

	

	//listar todos dados do anuncio selecionado
	public function getDetalhesAnuncio($valor)
	{

		$anuncio = \DB::table('tb_produto as anu')
			    ->leftJoin('tb_estado as est', 'est.cod_estado', '=', 'anu.estado')
			    ->leftJoin('tb_tipo_leilao as tip', 'tip.cod_tpo_leilao', '=', 'anu.cod_tpo_leilao')
			    ->leftJoin('tb_categoria as cat', 'cat.cod_categoria', '=', 'anu.cod_categoria')
			    ->leftJoin('tb_lote as lot', 'lot.cod_lote', '=', 'anu.cod_lote')
			    ->leftJoin('tb_cidade as cid', 'cid.cod_cidade', '=', 'lot.cidade')
			    ->leftJoin('tb_forma_pagamento as pag', 'pag.cod_forma_pag', '=', 'anu.cod_forma_pag')
			    ->where('anu.cod_produto', '=', $valor)
     			->get();

     	$statusBem = AnuncioModel::where('cod_produto','=' , $valor)->get();
				
		//fotos do anuncio
			$fotos = FotoModel::where('cod_produto','=' , $valor)->get();
						
		//lista bens do lote		
		//$benslotes = \DB::table('tb_produto as anu')
			//    ->leftJoin('tb_lote as lot', 'lot.cod_lote', '=', 'anu.cod_lote')
			  //  ->leftJoin('tb_status as st', 'st.cod_status', '=', 'lot.cod_status')
			    //->where('anu.cod_lote','=',$anuncio[0]->cod_lote)
			    //->get();
		
	//lista proximos bens do lote
		$benslotes = AnuncioModel::where('cod_lote', '=', $anuncio[0]->cod_lote)
		->where('ind_ativo', '=', "S")
		->where('cod_produto','<>',$valor) //nao retorna o bem atual
		->get();
		
     	$lances = \DB::table('tb_lances as lac')
			    ->leftJoin('tb_cliente as cli', 'cli.cod_cliente', '=', 'lac.cod_cliente')
			    ->where('lac.cod_produto', '=', $valor)
     			->take(5)
     			->orderBy('valor_lance', 'desc')
     			->get();

     	$lanceAtual = lanceModel::where('cod_produto', '=', $valor)->get()->max('valor_lance');

     // se tiver lance pega o valor, se nao pega o valor inicial
     	date_default_timezone_set('America/Sao_Paulo');
		$datAtual = date('Y-m-d H:i');

     	if(empty($lanceAtual) &&  $datAtual <= $anuncio[0]->dat_final_1){
     			$valorAtual = $anuncio[0]->lan_inicial_1;
     		}elseif (empty($lanceAtual) &&  $datAtual > $anuncio[0]->dat_final_1) {
     			$valorAtual = $anuncio[0]->lan_inicial_2;
     		}else{
     			$valorAtual = $lanceAtual;
     		}


     	//Combo lance
     	$select = [];
		$incremento = $anuncio[0]->dsc_incremento;
		$valUltimo = $valorAtual;

		for ($i=0; $i < 10; $i++) {
			array_push($select, ($valUltimo + $incremento) + ($i * $incremento));

		}

        $data = [ 'anuncio' => $anuncio , 'lances' => $lances , 'lanceAtual' => $lanceAtual, 'comboLances' => $select , 
        'benslotes' => $benslotes , 'fotos' => $fotos, 'statusBem' => $statusBem];

        return \View::make('cliente.detalhesAnuncio', $data);
	 
	}
	
	//listar dados do anuncio para telao
	public function getDetalhesAnuncioTelao($valor)
	{

		$anuncio = \DB::table('tb_produto as anu')
			    ->leftJoin('tb_estado as est', 'est.cod_estado', '=', 'anu.estado')
			    ->leftJoin('tb_tipo_leilao as tip', 'tip.cod_tpo_leilao', '=', 'anu.cod_tpo_leilao')
			    ->leftJoin('tb_categoria as cat', 'cat.cod_categoria', '=', 'anu.cod_categoria')
			    ->leftJoin('tb_lote as lot', 'lot.cod_lote', '=', 'anu.cod_lote')
			    ->leftJoin('tb_cidade as cid', 'cid.cod_cidade', '=', 'lot.cidade')
			    ->leftJoin('tb_forma_pagamento as pag', 'pag.cod_forma_pag', '=', 'anu.cod_forma_pag')
			    ->where('anu.cod_produto', '=', $valor)
     			->get();
				
		//fotos do anuncio
			$fotos = FotoModel::where('cod_produto','=' , $valor)->get();
						

	//lista proximos bens do lote
		$benslotes = AnuncioModel::where('cod_lote', '=', $anuncio[0]->cod_lote)
		->where('ind_ativo', '=', "S")
		->where('cod_produto','<>',$valor) //nao retorna o bem atual
		->get();
		
     	$lances = \DB::table('tb_lances as lac')
			    ->leftJoin('tb_cliente as cli', 'cli.cod_cliente', '=', 'lac.cod_cliente')
			    ->where('lac.cod_produto', '=', $valor)
     			->take(5)
     			->orderBy('valor_lance', 'desc')
     			->get();

     	$lanceAtual = lanceModel::where('cod_produto', '=', $valor)->get()->max('valor_lance');

     // se tiver lance pega o valor, se nao pega o valor inicial
     	date_default_timezone_set('America/Sao_Paulo');
		$datAtual = date('Y-m-d H:i');

     	if(empty($lanceAtual) &&  $datAtual <= $anuncio[0]->dat_final_1){
     			$valorAtual = $anuncio[0]->lan_inicial_1;
     		}elseif (empty($lanceAtual) &&  $datAtual > $anuncio[0]->dat_final_1) {
     			$valorAtual = $anuncio[0]->lan_inicial_2;
     		}else{
     			$valorAtual = $lanceAtual;
     		}


     	//Combo lance
     	$select = [];
		$incremento = $anuncio[0]->dsc_incremento;
		$valUltimo = $valorAtual;

		for ($i=0; $i < 10; $i++) {
			array_push($select, ($valUltimo + $incremento) + ($i * $incremento));

		}

        $data = [ 'anuncio' => $anuncio , 'lances' => $lances , 'lanceAtual' => $lanceAtual, 'comboLances' => $select , 'benslotes' => $benslotes , 'fotos' => $fotos];

        return \View::make('cliente.detalhesAnuncioTelao', $data);
	 
	}

//meus lances usando distinct
	public function getListMeusLances()
	{
		$usuario = \Auth::user()->cod_cliente;


     	$lances = \DB::table('tb_lances as lac')
			    ->leftJoin('tb_produto as anu', 'anu.cod_produto', '=', 'lac.cod_produto')
			    ->where('lac.cod_cliente', '=', $usuario)
			    ->groupBy('anu.cod_produto')
			    ->orderBy('lac.created_at','desc')
     			->get();


        $data = [ 'lances' => $lances ];

        return \View::make('cliente.painel.meus_lances', $data);
	 

	}

	//listar todos anuncios de bens

	public function show()
	{
				
		$anuncio = AnuncioModel::orderBy('created_at','desc')->get();
		

		$data = [ 'anuncio' => $anuncio ];

		return \View::make('cliente.painel.bens', $data);
	}

	//form de inserir novo bem
	public function index()
	{

		$cidades = CidadeModel::orderBy('nome_cidade')->where('cod_estado', '=', '11')->get()->lists('nome_cidade','cod_cidade');
		$categoria = CategoriaModel::orderBy('dsc_categoria')->get()->lists('dsc_categoria','cod_categoria');
		$tipo = TipoModel::orderBy('dsc_tpo_leilao')->get()->lists('dsc_tpo_leilao','cod_tpo_leilao');
		$pagamento = PagamentoModel::orderBy('dsc_forma_pag')->get()->lists('dsc_forma_pag','cod_forma_pag');
		$lote = LoteModel::orderBy('titulo_lote')->get()->lists('titulo_lote','cod_lote');
		$status = StatusModel::orderBy('dsc_status')->whereIn('cod_status', array(8, 9, 10,11))->get()->lists('dsc_status','cod_status');

		$data = [ 'cidades' => $cidades, 'categoria' =>  $categoria , 'tipo' => $tipo , 
				  'pagamento' =>  $pagamento , 'lote' => $lote , 'status' => $status
				];

		return \View::make('cliente.painel.novo_bem', $data);
	}

	//salvar novo bem
	public function store()
	{
		
		$filePdf = \Input::file('pdf_lote');

			if(!empty($filePdf)){
						$destinationPath = 'public/imagens';
						$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
						$extension = $filePdf->getClientOriginalExtension();
						$fileNamePdf = rand(1111,9999).'.'.$extension;			
						$filePdf->move($destinationPath, $fileNamePdf);	
			}else{
				$fileNamePdf = "";
			}
					$dados = [
								'nom_produto' => \Input::get('nom_produto'),
								'cod_categoria' => \Input::get('cod_categoria'),
								'cod_tpo_leilao' => \Input::get('cod_tpo_leilao'),
								'cod_lote' => \Input::get('dsc_lote'),
								'lan_inicial_1' => \Input::get('lan_inicial_1'),
								'lan_inicial_2' => \Input::get('lan_inicial_2'),
								'dsc_home' => \Input::get('dsc_home'),
								'dsc_incremento' => \Input::get('dsc_incremento'),
								'pdf_lote' => $fileNamePdf,
								'num_processo' => \Input::get('num_processo'),
								'dsc_produto' => \Input::get('dsc_produto'),
								'obs_produto' => \Input::get('obs_produto'),
								'dsc_visitacao' => \Input::get('dsc_visitacao'),
								'ind_ativo' => \Input::get('ind_ativo'),
								'cod_forma_pag' => \Input::get('cod_forma_pag'),
								'cod_status' => \Input::get('cod_status')
							];

			$salvarBem = new AnuncioModel($dados);
			$add = $salvarBem->save();
			

		if($add){
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>Bem cadastrado com sucesso!</div>');
				}else{
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-error" role="alert">Erro ao salvar, tente novamente!</div>');
				}

	}



	public function edit($id)
	{
		//recupera todos os dados do anuncio
		$anuncio  = AnuncioModel::find($id);

     	//combos para setar FK

     	$estados = EstadoModel::orderBy('nome_estado')->get();

     	$cidade = CidadeModel::where('cod_estado','=', '11')->orderBy('nome_cidade')->get();

		$categoria = CategoriaModel::orderBy('dsc_categoria')->get();

		$tipo = TipoModel::orderBy('dsc_tpo_leilao')->get();

		$pagamento = PagamentoModel::orderBy('dsc_forma_pag')->get();

		$lote = LoteModel::where('ind_exibir','=', 'S')->orderBy('titulo_lote')->get();

		$status = StatusModel::orderBy('dsc_status')->whereIn('cod_status', array(8, 9, 10,11))->get();

		$data = [ 'estados' => $estados, 'categoria' =>  $categoria , 'tipo' => $tipo , 'cidade' => $cidade , 
				  'pagamento' =>  $pagamento , 'anuncio' => $anuncio , 'lote' => $lote , 'status' => $status
				];

		return \View::make('cliente.painel.EditarBem')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		//recupera foto do anuncio
		$anuncio  = AnuncioModel::find($id);

		$filePdf = \Input::file('pdf_lote');
			
	  if (empty($filePdf)){
			$fileNamePdf = $anuncio->pdf_lote;
		}else  {
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $filePdf->getClientOriginalExtension();
					$fileNamePdf = rand(1111,9999).'.'.$extension;			
					$filePdf->move($destinationPath, $fileNamePdf);
		}
			
				$dados = [
								'nom_produto' => \Input::get('nom_produto'),
								'cod_categoria' => \Input::get('cod_categoria'),
								'cod_tpo_leilao' => \Input::get('cod_tpo_leilao'),
								'cod_lote' => \Input::get('dsc_lote'),
								'lan_inicial_1' => \Input::get('lan_inicial_1'),
								'lan_inicial_2' => \Input::get('lan_inicial_2'),
								'dsc_home' => \Input::get('dsc_home'),
								'dsc_incremento' => \Input::get('dsc_incremento'),
								'pdf_lote' => $fileNamePdf,
								'num_processo' => \Input::get('num_processo'),
								'dsc_produto' => \Input::get('dsc_produto'),
								'obs_produto' => \Input::get('obs_produto'),
								'dsc_visitacao' => \Input::get('dsc_visitacao'),
								'cod_forma_pag' => \Input::get('cod_forma_pag'),
								'ind_ativo' => \Input::get('ind_ativo'),
								'cod_status' => \Input::get('cod_status')
							];

				$alterado = AnuncioModel::where('cod_produto',$id)->update($dados);
				

				if($alterado){
						return \Redirect::back()->with('mensagem','<div class="text text-success">Dados alterado com sucesso!</div>');
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			
			}

		function getValidarLances(){

		//lotes
			$lotes = LoteModel::orderBy('titulo_lote')->get();

         	$data = [ 'lotes' => $lotes ] ;

        return \View::make('cliente.painel.lances', $data);

			}
			
	public function getNotaVenda(){
		
		$dadosNota = lanceModel::where('ind_pago', '=', 'N')->get();
		
		$data = [ 'dadosNota' => $dadosNota];

		return \View::make('cliente.painel.listaNota')->with($data);
				
			}

	


}
