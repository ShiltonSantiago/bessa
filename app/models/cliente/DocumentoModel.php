<?php

namespace app\models\cliente;

class DocumentoModel extends \Eloquent{

	protected $table = 'tb_documento';
	protected $guarded = [];
	protected $primaryKey = 'cod_documento';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    

	//relacionamento tipo - 1 para 1
	public function tipoDocumento(){
		return $this->hasOne('app\models\cliente\TipoDocumentoModel', 'cod_tpo_documento', 'cod_tpo_documento');
	}
}

