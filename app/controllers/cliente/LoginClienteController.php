<?php

namespace app\controllers\cliente;

use \app\models\cliente\LoginLogModel as LoginLogModel;
use \app\models\cliente\UserModel as UserModel;
use \app\models\cliente\EstadoModel as EstadoModel;
use \app\models\cliente\CivilModel as CivilModel;



class LoginClienteController extends \BaseController {

		//pagina conta
		public function getConta($valor)
		{
			$civil = CivilModel::orderBy('dsc_estado_civil')->get()->lists('dsc_estado_civil','cod_estado_civil');

			$tpoPessoa = $valor; //1 = fisica e 2 juridica

			$data = [ 'civil' => $civil , 'tpoPessoa' => $tpoPessoa ];

			return \View::make('cliente.conta', $data);
		}

		//valida login
		public function getValidaLogin()
		{

			$login = \Input::get('email');
			$senha = \Input::get('senha');

		//valida campos obriagatorios
			if(empty($login) || empty($senha)){
				$retorno = array('msg' => 'Favor informar seu dados', 'processo' => 'obrigatorio');
	            return $retorno; //envia array pro js


			}


		//email existe na base
			$validaEmail = UserModel::where('username', $login)->count();
			if(empty($validaEmail)){
				$retorno = array('msg' => 'E-mail não existe em nossa base', 'processo' => 'email');
	            return $retorno; //envia array pro js
			}

			//tenta realizar o login
				$credentials = [
					'username' => $login,
					'password' => $senha
				];
			
				$auth = \Auth::attempt($credentials,true);


				if($auth){
						//salva log login
						$dadosLog = [ 'cod_cliente' => \Auth::user()->cod_cliente ];
						$login_log = new LoginLogModel($dadosLog);
						$login_log->save();

						$retorno = array('msg' => 'Bem Vindo', 'processo' => 'logado');
	            		return $retorno; //envia array pro js
				}else{
						$retorno = array('msg' => 'e-mail ou senha incorretos', 'processo' => 'nlogado');
	            		return $retorno; //envia array pro js
				}
			}


	//logoff
		public function destroy(){

			\Auth::logout();
			return \Redirect::to('/');
		}



		//esqueci senha
			public function getEsqueci(){

			//email do cliente
	        	$email = \Input::get("email");
	        	var_dump($email);die();

	        //valida o email em nossa base
	        	$validaEmail = UserModel::where('username', $email)->count();
				if(empty($validaEmail)){
					$retorno = array('msg' => 'E-mail não existe em nossa base', 'processo' => 'email');
	            	return $retorno; //envia array pro js
				}

	        //email de envio
	        	$emailsender = 'contato@bupes.com.br';  

	        //nova senha / criptografada
	        	$senha = rand(1111,9999);
	        	$senhaCript = [ 'password' => \Hash::make($senha) ];

	        	$alterado = UserModel::where('username',$email)->update($senhaCript);

	        //se alterou senha envia email
	        if($alterado){
	            
	            /* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
	                if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
	                elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
	                elseif(PHP_OS == "Darwin") $quebra_linha = "\n"; //Se for MAC
	                else die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

	                // Passando os dados obtidos pelo formulário para as variáveis abaixo
	                $emaildestinatario = $email;
	                $assunto           = 'Nova senha de acesso - Bupes';
	                $mensagem          = 'Prezado Cliente, <br><br> Conforme solicitado, segue sua nova senha de acesso para o site Bupes.<br><br>'
	                . '<strong>Login:</strong> '.$email.'<br>'
	                . '<strong>Senha:</strong> '.$senha.'<br><br>'
	                . '<br> Cordialmente, <br> Equipe Bupes.';


	                 //Montando a mensagem a ser enviada no corpo do e-mail. 
	                $mensagemHTML = '<img src="http://bupes.com.br/logo_oficial.png" width="100" height="50"/>
	                <p>'.$mensagem.'</p>
	                <hr>';


	                /* Montando o cabeçalho da mensagem */
	                $headers = "MIME-Version: 1.1".$quebra_linha;
	                $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
	                // Perceba que a linha acima contém "text/html", sem essa linha, a mensagem não chegará formatada.
	                $headers .= "From: ".$emailsender.$quebra_linha;
	                $headers .= "Return-Path: " . $emailsender . $quebra_linha;

	                // Note que o e-mail do remetente será usado no campo Reply-To (Responder Para)
	                $headers .= "Reply-To: ".$email.$quebra_linha;


	                /* Enviando a mensagem */
	                mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);

	                $retorno = array('msg' => 'Nova senha foi enviada para o email informado !', 'processo' => 'enviada');
	            	return $retorno; //envia array pro js

	        }else{

	            	$retorno = array('msg' => 'Erro ao enviar email com senha', 'processo' => 'email');
	            	return $retorno; //envia array pro js

	        	return \Redirect::to('/');
	        }
		}


	}
