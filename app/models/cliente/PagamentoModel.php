<?php

namespace app\models\cliente;

class PagamentoModel extends \Eloquent{

	protected $table = 'tb_forma_pagamento';
	protected $guarded = [];
	protected $primaryKey = 'cod_forma_pag';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
}

