<?php

namespace app\models\cliente;

class InformacaoModel extends \Eloquent{

	protected $table = 'tb_informacao';
	protected $guarded = [];
	protected $primaryKey = 'cod_informacao';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    
}

