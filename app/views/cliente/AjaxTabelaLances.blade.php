 <?php

// Função de porcentagem: N é N% de X
function porcentagem_nnx ( $valorOriginal ) {
   $valor = $valorOriginal; 
   $percentual = 5.0 / 100.0; // 5%
   $valor_final = $valor + ($percentual * $valor);
   $valor_final_convertido = number_format($valor_final ,2,',', '.' );

    return $valor_final_convertido;
}
?>


 @if( empty($lances) )

                     <h5><span class="label label-danger">Item ainda sem lances</span></h5>

                  @else

                  <table class="table table-striped">
                  <thead>
                     <tr>
                        <th>Nº. do Lance</th>
                        <th>Lance (R$)</th>
                        <th>Lance + 5% (R$)</th>
                        <th>Usuário</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($lances as $lance)
                     <tr>
                        <th scope="row">{{ $lance->cod_lance}}</th>
                        <td>{{ number_format( $lance->valor_lance ,2,',', '.' ) }}</td> 
                        <td>{{ porcentagem_nnx($lance->valor_lance) }}</td>
                        <td>{{ $lance->nickname}}</td>
                     </tr>
                     @endforeach


                  </tbody>
               </table>

               @endif