<?php

namespace app\models\cliente;

class AnuncioModel extends \Eloquent{

	protected $table = 'tb_produto';
	protected $guarded = [];
	protected $primaryKey = 'cod_produto';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }


	//relacionamentos fotos - 1 para muitos
	public function fotos() {
		return $this->hasMany('app\models\cliente\FotoModel', 'cod_produto', 'cod_produto');
	}

//relacionamento lote - 1 para 1
	public function lote() {
		return $this->hasOne('app\models\cliente\LoteModel', 'cod_lote', 'cod_lote');
	}

	//relacionamento tipo - 1 para 1
	public function tipoLeilao() {
		return $this->hasOne('app\models\cliente\TipoModel', 'cod_tpo_leilao', 'cod_tpo_leilao');
	}

	//relacionamento lote - 1 para 1
	public function statusBem() {
		return $this->hasOne('app\models\cliente\StatusModel', 'cod_status', 'cod_status');
	}
	
	//relacionamento lote - 1 para 1
	public function dadoslote() {
		return $this->hasOne('app\models\cliente\LoteModel', 'cod_lote', 'cod_lote');
	}

}

