    


                     @if (Auth::check())
                         <li><a href="{{ URL('sair')}}" title="Sair"><i class="fa fa-sign-out"></i>Sair</a></li>
                          <li><a href="{{ URL('home')}}" title="Sair"><i class="fa fa-book"></i>Enviar Documentação</a></li>
                         <li> <a href="{{ URL('cliente/'.Auth::user()->cod_cliente.'/edit')}}">
                               <i class="fa fa-lock"></i>Minha conta</a>
                         </li>

                         <li><a href="{{ URL('senha/'.Auth::user()->cod_cliente.'/edit')}}"><i class="fa fa-key"></i> 
                              <span>Alterar minha senha</span></a>
                         </li>

                         <li> <a href="{{ URL('lances')}}"><i class="fa fa-money"></i>Meus lances</a> </li>

                         <li> <a href="#"><i class="fa fa-user"></i>{{ \Auth::user()->username }} </a></li>

                     @else
                         <li> <a href="#" data-toggle="modal" data-target="#myLogin"><i class="fa fa-lock"></i> Entrar</a> </li>
                         <li><a href="#" data-toggle="modal" data-target="#registrar"><i class="fa fa-lock"></i> Registrar </a> </li>
                    @endif

                    <!-- Logados e administradores -->

                    @if(Auth::check() && Auth::user()->ind_adm == "S")
                         
                         <li> <a href="{{ URL('lotes/show')}}"><i class="fa fa-tasks"></i>Lotes</a> </li>
                         <li> <a href="{{ URL('bens/show')}}"><i class="fa fa-shopping-cart"></i>Bens</a> </li>
                         <li> <a href="{{ URL('documento/show')}}"><i class="fa fa-file-text-o"></i>Documentos</a> </li>
                         <li> <a href="{{ URL('cliente/show')}}"><i class="fa fa-user"></i>Clientes</a> </li>
                         <li> <a href="{{ URL('lances/adm')}}"><i class="fa fa-refresh"></i>Validar Lances</a> </li>
                         <li> <a href="{{ URL('noticia/show')}}"><i class="fa fa-refresh"></i>Noticias</a> </li>
                         <li> <a href="{{ URL('texto/show')}}"><i class="fa fa-refresh"></i>Textos</a> </li>
						  <!-- <li> <a href="{{ URL('notavenda')}}"><i class="fa fa-ticket"></i>Nota</a> </li>-->
						 <li> <a href="{{ URL('relatorio')}}"><i class="fa fa-file-pdf-o"></i>Relatórios</a> </li>
                    @endif



<!-- Modal -->
<div class="modal fade" id="registrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Novo Cadastro</h4>
      </div>
          <div class="modal-body">
        <div class="row">
            <div class="col-md-4 col-md-offset-1"><a href="{{ URL('conta/1')}}" class="btn btn-primary btn-lg active" role="button">Pessoa Física</a></div>
            <div class="col-md-3 col-md-offset-2"><a href="{{ URL('conta/2')}}" class="btn btn-primary btn-lg active" role="button">Pessoa Jurídica</a></div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>








