<?php

namespace app\controllers\cliente;
use \app\models\cliente\ProdutoModel as UserModel;




class ProdutoController extends \BaseController {

	
	public function index()
	{
		//
	}

	public function show()
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//recupera todos os dados
		$dadosUser = UserModel::find($id);

		//verifica se o id que vc quer alterar os dados é o seu id meu, evitando assim que o usuario altere via url
		$idUser = \Auth::user()->cod_cliente;

		if($idUser != $id){
			return \Redirect::to('/cliente/'.$idUser.'/edit');
		}
		

		$data = ['user' => $dadosUser];

		return \View::make('cliente.painel.userEditar')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$rules = 
				[
					'nome' => 'required',
					'tel' => 'required'
				];

		//mensagens personalizadas
			$messages = \app\acme\MessagesAcme::Messages();
			$validator = \Validator::make(\Input::all(),$rules,$messages);

			if($validator->fails()){
				return \Redirect::back()
				->withInput(\Input::all())
				->withErrors($validator->messages());

			}else{

				$attributes = [
					'nome_user' => \Input::get('nome'),
					'num_tel' => \Input::get('tel'),
					'num_cel' => \Input::get('cel'),

				];

				$alterado = UserModel::where('cod_cliente',$id)->update($attributes);

				if($alterado){
						return \Redirect::back()->with('mensagem','<div class="text text-success">Dados alterado com sucesso!</div>');
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			}
	}


}
