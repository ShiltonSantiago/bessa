@extends('cliente.layout')
@section('conteudoPainel')


<!--======= CONTEÚDO DA PÁGINA =========-->

   <div class="index-page">
      <div class="container">

      @if(empty($lances))
<center>
               <div class="alert alert-danger" role="alert">Você ainda não fez nenhum lance.</div>
</center>
      @else

         <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

      @foreach($lances as $lance)

           <div class="panel panel-default">
             <div class="panel-heading" role="tab" id="headingThree">
               <h4 class="panel-title">
                 <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $lance->cod_lance}}" aria-expanded="false" aria-controls="collapseThree">
                  <i class="fa fa-info-circle" aria-hidden="true"></i> Item Leiloado: {{ $lance->nom_produto}} 
                  
                  <p class="text-right"><i class="fa fa-plus-circle" aria-hidden="true"></i> mais detalhes</p>
                 </a>
               </h4>
             </div>
             <div id="{{ $lance->cod_lance}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
               <div class="panel-body">
                  <button type="button"  data-toggle="modal" data-target="#myModal" 
                  class="btn btn-primary btn-ver-lance" data-bem="{{ $lance->cod_produto}}">
                  Visualizar todos os meus lances para esse item</button>
               </div>
             </div>
           </div>

      @endforeach


         </div>
      @endif
      </div>
   </div>

   <!--======= FIM DO CONTEÚDO DA PÁGINA =========-->

  <!-- Modal dados do cliente-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Meus Lances</h4>
      </div>
      <div class="modal-body">
        
    <div id="retorna_dados_lance"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


@stop
