<?php

namespace app\controllers\cliente;

use \app\models\cliente\ServicoModel as ServicoModel;
use \app\models\cliente\CidadeModel as CidadeModel;
use \app\models\cliente\lanceModel as lanceModel;
use \app\models\cliente\AnuncioModel as AnuncioModel;
use \app\models\cliente\UserModel as UserModel;
use \app\models\cliente\DocumentoModel as DocumentoModel;
use \app\models\cliente\LoteModel as LoteModel;
use \app\models\cliente\BancoModel as BancoModel;
use \app\models\cliente\FotoModel as FotoModel;


class AjaxController extends \BaseController {
    

    //Recupera servicos home via ajax
    public function getListaServico($valor)
    {
        $buscarServicosHome = ServicoModel::where('ind_ativo', '=', 'S')
                                      ->where('nome_servico', 'like', '%'.$valor.'%')
                                      ->orderBy('nome_servico');
        $resultadoServicosHome = $buscarServicosHome->get();

         $data = [ 'resultadoServicosHome' => $resultadoServicosHome ];

        return \View::make('cliente.AjaxRetornaBuscaServicoAutoComplete', $data);
    
        

    }

    //cidades por estado ajax
      public function getCidades($idEstado)
    {
        $estado = CidadeModel::where('cod_estado', '=', $idEstado) ->orderBy('nome_cidade');
        $cidades = $estado->get()->lists('nome_cidade', 'cod_cidade');
        return $cidades;
    }

    //listar servicos por categoria ajax
      public function getServicos($idCategoria)
    {
        $categoria = ServicoModel::where('cod_tipo_servico', '=', $idCategoria) ->orderBy('nome_servico');
        $servicos = $categoria->get()->lists('nome_servico', 'cod_servico');
        return $servicos;
    }

    //lista pelo combo na home busca avan�ada
    public function getBuscaAvancada(){

        $modalidade = \Input::get('modalidade');
        $cidade = \Input::get('cidade');
        $tipo = \Input::get('tipo');
        $categoria = \Input::get('categoria');

         $anuncios = \DB::table('tb_produto as pro')
                ->leftJoin('tb_lote as lot', 'lot.cod_lote', '=', 'pro.cod_lote')
                ->leftJoin('tb_tipo_leilao as tip', 'tip.cod_tpo_leilao', '=', 'lot.cod_tpo_leilao')
                ->leftJoin('tb_foto as fot','pro.cod_produto','=','fot.cod_produto')
                ->Where('pro.cod_categoria', 'like', "%$categoria%")
                ->Where('pro.cod_tpo_leilao', 'like', "%$tipo%")
                ->Where('lot.cidade', 'like', "%$cidade%")
                ->Where('lot.cod_tpo_leilao', 'like', "%$modalidade%")
                ->Where("fot.ind_foto_padrao", "=","S")
                ->get();

                $data = [ 'anuncios' => $anuncios ];

         return \View::make('cliente.AjaxRetornaBuscaAvancada', $data);

    }

    //retorna lance maior
    public function getLanceAtual(){

        $cod_produto = \Input::get('bem');

        $lanceAtual = lanceModel::where('cod_produto', '=', $cod_produto)->get()->max('valor_lance');

        return number_format($lanceAtual ,2,',', '.' );

    }

    //dar lance
    public function getAplicarLance(){

        // valida se o cliente tem documento ok
        $idUser = \Auth::user()->cod_cliente;
        $dadosDocs = UserModel::where('cod_cliente','=', $idUser)->where('ind_aceito_documento','=', 'A')->count();

        if(empty($dadosDocs)){
                $retorno = array('msg' => utf8_encode('Voc� n�o pode dar lance, pois sua documenta��o ainda n�o foi aceita!'), 'processo' => 'error');
                return $retorno; //envia array pro js
            }

            //recupera todos os dados do formulario do lance
            $dados = \Input::all();
			
			//bem ja foi arrematado
			$bem = AnuncioModel::find($dados["cod_produto"]);
			
             if($bem->ind_finalizado == 'S'){
                $retorno = array('msg' => utf8_encode('Voc� n�o pode dar lance, bem j� foi arrematado!'), 'processo' => 'error');
                return $retorno; //envia array pro js
            }
			
        
        //valida se o valor do lance nao � igual 
            $lanceAtual = lanceModel::where('cod_produto', '=', $dados["cod_produto"])->get()->max('valor_lance');
            
            if($dados["valor_lance"] <= $lanceAtual){
                $retorno = array('msg' => 'Seu lance deve ser maior, tente novamente!', 'processo' => 'lancemenor');
                return $retorno; //envia array pro js
            }


            //Salva lance
            $salvarLance = new lanceModel($dados);
            $add = $salvarLance->save();

            if($add){
                    $retorno = array('msg' => 'Lance realizado com sucesso!', 'processo' => 'success');
                     return $retorno; //envia array pro js
            }else{
                   $retorno = array('msg' => 'Erro ao realizar lance', 'processo' => 'error');
                    return $retorno; //envia array pro js
            }

    }

    // encerrar lances
    public function getEncerrarLance(){

        // arrematou pra x lance
        $cod_produto = \Input::get('cod_produto');
        $lanceAtual = lanceModel::where('cod_produto', '=', $cod_produto)->orderBy('valor_lance', 'desc')->take(1)->get();

        //valor do maior lance e codigo do lance
        $maior_lance = $lanceAtual[0]->valor_lance;
        $cod_maior_lance = $lanceAtual[0]->cod_lance;

        //cliente que deu maior lance
        $dadosUser = UserModel::find($lanceAtual[0]->cod_cliente);

        //dados do bem
        $dadosBem = AnuncioModel::find($lanceAtual[0]->cod_produto);

        //dados do lote
         $dadosLote = LoteModel::find($dadosBem->cod_lote);

        //arremata o lance
        $attributes = [ 'ind_arrematado' => "S" ];
        $alterado = LanceModel::where('cod_lance',$cod_maior_lance)->update($attributes);

        //daods financeiros paulo
        $banco = BancoModel::find("1");

        //enviar email para o dono do maior lance

                    //email de envio
                    $emailsender = 'site@bessaleiloes.com.br';   
                    $nomeCliente = $dadosUser["nom_cliente"];
                    $emailCliente = $dadosUser["username"];
                    $nome_bem = $dadosBem["nom_produto"];

                    //dados financeiros do lote parte credor
                    $num_processo = $dadosBem->num_processo;
                    $nome_credor = $dadosLote->credor_nome;
                    $agencia_credor = $dadosLote->credor_agencia;
                    $conta_credor = $dadosLote->credor_conta;

                    //dados financeiro pauloa bessa
                    $tipoConta = "Conta Corrente";
                    $nome_banco =  $banco->nom_banco;
                    $agencia = $banco->num_agencia;
                    $conta = $banco->num_conta;
                    $dono = $banco->nom_titular;
                    
                    // Fun��o de porcentagem: N � N% de X
                function porcentagem_nnx ( $valorOriginal ) {
               $valor = $valorOriginal; 
               $percentual = 5.0 / 100.0; // 5%
               $valor_final = $percentual * $valor;
               $valor_final_convertido = number_format($valor_final ,2,',', '.' );

                return $valor_final_convertido;
                }
                    
                    //valor do lance com a porcentagem de 5%
                        $lancePorcetagemBessa = porcentagem_nnx($maior_lance);



                    /* Verifica qual � o sistema operacional do servidor para ajustar o cabe�alho de forma correta. N�o alterar */
                    if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
                    elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
                    else die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

                    // Passando os dados obtidos pelo formul�rio para as vari�veis abaixo
                    setlocale(LC_TIME,"portuguese");
                    $data     = strftime(" %A, %d de %B de %Y ");
                    $hora = date ("H:i:s");
                    $emaildestinatario = $emailCliente;
                    $assunto           = 'Bessa Leil�es - Lance Arrematado - '.utf8_decode($nome_bem).'';
                    $link = 'http://www.bessaleiloes.com.br/';
                    $mensagem          = 'Parab�ns, '.utf8_decode($nomeCliente).',<br><br>'
                    . 'O seu lance foi o vencedor e arrematou: <b>'.utf8_decode($nome_bem).'</b><br>'
                    . 'Agora � muito importante que voc� fa�a o dep�sito da comiss�o do leiloeiro e envie o comprovante para receber a Guia de Pagamento do bem arrematado.<br><br>'
                    . '<b>Dados para dep�sito:</b><br>'
                    . 'Valor do dep�sito:R$ '.$lancePorcetagemBessa.'<br>'
                    . $nome_banco.'<br>'
                    . 'Ag�ncia:'.$agencia.'<br>'
                    . 'Conta Corrente'.$conta.'<br><br>'
                    . '<strong>Dados para envio do comprovante. Escolha a melhor op��o para envio:</strong><br>'
                    . 'E-mail: contato@bessaleiloes.com.br <br>'
                    . 'Whatsapp: (33) 98852-7310 <br>'
                    . 'Endere�o: Rua Prudente de Morais, 714 sala 103 <br><br>'
                    .'Cordialmente, <br><br>'
                    .'<strong>Paulo Bessa</strong><br>'
                    .'Leiloeiro Oficial';


                /* Montando a mensagem a ser enviada no corpo do e-mail. */
                $mensagemHTML = '<img src="http://bessaleiloes.com.br/bessa/public/frameworks/cliente/images/logo.png" width="150" height="auto"/>
                <p>'.$mensagem.'</p>
                <hr>';


                /* Montando o cabe�alho da mensagem */
                $headers = "MIME-Version: 1.1".$quebra_linha;
                $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
                // Perceba que a linha acima cont�m "text/html", sem essa linha, a mensagem n�o chegar� formatada.
                $headers .= "From: ".$emailsender.$quebra_linha;
                $headers .= "Return-Path: " . $emailsender . $quebra_linha;

                // Note que o e-mail do remetente ser� usado no campo Reply-To (Responder Para)
                $headers .= "Reply-To: ".$emailCliente.$quebra_linha;


                /* Enviando a mensagem */
                mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);

        // finalizou o anuncio
        $attributes = [ 'ind_finalizado' => "S" ];
        $finalizado = AnuncioModel::where('cod_produto',$cod_produto)->update($attributes);

         if($finalizado){
                    $retorno = array('msg' => 'Bem encerrado com sucesso!', 'processo' => 'success');
                     return $retorno; //envia array pro js
            }else{
                   $retorno = array('msg' => 'Erro ao encerrar bem', 'processo' => 'error');
                    return $retorno; //envia array pro js
            }

    }  

    //buscar dados do cliente 
    public function getDadosCliente(){

        $cod_cliente = \Input::get('cod_cliente');

        $dados = UserModel::where('cod_cliente', '=', $cod_cliente)->get();

        $data = [ 'dadosCliente' => $dados ];

         return \View::make('cliente.painel.AjaxRetornaDadosCliente', $data);
    }


      //buscar lances que um cliente deu pra um item 
    public function getDadosLances(){

        $bem = \Input::get('bem');

        $idUser = \Auth::user()->cod_cliente;

        $lances = LanceModel::where('cod_cliente','=', $idUser)
        ->where('cod_produto','=', $bem)
        ->orderBy('created_at', 'desc')
        ->get();

        $data = [ 'lances' => $lances ];

         return \View::make('cliente.painel.AjaxRetornaLancesCliente', $data);
    }

    //bem  por lote ajax
      public function getLotes($idLote)
    {
        $lotes = AnuncioModel::where('cod_lote', '=', $idLote) ->orderBy('nom_produto');
        $bens = $lotes->get()->lists('nom_produto', 'cod_produto');
        return $bens;
    }

    //lista todos os lances de um bem pesquisa no adm
        public function getLancesTotal(){

        $bem = \Input::get('bem');

        $lances = \DB::table('tb_lances as lan')
                ->leftJoin('tb_cliente as cli', 'cli.cod_cliente', '=', 'lan.cod_cliente')
                ->where('lan.cod_produto', '=', $bem)
                ->orderBy('valor_lance', 'desc')
                ->get();

        $data = [ 'lances' => $lances ];

         return \View::make('cliente.painel.AjaxRetornaLancesTotal', $data);

    }

    //libera boleot ou confirma pagamento
    public function getFinaliza(){

        $codlance = \Input::get('codlance');
        $boleto = \Input::get('boleto');

        if($boleto == "S"){

            $attributes = [ 'ind_pago' => "S" ];

            $alterado = LanceModel::where('cod_lance',$codlance)->update($attributes);

            if($alterado){
                    $retorno = array('msg' => 'Boleto Pago com sucesso!', 'processo' => 'success');
                     return $retorno; //envia array pro js
            }else{
                   $retorno = array('msg' => 'Erro ao pagar boleto', 'processo' => 'error');
                    return $retorno; //envia array pro js
            }
        }else{

            $attributes = [ 'ind_arrematado' => "S" ];

            $alterado = LanceModel::where('cod_lance',$codlance)->update($attributes);

          

    //finalza o leilao do bem e manda email pro sortudo

    //busca 
        $lance = LanceModel::find($codlance);

     //cliente que deu maior lance
        $dadosUser = UserModel::find($lance["cod_cliente"]);

        //dados do bem
        $dadosBem = AnuncioModel::find($lance["cod_produto"]);

        //enviar email para o dono do maior lance

                    //email de envio
                    $emailsender = 'site@bessaleiloes.com.br';   
                    $nomeCliente = $dadosUser["nom_cliente"];
                    $emailCliente = $dadosUser["username"];
                    $nome_bem = $dadosBem["nom_produto"];

                    /* Verifica qual � o sistema operacional do servidor para ajustar o cabe�alho de forma correta. N�o alterar */
                    if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
                    elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
                    else die("Este script n�o esta preparado para funcionar com o sistema operacional de seu servidor");

                    // Passando os dados obtidos pelo formul�rio para as vari�veis abaixo
                    setlocale(LC_TIME,"portuguese");
                    $data     = strftime(" %A, %d de %B de %Y ");
                    $hora = date ("H:i:s");
                    $emaildestinatario = $emailCliente;
                    $assunto           = 'Bessa Leil�es - Lance Arrematado - '.$nome_bem.'';
                    $link = 'http://www.bessaleiloes.com.br/bessa/public';
                    $mensagem          = 'Parab�ns, '.$nomeCliente.',<br><br>'
                    . 'O seu lance foi o vencedor e arrematou o seguinte produto: '.$nome_bem.'<br>'
                    . 'Agora, � muito importante que voc� <a href='.$link.'>acesse aqui</a> para gerar os boletos e fazer o pagamento.<br>'
                    . '<br> Atenciosamente, <br> Paulo Bessa <br> Leiloeiro Oficial';

                /* Montando a mensagem a ser enviada no corpo do e-mail. */
                $mensagemHTML = '<img src="http://bessaleiloes.com.br/bessa/public/frameworks/cliente/images/logo.png" width="100" height="auto"/>
                <p>'.$mensagem.'</p>
                <hr>';


                /* Montando o cabe�alho da mensagem */
                $headers = "MIME-Version: 1.1".$quebra_linha;
                $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
                // Perceba que a linha acima cont�m "text/html", sem essa linha, a mensagem n�o chegar� formatada.
                $headers .= "From: ".$emailsender.$quebra_linha;
                $headers .= "Return-Path: " . $emailsender . $quebra_linha;

                // Note que o e-mail do remetente ser� usado no campo Reply-To (Responder Para)
                $headers .= "Reply-To: ".$emailCliente.$quebra_linha;


                /* Enviando a mensagem */
                mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);

        // finalizou o anuncio
        $attributes = [ 'ind_finalizado' => "S" ];
        $finalizado = AnuncioModel::where('cod_produto',$lance["cod_produto"])->update($attributes);

          if($alterado){
                    $retorno = array('msg' => 'Boleto Liberado com sucesso!', 'processo' => 'success');
                     return $retorno; //envia array pro js
            }else{
                   $retorno = array('msg' => 'Erro ao liberar boleto', 'processo' => 'error');
                    return $retorno; //envia array pro js
            }

        }


    }
 

//atualiza combo ajax de valores possiveis de lance
    public function getPossiveisLances(){

    $cod_produto = \Input::get('bem');
    $anuncio = AnuncioModel::find($cod_produto);

    $lanceAtual = lanceModel::where('cod_produto', '=', $cod_produto)->get()->max('valor_lance');

    // se tiver lance pega o valor, se nao pega o valor inicial

     // se tiver lance pega o valor, se nao pega o valor inicial
        date_default_timezone_set('America/Sao_Paulo');
        $datAtual = date('Y-m-d H:i');

        if(empty($lanceAtual) &&  $datAtual <= $anuncio->dat_final_1){
                $valorAtual = $anuncio->lan_inicial_1;
            }elseif (empty($lanceAtual) &&  $datAtual > $anuncio->dat_final_1) {
                $valorAtual = $anuncio->lan_inicial_2;
            }else{
                $valorAtual = $lanceAtual;
            }

        $select = [];
        $incremento = $anuncio->dsc_incremento;
        $valUltimo = $valorAtual;


        for ($i=0; $i < 10; $i++) {
            array_push($select, ($valUltimo + $incremento) + ($i * $incremento));

        }

        $dados = [];
        for($i=0; $i < count($select); $i++) {
            $dados[$i]['codigo'] = $select[$i];
            $dados[$i]['preco'] = 'R$ ' . number_format($select[$i] ,2,',', '.' );
        }

        return $dados;

    }

    //tabela ajax lances 
       public function getAjaxTabelaLances()
    {
        $cod_produto = \Input::get('bem');
        $lances = \DB::table('tb_lances as lac')
                ->leftJoin('tb_cliente as cli', 'cli.cod_cliente', '=', 'lac.cod_cliente')
                ->where('lac.cod_produto', '=', $cod_produto)
                ->take(5)
                ->orderBy('valor_lance', 'desc')
                ->get();

         $data = [ 'lances' => $lances ];

        return \View::make('cliente.AjaxTabelaLances', $data);
    
        

    }


}
