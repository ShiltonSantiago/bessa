<?php

namespace app\controllers\cliente;
use \app\models\cliente\LoteModel as LoteModel;
use \app\models\cliente\AnuncioModel as AnuncioModel;


class RelatorioController extends \BaseController {

//form de gerar relatorio
	public function index()
	{

		$lotes = LoteModel::orderBy('ind_exibir','desc')->get();

		$data = [ 'lotes' => $lotes ];

		return \View::make('cliente.painel.form_relatorio', $data);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getPDF()
	{


		$data = ['dados' =>'teste'];

		$pdf = \PDF::loadView('cliente.painel.tabela_teste', $data);
		return $pdf->download('relatorio.pdf');
	}



}
