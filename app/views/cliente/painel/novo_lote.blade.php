﻿   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">



{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'lotes.store']) }}
   <fieldset>

   <!-- Form Name -->
   <legend>Cadastro do Lote</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Titulo</label>  
     <div class="col-md-5">
   {{ Form::text('titulo_lote',Input::old('titulo_lote'),['class' => 'form-control input-md']) }} {{ $errors->first('titulo_lote') }}
     </div>
   </div>


     <!-- Text input
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">1º Praça Data Inicial</label>
     <div class="col-md-5">
      {{ Form::text('dat_inicial_1',Input::old('dat_inicial_1'),['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_inicial_1') }}
     </div>
   </div>-->

     <!-- Text input
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">2º Praça Data Inicial</label> 
     <div class="col-md-5">
      {{ Form::text('dat_inicial_2',Input::old('dat_inicial_2'),['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_inicial_2') }}
     </div>
   </div>-->

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">1º Praça Data</label>  
     <div class="col-md-5">
     {{ Form::text('dat_final_1',Input::old('dat_final_1'),['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_final_1') }}
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">2º Praça Data</label>  
     <div class="col-md-5">
     {{ Form::text('dat_final_2',Input::old('dat_final_2'),['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_final_2') }}
     </div>
   </div>

    <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir dados da 2º Praça ?</label>
     <div class="col-md-5">
 
 <select name="ind_exibir_praca" class="form-control">
      <option value="N"> Nao </option>
       <option value="S"> Sim </option>
  </select>

     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cid_cidade">Cidade</label>
     <div class="col-md-5">
     {{Form::select('cidade', array('' => 'Selecione') + $cidades, Input::old('cidade'),['class' => 'form-control'])}}
      {{ $errors->first('cidade') }}
     </div>
   </div>


   <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Imagem Lote</label>
     <div class="col-md-5">
     {{ Form::file('img_lote', ['class' => 'input-file']) }}
     </div>
   </div>

  

      <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_status">Status</label>
     <div class="col-md-5">
     {{Form::select('cod_status', array('' => 'Selecione') + $status, Input::old('cod_status'), ['class' => 'form-control'])}}
      {{ $errors->first('cod_status') }}
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_tpo_leilao">Modalidade</label>
     <div class="col-md-5">
     {{Form::select('cod_tpo_leilao', array('' => 'Selecione') + $modalidades, Input::old('cod_tpo_leilao'), ['class' => 'form-control'])}}
      {{ $errors->first('cod_tpo_leilao') }}
     </div>
   </div>


   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir</label>
     <div class="col-md-5">
 
 <select name="ind_exibir" class="form-control">
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
  </select>

     </div>
   </div>

   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_lote">Informações</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_lote', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_local_lote">Local do Evento</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_local_lote', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>
   
   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Ao Vivo</label>  
     <div class="col-md-5">
   {{ Form::text('url_video',Input::old('url_video'),['class' => 'form-control input-md']) }} {{ $errors->first('url_video') }}
     </div>
   </div>

 <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir Video ?</label>
     <div class="col-md-5">
 
 <select name="ind_exibir_video" class="form-control">
      <option value="N"> Nao </option>
       <option value="S"> Sim </option>
  </select>

     </div>
   </div>


    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Nome do Credor</label>  
     <div class="col-md-5">
   {{ Form::text('credor_nome',Input::old('credor_nome'),['class' => 'form-control input-md']) }} {{ $errors->first('credor_nome') }}
     </div>
   </div>

    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Agencia Credor</label>  
     <div class="col-md-5">
   {{ Form::text('credor_agencia',Input::old('credor_agencia'),['class' => 'form-control input-md']) }} {{ $errors->first('credor_agencia') }}
     </div>
   </div>

    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Conta Credor</label>  
     <div class="col-md-5">
   {{ Form::text('credor_conta',Input::old('credor_conta'),['class' => 'form-control input-md']) }} {{ $errors->first('credor_conta') }}
     </div>
   </div>

  

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}



      </div>
     </div>


 <script>

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});

$("#datetimepicker_format_change").on("click", function(e){
  $("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
  $.datetimepicker.setLocale($(e.currentTarget).val());
});

$('.datas').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:  '1986/01/05'
});

</script>



   @stop
