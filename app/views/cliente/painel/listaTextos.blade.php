@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
  
<h1>Listagem de Textos</h1>

<hr>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#textos">
  Cadastrar novo texto
</button>

<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#slides">
  Cadastrar novo slider ou banner
</button>

<hr>

<table class="table table-striped" id="tabela_textos">
                  <thead>
                     <tr>
                        <th>Tipo</th>
						<th>Imagem</th>
                        <th>Editar</th>
						<th>Excluir</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($textos as $texto)
                     <tr>

                        <td>{{ $texto->dsc_tpo_informacao}}</td>
                        <td>
						@if(empty($texto->img_texto))
							Não se aplica
						@else
						<img width="100" height="100" src="{{ URL('public/imagens/'.$texto->img_texto.'')}}">
					@endif
						</td>
                  <td>
                    <a href="{{ URL('texto/'.$texto->cod_informacao.'/edit')}}">
                    <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
                  </td> 
				  
				  <td>
                        {{ Form::open([ 'method' => 'DELETE', 'route' => [ 'texto.destroy',$texto->cod_informacao ] ]) }}
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Excluir
                          </button>
                            {{ Form::close() }}
                        </td>

                     </tr>
                     @endforeach
                  </tbody>

               </table>


   </div>
  </div>

  <!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="textos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cadastrar novo texto</h4>
      </div>
      <div class="modal-body">
        
{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'texto.store']) }}

<!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-1 control-label" for="cid_cidade">Tipo</label>
     <div class="col-md-5">
     {{Form::select('cod_tpo_informacao', array('' => 'Selecione') + $tiposSimples, Input::old('cod_tpo_informacao'),['class' => 'form-control'])}}
      {{ $errors->first('cod_tpo_informacao') }}
     </div>
   </div>

  <!-- Text input-->
   <div class="form-group">
     <label class="col-md-1 control-label" for="textinput">Descrição</label>  
     <div class="col-md-11">
     {{ Form::textarea('dsc_informacao', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}


      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="slides" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"> Cadastrar novo slider ou banner</h4>
      </div>
      <div class="modal-body">
       {{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'texto.store']) }}

<!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cid_cidade">Tipo</label>
     <div class="col-md-5">
     {{Form::select('cod_tpo_informacao', array('' => 'Selecione') + $tiposUpload, Input::old('cod_tpo_informacao'),['class' => 'form-control'])}}
      {{ $errors->first('cod_tpo_informacao') }}
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Link</label>  
     <div class="col-md-5">
   {{ Form::text('link',Input::old('link'),['class' => 'form-control input-md']) }} {{ $errors->first('link') }}
     </div>
   </div>

    <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Imagem</label>
     <div class="col-md-5">
     {{ Form::file('img_texto', ['class' => 'input-file']) }}
     </div>
   </div>

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}
      </div>
    </div>
  </div>
</div>



  

@stop
