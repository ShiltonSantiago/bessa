<?php

namespace app\models\cliente;

class BancoModel extends \Eloquent{

	protected $table = 'tb_dados_bancarios';
	protected $guarded = [];
	protected $primaryKey = 'cod_dados_bancarios';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
}






