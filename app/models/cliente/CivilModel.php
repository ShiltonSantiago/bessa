<?php

namespace app\models\cliente;

class CivilModel extends \Eloquent{

	protected $table = 'tb_estado_civil';
	protected $guarded = [];
	protected $primaryKey = 'cod_estado_civil';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
}

