<?php

namespace app\controllers\cliente;
use \app\models\cliente\UserModel as UserModel;
use \app\models\cliente\CivilModel as CivilModel;
use \app\models\cliente\LoginLogModel as LoginLogModel;
use \app\models\cliente\DocumentoModel as DocumentoModel;
use Dompdf\Dompdf;
use Dompdf\Options;


class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$dadosUser = \DB::table('tb_cliente as cli')->get();
		$civils = CivilModel::orderBy('dsc_estado_civil')->get();

		$data = [
					'user' => $dadosUser, 
					'civils' => $civils
				];

		return \View::make('cliente.painel.listaUsuarios')->with($data);
	}

	// ativar cadastro
	public function getAtivarCadastro($valor)
	{
		$token_recebido = base64_decode($valor);

		$dadosUser = UserModel::find($token_recebido);

		if($dadosUser){
				$attributes =  [ 'ind_ativo' => "S" ];
				$alterado = UserModel::where('cod_cliente',$token_recebido)->update($attributes);
				$Ativado = "S";
			}



		$data = [ 'ativado' => $Ativado ];

		return \View::make('cliente.ativar')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		
	// instantiate and use the dompdf class
		$options = new Options();
		$options->set('isRemoteEnabled', TRUE);
		$dompdf = new Dompdf($options);

	//dados do cliente
		$codCliente = \Auth::user()->cod_cliente;
		$cpf = \Auth::user()->num_cpf;
		$nom_cliente =  \Auth::user()->nom_cliente;

$conteudo_pdf = "<center><img src='http://www.bessaleiloes.com.br/frameworks/cliente/images/logo.png' width='180' height='auto' /></center>
<h3 style='text-align: center;'>Termo de serviço</h3>
<br>
<br>
O presente <strong>CONTRATO DE ADESÃO</strong> descreve os <strong>TERMOS</strong> e <strong>CONDIÇÕES</strong> para a participação do usuário nos leilões disponíveis no “site”. Ao clicar nos botões de Pessoa Física ou Pessoa Jurídica, o usuário <strong>DECLARA ESTAR CIENTE E DE ACORDO</strong> com as condições estabelecidas neste contrato. Qualquer dúvida não hesite em contatar o Bessa Leilões através do atendimento “CONTATO”, ou ligando para o telefone: (33) 98852-7310.
<br><br>
1) Para estar apto a dar ofertas/lances para a aquisição dos bens (veículos, imóveis, máquinas, equipamentos e outros bens) expostos no “site” www.bessaleiloes.com.br , o usuário deverá ter capacidade civil para contratar, nos termos da legislação em vigor. Menores de idade de 18 anos e os que são considerados absolutamente incapazes para realizar quaisquer atos da vida civil são impedidos de adesão e negociação.
<br><br>
2) Para participação nos leilões divulgados neste “site”, o usuário deverá fornecer seus dados pessoais para cadastramento, sendo essencial, para tanto, preencher todos os campos de forma clara e precisa. Sempre que necessário, o cadastro do usuário deverá ser atualizado.
<br><br>
3) Ao se cadastrar o usuário indicará um nickname (apelido) para sua identificação quando der lances nos leilões oferecidos no site, bem como e-mail, uma senha pessoal e intransferível, a qual não poderá ser utilizada para outras finalidades não autorizadas.
<br><br>
4) O login a ser cadastrado pelo usuário para acesso ao site será o seu E-mail e não poderá guardar semelhança com o nome do “site” ou leiloeiro ou com o nome das empresas proprietárias dos bens divulgados no “site”, como também não poderá ser cadastrado login considerado ofensivo ou que contenha dados pessoais do usuário, alguma URL ou endereço eletrônico.
<br><br>
5) Caso o usuário não queira ser reconhecido pelos demais usuários do Site deverá optar em usar o nickname, caso contrário junto ao seu lance será informado o seu e-mail.
<br><br>
6) Para segurança do usuário, sua senha e dados serão transmitidos criptografados (Certificado de Segurança SSL - Secure Socket Layer).
<br><br>
7) O usuário se compromete a não divulgar a sua senha a terceiros. No caso de uso não autorizado da senha, o usuário deverá enviar, imediatamente, um e-mail à Central de Atendimento do site comunicando o fato, sob pena de incorrer nas perdas e danos cabíveis.
<br><br>
<strong>8) O usuário cadastrado no “site” autoriza expressamente a verificação de seus dados junto aos órgãos de proteção ao crédito. </strong>
<br><br>
9) Verificada inconsistência nos dados informados pelo usuário e/ou pendência financeira, a Central de Atendimento do Bessa Leilões entrará em contato via e-mail ou telefone.
<br><br>
<strong>10) Após feito o cadastro o usuário deverá enviar pelo próprio “site” seus documentos digitalizados, juntamente com o termo de compromisso assinado e com reconhecimento de firma ou autenticação. </strong>
<br><br>
<strong>11) Assim que concluso o cadastro a equipe do Bessa Leilões terá até 72 horas para analisar e HOMOLOGAR o cadastro. SOMENTE após homologação o usuário será liberado para participar dos leilões. </strong>
<br><br>
12) Os leilões serão realizados por Leiloeiro Oficial legalmente habilitado para o exercício das funções em data, horário e local previamente determinados e divulgados em editais publicados em jornais de grande circulação, no SITE, em Redes Sociais e através de Email Marketing.
<br><br>
13) Os lances poderão ser ofertados através do SITE www.bessaleiloes.com.br e/ou presencialmente (na data do encerramento do leilão).<strong> Os lances ofertados são IRREVOGÁVEIS e IRRETRATÁVEIS. O usuário é responsável por todas as ofertas registradas em seu nome, e se obriga pelo que os lances não podem ser anulados e/ou cancelados em nenhuma hipótese.</strong>
<br><br>
14) O usuário poderá ofertar mais de um lance para um mesmo bem, prevalecendo sempre o maior lance ofertado. O “site” www.bessaleiloes.com.br permite o recebimento de lances virtuais simultaneamente aos presenciais e em tempo real.
<br><br>
15) Próximo do fechamento do lote, em leilões na modalidade mista (presencial e online), caso algum lance seja recebido no último minuto do fechamento do lote, ou alguma oscilação de acesso internet no local do pregão presencial durante a disputa, poderá ser reaberto o cronômetro em 01 (um) minuto para o encerramento do lote, para que todos os usuários interessados tenham a oportunidade de efetuar novos lances.
<br><br>
16) O Bessa Leilões poderá limitar, cancelar ou suspender o cadastro definitivamente, de qualquer usuário que não cumprir as condições estabelecidas no presente contrato e ainda poderá impedir a participação do usuário nos leilões realizados por este leiloeiro. E o arrematante inadimplente não será admitido a participar de qualquer outro leilão divulgado no “site” www.bessaleiloes.com.br, pelo que seu cadastro ficará bloqueado. Caso sejam identificados cadastros vinculados a este cadastro bloqueado, os mesmos serão igualmente bloqueados.
<br><br>
<strong>17) O arrematante que não enviar seus documentos digitalizados em um prazo máximo de até 90 dias terá seu cadastro inativado.</strong>
<br><br>
18) Os bens (veículos, máquinas, equipamentos e outros bens) a serem levados a leilão estarão em exposição nos locais indicados na descrição de cada lote (no “site”), para visitação dos interessados, nos dias e horários determinados que antecederem os leilões OU por fotos (meramente ilustrativas) exibidas na “homepage” www.bessaleiloes.com.br.
<br><br>
<strong>19) Para aqueles usuários que arrematarem bens através do Leilão Online do “site” www.bessaleiloes.com.brautorizam através do aceite eletrônico deste contrato o LEILOEIRO designado em Edital para assinar os Termos de Arremate e Recibos em seu nome. </strong>
<br><br>
<strong>20) Não há custo algum para o usuário lançar sua(s) oferta(s) no “site” www.bessaleiloes.com.br, a não ser que seu lance seja vencedor, então arcará com a comissão que incidirá sobre o valor do bem e a ser pago juntamente com o valor do bem arrematado e despesas administrativa se houver, cabe ao arrematante o pagamento das despesas administrativas informadas nos editais e anexos, devendo ser paga diretamente ao leiloeiro. </strong>
<br><br>
21) Havendo inadimplência poderá o leiloeiro emitir titulo de crédito para a cobrança de valores e despesas encaminhando a protesto, por falta de pagamento, se for o caso, sem prejuízo da execução prevista no artigo 39, do Decreto nº. 21.981/32.
<br><br>
22) Os bens serão vendidos no estado em que se encontram, sendo que o vendedor se, no caso de veículo responsabiliza pelo pagamento de todos e quaisquer tributos, encargos e multas (inclusive o Imposto sobre a Propriedade de Veículos Automotores - IPVA e o Seguro Obrigatório), pendentes sobre os veículos apregoados, relativos a períodos anteriores à data da venda, EXCETO quando os tributos, encargos, multas, IPVA e o Seguro Obrigatório estejam expressamente previstos no lote, cujo pagamento será de responsabilidade do arrematante.
<br><br>
23) OS BENS SERÃO VENDIDOS NO ESTADO E CONSERVAÇÃO EM QUE SE ENCONTRAM, SEM GARANTIA, inclusive quanto a câmbio e motor (para veículos) que porventura não sejam originais de fábrica, ficando a sua regularização por conta do arrematante, isentando assim o Comitente Vendedor e o Leiloeiro que é mero mandatário, de quaisquer defeitos ou vícios ocultos.
<br><br>
24) Os lotes serão vendidos “um a um”, a quem oferecer maior lance, desde que o valor do lance seja igual ou superior ao preço mínimo determinado pelo vendedor. Todos os lances captados durante o leilão serão inseridos no “site”, possibilitando a todos os usuários o acompanhamento “ONLINE” do leilão.
<br><br>
25) O prazo de entrega da documentação dos veículos arrematados será informada na descrição dos respectivos lotes. A seu exclusivo custo o arrematante assume, a responsabilidade de transferência da propriedade do veículo para o seu nome, no prazo máximo de 30 (trinta) dias contados da data do recebimento dos documentos.
<br><br>
26) O arrematante declara estar ciente de que o veículo está sendo arrematado, no estado que se encontra, sem garantia, revisão e que não está coberto pelo prazo de garantia do fabricante e assim isentando o vendedor e o leiloeiro de qualquer responsabilidade, inclusive por vícios ou defeitos, ocultos ou não, considerando-se que o mesmo teve oportunidade de vistoriar o bem, conforme os editais.
<br><br>
27) O bem arrematado e pago, deverá, obrigatoriamente, ser retirado do pátio do Bessa Leilões, dentro do prazo máximo de 48 (quarenta e oito) horas, no endereço a ser informado na confirmação da arrematação sob pena de ser cobrada uma <strong>taxa de estadia diária de R$ 8,00 (oito reais)</strong>, a ser paga no ato da retirada.
<br><br>
28) As políticas de segurança estão disponíveis na seção “Política de Segurança” do “site”.
<br><br>
29) O Bessa Leilões não se responsabiliza por prejuízos ou quaisquer tipos de danos advindos das transações efetuadas entre os usuários e os vendedores, atuando sempre e tão somente como provedora de espaço virtual para divulgação online de bens, limitando-se a veicular, através de seu “site” específico, os dados dos bens fornecidos pelos vendedores.
<br><br>
30) Os problemas e dúvidas envolvidas com as transações efetuadas durante e depois do leilão serão dirimidas através do Bessa Leilões nos telefones disponíveis na seção CONTATO.
<br><br>
31) O Bessa Leilões a seu exclusivo critério, poderá cancelar qualquer oferta de compra, sempre que não for possível autenticar a identidade do usuário, ou caso este venha a descumprir as condições estabelecidas no presente instrumento.
<br><br>
32) Responderá o usuário criminalmente pelo uso de equipamento, programa ou procedimento que interferir no funcionamento do “site”.
<br><br>
33) Qualquer prejuízo eventualmente acarretado ao usuário, decorrente de dificuldades técnicas ou falhas no sistema da Internet, não poderá ser atribuída a responsabilidade à Bessa Leilões. O Bessa Leilões não garante o acesso contínuo de seus serviços, uma vez que a operação do “site” poderá sofrer interferências acarretadas por diversos fatores fora de seu controle.
<br><br>
34) O Bessa Leilões poderá, a seu livre arbítrio e a qualquer momento, acrescentar, extinguir ou alterar todos ou alguns dos serviços disponíveis no “site”, bem como alterar as condições constantes deste contrato, após a sua divulgação no “site”, ficando desde já estabelecido que as alterações passarão a vigorar no mínimo com antecedência de 05 (cinco) dias da realização do leilão.
<br><br>
35) Este contrato será regido pela legislação brasileira em vigor, ficando desde já eleito o Foro da Comarca de Governador Valadares/MG, como competente para dirimir toda e qualquer questão oriunda do seu cumprimento.
<br>
<br>
<br>

<img src='/var/www/public_html/bessa/imagens_pdf/assinatura.jpg' width='160' height='auto' />
<strong>Paulo Bessa</strong>
LEILOEIRO OFICIAL
<br><br><br><br>
_______________________________
<br>
$nom_cliente<br>
$cpf
";

if ( get_magic_quotes_gpc() )
    $documentTemplate = stripslashes($documentTemplate);
$dompdf->load_html($conteudo_pdf, 'UTF-8');
//$dompdf->loadHtml($conteudo_pdf, 'UTF-8');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// enviar documento destino para download
$output = $dompdf->output();

file_put_contents("$codCliente.pdf", $output);
		

		//recupera todos os dados
		$dadosUser = UserModel::find($id);

		$civils = CivilModel::orderBy('dsc_estado_civil')->get();

		//verifica se o id que vc quer alterar os dados é o seu id meu, evitando assim que o usuario altere via url
		$idUser = \Auth::user()->cod_cliente;

		if(\Auth::user()->ind_adm == "N"){
			if($idUser != $id){
				return \Redirect::to('/cliente/'.$idUser.'/edit');
			}
	}
		

		$data = ['user' => $dadosUser, 'civils' => $civils ];

		return \View::make('cliente.painel.userEditar')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

				$attributes =  [   
					  		'nom_cliente' => \Input::get('nom_cliente'),
					  		'nom_mae' => \Input::get('nom_mae'),
					  		'num_rg' => \Input::get('num_rg'),
					  		'num_cpf' => \Input::get('num_cpf'),
					  		'dat_nasc' => \Input::get('dat_nasc'),
					  		'atv_profissional' => \Input::get('atv_profissional'),
					  		'nom_empresa' => \Input::get('nom_empresa'),
					  		'end' => \Input::get('end'),
					  		'num_cep' => \Input::get('num_cep'),
					  		'complemento' => \Input::get('complemento'),
					  		'bairro' => \Input::get('bairro'),
					  		'num_end' => \Input::get('num_end'),
					  		'num_telefone' => \Input::get('num_telefone'),
					  		'num_celular' => \Input::get('num_celular'),
					  		'estado' => \Input::get('estado'),
					  		'cidade' => \Input::get('cidade'),
					  		'cod_estado_civil' => \Input::get('cod_estado_civil'),
					  		'cod_tpo_pessoa' => \Input::get('cod_tpo_pessoa'),
					  		'insc_estadual' => \Input::get('insc_estadual'),
		  					'cnpj' => \Input::get('cnpj')
					 ];

				$alterado = UserModel::where('cod_cliente',$id)->update($attributes);

				if($alterado){
						return \Redirect::back()->with('mensagem','<div class="text text-success">Dados alterados com sucesso!</div>');
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			
	}

	public function store()
	{
			$codTpoPessoa = \Input::get('cod_tpo_pessoa');

			$rulesFisica = [
				'nom_cliente' => 'required',
				'nickname' => 'required|unique:tb_cliente',
		  		'nom_mae' =>  'required',
		  		'username' => 'required|unique:tb_cliente|email',
		  		'num_rg' =>  'required',
		  		'num_cpf' =>  'required',
		  		'dat_nasc' =>  'required',
		  		'atv_profissional' =>  'required',
		  		'password' =>  'required',
		  		'end' =>  'required',
		  		'num_cep' =>  'required',
		  		'bairro' =>  'required',
		  		'num_end' =>  'required',
		  		'num_celular' =>  'required',
		  		'estado' =>  'required',
		  		'cidade' =>  'required',
		  		'cod_estado_civil' =>  'required'
		];

			$rulesJuridica = [
				'nom_cliente' => 'required',
				'nickname' => 'required|unique:tb_cliente',
		  		'username' => 'required|unique:tb_cliente|email',
		  		'cnpj' =>  'required',
		  		'dat_nasc' =>  'required',
		  		'atv_profissional' =>  'required',
		  		'nom_empresa' =>  'required',
		  		'password' =>  'required',
		  		'end' =>  'required',
		  		'num_cep' =>  'required',
		  		'bairro' =>  'required',
		  		'num_end' =>  'required',
		  		'num_celular' =>  'required',
		  		'estado' =>  'required',
		  		'cidade' =>  'required',
		  		'cod_estado_civil' =>  'required'
		];

		//fisica ou juridica
		if($codTpoPessoa == 1){
			$rules = $rulesFisica;
		}else{
			$rules = $rulesJuridica;
		}

		//mensagens personalizadas
			$messages = \app\acme\MessagesAcme::Messages();
			$validator = \Validator::make(\Input::all(),$rules,$messages);

			if($validator->fails()){
				return \Redirect::back()
				->withInput(\Input::all())
				->withErrors($validator->messages());

			}else{

			$dados = [   
		  		'nom_cliente' => \Input::get('nom_cliente'),
		  		'nickname' => \Input::get('nickname'),
		  		'nom_mae' => \Input::get('nom_mae'),
		  		'username' => \Input::get('username'),
		  		'num_rg' => \Input::get('num_rg'),
		  		'num_cpf' => \Input::get('num_cpf'),
		  		'dat_nasc' => \Input::get('dat_nasc'),
		  		'atv_profissional' => \Input::get('atv_profissional'),
		  		'nom_empresa' => \Input::get('nom_empresa'),
		  		'password' => \Hash::make(\Input::get('password')),
		  		'end' => \Input::get('end'),
		  		'num_cep' => \Input::get('num_cep'),
		  		'complemento' => \Input::get('complemento'),
		  		'bairro' => \Input::get('bairro'),
		  		'num_end' => \Input::get('num_end'),
		  		'num_telefone' => \Input::get('num_telefone'),
		  		'num_celular' => \Input::get('num_celular'),
		  		'estado' => \Input::get('estado'),
		  		'cidade' => \Input::get('cidade'),
		  		'cod_estado_civil' => \Input::get('cod_estado_civil'),
		  		'cod_tpo_pessoa' => \Input::get('cod_tpo_pessoa'),
		  		'insc_estadual' => \Input::get('insc_estadual'),
		  		'cnpj' => \Input::get('cnpj')
		 ];
			$salvarNovoCliente = new UserModel($dados);
			$salvarNovoCliente->save();

			if($salvarNovoCliente){

					$credentials = [ 'username' => \Input::get('username'), 'password' => \Input::get('password') ];

				//autentica e já salva log login
					$auth = \Auth::attempt($credentials,true);

					if($auth){
						//salva log login
						$dadosLog = [ 'cod_cliente' => \Auth::user()->cod_cliente ];
						$login_log = new LoginLogModel($dadosLog);
						$login_log->save();
					}

						//coloca ele como aguardando documento , coloca F como ele ainda nao enviou
						$status = "F";
						$attributes = [ 'ind_aceito_documento' => $status ];
						$alterado = UserModel::where('cod_cliente',\Auth::user()->cod_cliente)->update($attributes);
						
					//apos logar envio email de bem vindo

					//email de envio
            		$emailsender = 'site@bessaleiloes.com.br';   

            		$nomeCliente = \Input::get('nom_cliente');
        			$emailCliente = \Input::get('username');

			        /* Verifica qual é o sistema operacional do servidor para ajustar o cabeçalho de forma correta. Não alterar */
			        if(PHP_OS == "Linux") $quebra_linha = "\n"; //Se for Linux
			        elseif(PHP_OS == "WINNT") $quebra_linha = "\r\n"; // Se for Windows
			        else die("Este script nao esta preparado para funcionar com o sistema operacional de seu servidor");

			        // Passando os dados obtidos pelo formulário para as variáveis abaixo
			        setlocale(LC_TIME,"portuguese");
			        $data     = strftime(" %A, %d de %B de %Y ");
			        $hora = date ("H:i:s");
			        $token = base64_encode(\Auth::user()->cod_cliente);
			        $emaildestinatario = $emailCliente;
					$bcc = 'contato@bessaleiloes.com.br';
			        $link = 'www.bessaleiloes.com.br/bessa/public/ativar/'.$token.'';
			        $termo = 'www.bessaleiloes.com.br/bessa/public/termo.pdf';
			        $assunto           = 'Bem-vindo ao Bessa Leilões';
			        $mensagem          = 'Bem-vindo '.$nomeCliente.',<br><br>Estamos muito satisfeitos em tê-lo como cliente.<br><br>'
			        . 'Para concluir o seu cadastro será preciso que você digitalize e envie cópia dos seguintes documentos:<br>'

					. 'Pessoa física:<br/>
					a) Carteira de Identidade (RG) ou documento equivalente (documento de identidade expedido por entidades de classe, tais como OAB, CREA, CRM e outras, ou pelas Forças Armadas do Brasil);<br/>
					b) Cadastro de Pessoa Física (CPF);<br/>
					c) RG ou documento equivalente e nome e CPF do cônjuge, se for o caso;<br/>
					d) comprovante de residência em nome do arrematante (conta de água, luz ou telefone)<br/>
					e) Termo de Adesão assinado e com firma reconhecida.<br/><br/>'
					. 'Pessoa jurídica:<br/>
					a) comprovante de inscrição e de situação cadastral no Cadastro Nacional da Pessoa Jurídica (CNPJ);<br/>
					b) contrato social, até a última alteração, ou Declaração de Firma Individual;<br/>
					c) Carteira de Identidade (RG) ou documento equivalente (documento de identidade expedido por entidades de classe, tais como OAB, CREA, CRM e outras, ou pelas Forças Armadas do Brasil) e Cadastro de Pessoa Física (CPF) do representante legal ou do preposto da pessoa jurídica.<br/>
					d) Termo de Adesão assinado e com firma reconhecida.<br/><br/>'

			        . 'Clique no link para imprimir o Termo de Adesão: <a href='.$termo.'>Abrir Termo de Adesão </a><br><br>'
			        . 'Com os documentos em mãos, você precisa ativar a sua conta clicando no link: <a href='.$link.'>Ativar Conta </a><br><br>'
			        . 'Faça <em>login</em> com sua conta e envie os documentos.'
			        . '<br><br>Nosso e-mail de contato: contato@bessaleiloes.com.br<br>'
	                . '<br> Cordialmente, <br> <b>Paulo Bessa</b>';


		        /* Montando a mensagem a ser enviada no corpo do e-mail. */
		        $mensagemHTML = '<img src="http://bessaleiloes.com.br/bessa/public/frameworks/cliente/images/logo.png" width="130" height="auto"/>
		        <p>'.$mensagem.'</p>
		        <hr>';


		        /* Montando o cabeçalho da mensagem */
		        $headers = "MIME-Version: 1.1".$quebra_linha;
		        $headers .= "Content-type: text/html; charset=iso-8859-1".$quebra_linha;
		        // Perceba que a linha acima contém "text/html", sem essa linha, a mensagem não chegará formatada.
		        $headers .= "From: ".$emailsender.$quebra_linha;
				$headers .= "Bcc: ".$bcc.$quebra_linha;
		        $headers .= "Return-Path: " . $emailsender . $quebra_linha;

		        // Note que o e-mail do remetente será usado no campo Reply-To (Responder Para)
		        $headers .= "Reply-To: ".$emailCliente.$quebra_linha;


		        /* Enviando a mensagem */
		        mail($emaildestinatario, $assunto, $mensagemHTML, $headers, "-r". $emailsender);

					return \Redirect::to('/home');
			}else{
				 	return \Redirect::back()->with('mensagem',
						'<div class="alert alert-error" role="alert">Erro ao criar conta, tente novamente!</div>');
				}


		}
	}
}
