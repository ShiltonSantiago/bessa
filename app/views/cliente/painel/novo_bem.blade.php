   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">



{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'bens.store']) }}
   <fieldset>

   <!-- Form Name -->
   <legend>Cadastro do Bem</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Nome</label>  
     <div class="col-md-5">
   {{ Form::text('nom_produto',Input::old('nom_produto'),['class' => 'form-control input-md']) }} {{ $errors->first('nom_produto') }}
     </div>
   </div>


  
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Texto da Home</label>  
     <div class="col-md-5">
      {{ Form::text('dsc_home',Input::old('dsc_home'),['class' => 'form-control input-md']) }} {{ $errors->first('dsc_home') }}
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Valor Lance Inicial 1º Praça</label>  
     <div class="col-md-5">
      <div class="input-group">
      <div class="input-group-addon">R$ </div>
          {{ Form::text('lan_inicial_1',Input::old('lan_inicial_1'),['class' => 'form-control input-md lan_inicial']) }} {{ $errors->first('lan_inicial_1') }}
     </div>
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Valor Lance Inicial 1º Praça</label>   
     <div class="col-md-5">
      <div class="input-group">
      <div class="input-group-addon">R$ </div>
          {{ Form::text('lan_inicial_2',Input::old('lan_inicial_2'),['class' => 'form-control input-md lan_inicial']) }} {{ $errors->first('lan_inicial_2') }}
     </div>
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_categoria">Categoria</label>
     <div class="col-md-5">
      {{Form::select('cod_categoria', array('' => 'Selecione') + $categoria , Input::old('cod_categoria'),['class' => 'form-control'])}}
      {{ $errors->first('cod_categoria') }}
     </div>
   </div>

     <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_status">Status</label>
     <div class="col-md-5">
     {{Form::select('cod_status', array('' => 'Selecione') + $status, Input::old('cod_status'), ['class' => 'form-control'])}}
      {{ $errors->first('cod_status') }}
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_tpo_leilao">Tipo de leilão</label>
     <div class="col-md-5">
      {{Form::select('cod_tpo_leilao', array('' => 'Selecione') + $tipo, Input::old('cod_tpo_leilao'),['class' => 'form-control'])}}
      {{ $errors->first('cod_tpo_leilao') }}
     </div>
   </div>


   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Lote</label>  
     <div class="col-md-5">
     {{Form::select('dsc_lote', array('' => 'Selecione') + $lote, Input::old('dsc_lote'),['class' => 'form-control'])}}
      {{ $errors->first('dsc_lote') }}
     </div>
   </div>


   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_incremento">Incremento</label>  
     <div class="col-md-5">
            {{ Form::text('dsc_incremento',Input::old('dsc_incremento'),['class' => 'form-control input-md']) }} {{ $errors->first('dsc_incremento') }}      
     </div>
   </div>

    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Número Processo</label>  
     <div class="col-md-5">
   {{ Form::text('num_processo',Input::old('num_processo'),['class' => 'form-control input-md']) }} {{ $errors->first('num_processo') }}
     </div>
   </div>

    <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">PDF</label>
     <div class="col-md-5">
     {{ Form::file('pdf_lote', ['class' => 'input-file']) }}
     </div>
   </div>


   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_produto">Descrição</label>
     <div class="col-md-5">                     
            {{ Form::textarea('dsc_produto', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="obs_produto">Observações</label>
     <div class="col-md-5">                     
     {{ Form::textarea('obs_produto', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>


   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_visitacao">Localização e Retirada</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_visitacao', null, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Forma de pagamento</label>
     <div class="col-md-5">
       {{Form::select('cod_forma_pag', array('' => 'Selecione') +  $pagamento, Input::old('cod_forma_pag'),['class' => 'form-control'])}}
      {{ $errors->first('cod_forma_pag') }}
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir</label>
     <div class="col-md-5">
 
 <select name="ind_ativo" class="form-control">
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
  </select>

     </div>
   </div>
   
   

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}



      </div>
     </div>


 <script>

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});

$("#datetimepicker_format_change").on("click", function(e){
  $("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
  $.datetimepicker.setLocale($(e.currentTarget).val());
});

$('.datas').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:  '1986/01/05'
});

</script>

   <script type="text/javascript">
        $(document).ready(function(){
              $(".lan_inicial").maskMoney({showSymbol:true, symbol:"", decimal:".", thousands:""});
        });
    </script>



   @stop
