
$(document).ready(function() {
    $("#leilao-todos").click(function() {
    	$(".retiraleilao-liberado").show(300);
    	$(".retiraleilao-em-breve").show(300);
    	$(".retiraleilao-suspenso").show(300);
    	$(".retiraleilao-encerrado").show(300);
    });
    $("#leilao-embreve").click(function() {
    	$(".retiraleilao-liberado").hide(300);
    	$(".retiraleilao-encerrado").hide(300);
    	$(".retiraleilao-suspenso").hide(300);
    	$(".retiraleilao-em-breve").show(300);
    });
    $("#leilao-liberado").click(function() {
    	$(".retiraleilao-em-breve").hide(300);
		$(".retiraleilao-suspenso").hide(300);
    	$(".retiraleilao-encerrado").hide(300);
    	$(".retiraleilao-liberado").show(300);
    });
    $("#leilao-suspenso").click(function() {
    	$(".retiraleilao-em-breve").hide(300);
		$(".retiraleilao-liberado").hide(300);
    	$(".retiraleilao-encerrado").hide(300);
    	$(".retiraleilao-suspenso").show(300);
    });
    $("#leilao-encerrado").click(function() {
    	$(".retiraleilao-em-breve").hide(300);
		$(".retiraleilao-suspenso").hide(300);
    	$(".retiraleilao-liberado").hide(300);
    	$(".retiraleilao-encerrado").show(300);
    });
});