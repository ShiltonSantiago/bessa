
<!DOCTYPE html>
<html lang="pt_BR">
<head>
   <!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
   <!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
   <!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
   <!--[if (gte IE 9)|!(IE)]><!--><!--<![endif]-->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Bessa Leilões</title>
   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/css/lightbox.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/css/bootstrap.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/js/datetime/jquery.datetimepicker.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.css')}}">
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/external/jquery/jquery.js')}}"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script> -->
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/modernizr.custom.29473.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/scripts.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cidades.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/login.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cep.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/busca_avancada.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/lance.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery.maskedinput.js')}}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaBens.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaLotes.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/tabelaTextos.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaDocumentos.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaRelatorio.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaClientes.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/jquery.maskMoney.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tinymce/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/tinyInit.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/datetime/jquery.datetimepicker.full.js') }}"></script>
     
   <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.7.1/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.min.css"/>

<!-- 
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.rtl.min.css"/>
</head>
<body>

<div class="container">
         @yield('conteudoPainel')
 </div>  

   
</body>
</html>

