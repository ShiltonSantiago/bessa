
<table class="table table-striped">
   <thead>
      <tr>
         <th>Cliente</th>
         <th><i class="fa fa-calendar" aria-hidden="true"></i> Data do Lance</th>
         <th>Valor do Lance</th>
         <th>Nota de arrematação</th>
      </tr>
   </thead>
   <tbody>
   @foreach($lances as $lance)
      <tr>
       <td>{{ $lance->nom_cliente}}</td>
        <td> {{ date('d/m/Y', strtotime($lance->created_at)) }} </td>
         <td>R$ {{ $lance->valor_lance}}</td>
         <td><a href=""><i class="fa fa-files-o" aria-hidden="true"></i></a></td>
      </tr>
@endforeach
   </tbody>
</table>


