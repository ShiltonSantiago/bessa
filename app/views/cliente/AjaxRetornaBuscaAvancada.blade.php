

<div class="container">

 @if( empty($anuncios) )

         <div class="alert alert-danger text-center" role="alert">
         <span><h4> Desculpe, mas não temos bens para o filtro pesquisado por você. <i class="fa fa-frown-o" aria-hidden="true"></i>
          <br>
         </h4> </span></div>

@else

     <div class="row todos-leiloes">


            @foreach($anuncios as $anuncio)

               <div class="col-sm-4 col-md-3 col-xs-12"> 
                  <div class="single-leilao-home">
                     <!--<div class="estado-leilao">
                     </div>
                     <div class="estado-leilao-icon">
                        <i class="fa fa-gavel" aria-hidden="true"></i>
                     </div>-->
                     <div class="clearfix"></div>
                     <a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}">
                        <div class="imagem-leilao" id="imagem-leilao">
                           <div class="image-destaque-lote">
                              @if(empty($anuncio->cod_foto))
                                Imagem não cadastrada!
                             @else
                                 <img src="{{ URL('public/imagens/'.$anuncio->dsc_foto.'')}}"  alt="">
                              @endif
                           </div>
                           <div class="leilao-hover">
                              <i id="flutuar-lei" class="fa fa-gavel" aria-hidden="true"></i>
                           </div>
                        </div>
                     </a>
                     <div class="title-leilao">
                        <h3><a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}">{{ $anuncio->nom_produto}}</a></h3>
                        @if( $anuncio->ind_finalizado == "S" && $anuncio->cod_produto == "795")
                           <span class="label label-danger">Suspenso</span>
                  @elseif( $anuncio->ind_finalizado == "S")
                     <span class="label label-danger">Vendido</span>
                        @endif
                     </div>
                     <center>
                     <div class="info-leilao">
                     {{ $anuncio->dsc_home}}
                     <br>
                     {{ $anuncio->titulo_lote}}
                     </div>
                     </center>
                     <div class="botao-leilao-home">
                        <a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}" name="botao-ok" id="botao-leilao-home">Mais Detalhes</a>
                     </div>
<div class="codigo-tipo-leilao">
                        <div class="codigo-leilao tipo-leilao">
                           <p>Cód: <b>{{ $anuncio->cod_produto}}</b></p>
                        
                        <p>{{ $anuncio->dsc_tpo_leilao}}</p>
                           <p>

              @if($anuncio->ind_exibir_praca =="S")
                  1º Praça em: {{ date('d/m/Y', strtotime($anuncio->dat_final_1)) }} às {{ date('H:i', strtotime($anuncio->dat_final_1)) }}h
              @else
                  Dia do leilão: {{ date('d/m/Y', strtotime($anuncio->dat_final_1)) }} às {{ date('H:i', strtotime($anuncio->dat_final_1)) }}h
              @endif
                        <br>
                     
                   @if($anuncio->ind_exibir_praca =="S")
                      2º Praça em: {{ date('d/m/Y', strtotime($anuncio->dat_final_2)) }} às {{ date('H:i', strtotime($anuncio->dat_final_2)) }}h
                          @endif</p>
                           </div>

                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
               @endforeach
               
               @endif

            </div>
   </div>

