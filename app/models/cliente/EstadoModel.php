<?php

namespace app\models\cliente;

class EstadoModel extends \Eloquent{

	protected $table = 'tb_estado';
	protected $guarded = [];
	protected $primaryKey = 'cod_estado';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    
}

