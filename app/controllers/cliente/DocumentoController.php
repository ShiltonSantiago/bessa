<?php

namespace app\controllers\cliente;
use \app\models\cliente\DocumentoModel as DocumentoModel;
use \app\models\cliente\UserModel as UserModel;


class DocumentoController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}



	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$id = \Input::get('cod_cliente');
		$tipo = \Input::get('cod_tpo_documento');
		$doc = \Input::file('dsc_documento');


					$destinationPath = 'public/documentos';
					$extensoes_permitidas = array('pdf', 'doc','jpg','jpeg');
					$extension = $doc->getClientOriginalExtension();
					$fileName = rand(1111,9999).'.'.$extension;			
					$doc->move($destinationPath, $fileName);
					$dados = [ 'cod_cliente' => $id,'dsc_documento' => $fileName , 'cod_tpo_documento' => $tipo ];	
					$salvarDoc = new DocumentoModel($dados);
					$add = $salvarDoc->save();	

					//colocar usuario com documentos pendente
					$attributes = [ 'ind_aceito_documento' => "P" ];
					$alterado = UserModel::where('cod_cliente',$id)->update($attributes);

				return \Redirect::to('/home');

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{

		$clientes = UserModel::all();

		 $data = [ 'clientes' => $clientes ];

        return \View::make('cliente.painel.documentos', $data);
	}



	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getParecerDoc($id,$status)
	{
		//Aceito ou negado / vazio é pendente
		if($status =="S"){
			$parecer = "A";
		}elseif($status =="N"){
			$parecer = "N";
		}else{
			$parecer = "P";
		}
 
	    $datHoje = date('Y/m/d');
		$attributes = [ 'ind_aceito_documento' => $parecer , 'dat_aprovacao_doc' => $datHoje ];

		$alterado = UserModel::where('cod_cliente',$id)->update($attributes);

		if($alterado){

				return \Redirect::to('/documento/show');

				}else{
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-error" role="alert">Ocorreu um erro, tente novamente!</div>');
				}

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
