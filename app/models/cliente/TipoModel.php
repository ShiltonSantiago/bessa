<?php

namespace app\models\cliente;

class TipoModel extends \Eloquent{

	protected $table = 'tb_tipo_leilao';
	protected $guarded = [];
	protected $primaryKey = 'cod_tpo_leilao';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }

    
}

