@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Listagem de Noticias</h1>

<hr>

<a href="{{ URL('noticia')}}"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>Cadastrar novo</a>

<hr>

<table class="table table-striped" id="tabela_noticias">
                  <thead>
                     <tr>
                        <th>Codigo</th>
                        <th>Titulo</th>
                        <th>Data Cadastro</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($noticias as $noticia)
                     <tr>
                        <td>{{ $noticia->cod_noticia}}</td>
                        <td>{{ $noticia->nom_noticia}}</td>
                        <td>{{ date('d/m/Y', strtotime($noticia->dat_cadastro)) }}</td>
                         <td>
			                     <a href="{{ URL('noticia/'.$noticia->cod_noticia.'/edit')}}">
			                     <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
			          	      </td> 
                        <td>
                        {{ Form::open([ 'method' => 'DELETE', 'route' => [ 'noticia.destroy',$noticia->cod_noticia ] ]) }}
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Excluir
                          </button>
                            {{ Form::close() }}
                        </td>

                     </tr>
                     @endforeach
                  </tbody>

               </table>


   </div>
  </div>



  

@stop
