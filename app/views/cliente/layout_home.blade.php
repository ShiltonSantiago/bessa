<!DOCTYPE html>
<html lang="pt_BR">
<head>
   <!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
   <!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
   <!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
   <!--[if (gte IE 9)|!(IE)]><!--><!--<![endif]-->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Bessa Leilões</title>
   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/css/bootstrap.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.css')}}">
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/external/jquery/jquery.js')}}"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script> -->
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/modernizr.custom.29473.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/scripts.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cidades.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/login.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cep.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/busca_avancada.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/lance.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery.maskedinput.js')}}"></script>
    <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery.maskedinput.js')}}"></script>
    <script type="text/javascript">
      jQuery(function($){
       $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
       $("#num_telefone").mask("(99) 9999-9999");
       $("#num_celular").mask("(99) 99999-9999");
       $("#num_cep").mask('99999-999');
       $("#cpf").mask("999.999.999-99");
       $("#data").mask("99/99/9999");
    });
    </script>
  
   
   <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.7.1/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.min.css"/>

<!-- 
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.rtl.min.css"/>
</head>
<body>



   <!--======= TOP BAR =========-->
    <div class="top-bar">
        <div class="container">
            <ul class="left-bar-side ul-conta">
                   <nav class="navbar nav-conta">
                    <div class="container">
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle menu-conta" data-toggle="collapse" data-target="#my1Navbar">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                      </div>
                      <div class="collapse navbar-collapse" id="my1Navbar">
                        <ul class="nav navbar-nav">   
            @include('cliente.menus')
                        </ul></div></div></nav>

            </ul>
            <ul class="hidden-xs right-bar-side social_icons">
                  <li class="face"> <a href="https://www.facebook.com/bessaleiloes/" target="blank"><i class="fa fa-facebook"></i></a></li>
                  <li class="twit"> <a href="https://www.instagram.com/bessaleiloes/" target="blank"><i class="fa fa-instagram"></i></a></li>
         </ul>

        </div>
    </div>

   <!--======= HEADER =========-->
   <header>
      <div class="section1">
         <div class="container">

            <!--======= LOGO =========-->
            <div class="col-sm-4">
               <div class="row">
                  <div class="logo">
                           <a href="{{ URL('/')}}" title="logo">
                              <img src="{{ URL('frameworks/cliente/images/logo.png')}}"  alt="logo" />
                           </a>
                     </div>
                  </div>
               </div>

               <!--======= PUBLICIDADE HEADER =========-->           
            <div class="col-sm-8 hidden-xs">
               <div class="row">
                  <div class="ad-header">
                    <div class="col-sm-6">
                      <div class="row">
                        <a href="#" target="blank">
                          <img src="{{ URL('frameworks/cliente/images/anuncio1.jpg')}}"  alt="" />
                        </a>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="row">
                        <a href="#" target="blank">
                          <img src="{{ URL('frameworks/cliente/images/anuncio1a.jpg')}}"  alt="" />
                        </a>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>

            <!--======= PESQUISA =========-->
            <div class="pesquisa-home col-xs-12">
              <div class="pesquisa-home-form ">
                  <form method="post" id="form_busca_avancada">
                    <div class="col-md-3">
                      <select class="form-control" name="cidade" id="cidade">
                        <option value="">Selecione a cidade</option>
                          @foreach($cidades as $cidade)
                        <option value="{{ $cidade->cod_cidade}}">{{ $cidade->nome_cidade }}</option>
                            @endforeach
                      </select>
                    </div>
                    
                    <div class="col-md-2">
                      <select class="form-control" name="categoria" id="categoria">
                        <option value="">Categoria</option>
                          @foreach($categoria as $categ)
                        <option value="{{ $categ->cod_categoria}}"> {{ $categ->dsc_categoria}} </option>
                          @endforeach
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select class="form-control" name="categoria" id="categoria">
                        <option value="">Modalidade</option>
                          @foreach($modalidades as $modalidade)
                        <option value="{{ $modalidade->cod_tpo_leilao}}"> {{ $modalidade->dsc_tpo_leilao}} </option>
                          @endforeach
                      </select>
                    </div>


                    <div class="col-md-3">
                      <select class="form-control" name="tipo" id="tipo">
                        <option value="">Tipo</option>
                          @foreach($tipo as $tipos)
                        <option value="{{ $tipos->cod_tpo_leilao }}">{{ $tipos->dsc_tpo_leilao }} </option>
                          @endforeach
                      </select>
                    </div>

                    <div class="col-md-1">
                     <input class="submit" type="submit" value="BUSCAR">
                    </div>
                    <div class="clearfix"></div>
                  </form>
              </div>
            </div>
          </div>  

      <!--======= NAV =========-->
      <nav class="navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="{{ URL('/')}}">Home</a></li>
              <li><a href="{{ URL('funciona')}}">Como Participar</a></li>
               <li><a href="{{ URL('leiloeiro')}}">O Leiloeiro</a></li>
               <li><a href="{{ URL('noticias')}}">Notícias</a></li> 
               <li><a href="{{ URL('contato')}}">Fale Conosco</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="menu-ao-vivo" data-toggle="modal" data-target="#myModalAoVivo"><a href="#">Ao Vivo</a></li>
            </ul>
          </div>
        </div>
      </nav></div>
   </header>
   <!--======= FIM DO HEADER =========-->


  <!-- retorno do filtro avancado -->
  <div id="resultado_filtro_avancado"></div>


 <!--  pagina inicial que será removida no momento da busca avancada -->
  <div id="filtro_fixo">

   <!--======= SLIDER =========-->
   <div class="section2">
      <div class="slide-principal">
         <div class="carousel fade-carousel slide" id="myCarousel" data-ride="carousel" data-interval="6000" id="bs-carousel"> 
           <!-- Indicators -->
           <ol class="carousel-indicators">
             <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
             <li data-target="#myCarousel" data-slide-to="1"></li>
             <li data-target="#myCarousel" data-slide-to="3"></li>
           </ol>
           <!-- Wrapper for slides -->
           <div class="carousel-inner" role="listbox">
             <div class="item active">
               <img src="{{ URL('frameworks/cliente/images/slidea.jpg')}}"  alt="Bessa Leilões">
             </div>

             <div class="item">
               <img src="{{ URL('frameworks/cliente/images/slideb.jpg')}}"  alt="Bessa Leilões">
             </div>

             <div class="item">
               <a href="x"><img src="{{ URL('frameworks/cliente/images/slideguia.jpg')}}"  alt="Guia do Leiloeiro"></a>
             </div>

           </div>
           <!-- Left and right controls -->
           <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
             <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
             <span class="sr-only">Previous</span>
           </a>
           <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
             <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
             <span class="sr-only">Next</span>
           </a>
         </div>
      </div>
   </div>

   <!--======= LEILÕES =========-->
   <div class="section4">
      <div class="leiloes">
         <div class="container">
            <div class="col-xs-12">
               <div class="row">
                  <div class="filtro-leiloes-home">
                     <ul>
                        <li id="leilao-todos">Todos</li>
                        <li id="leilao-liberado">Liberados</li>
                        <li id="leilao-embreve">Em Breve</li>
                        <li id="leilao-suspenso">Suspensos</li>
                        <li id="leilao-encerrado">Encerrados</li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="clearfix"></div>

            <div class="row todos-leiloes">

            @foreach($lotes as $lote)
              <center>
                <h4></h4>
              </center>

               <div class="col-sm-4 col-md-3 col-xs-12 retiraleilao-{{ $lote->status->dsc_icon}}"> 
                  <div class="single-leilao-home">
                  <div class="estado-leilao-{{ $lote->status->dsc_icon}}">{{ $lote->status->dsc_status}} </div>
                  <div class="estado-leilao-icon-{{ $lote->status->dsc_icon}}">
                    <i class="fa fa-gavel" aria-hidden="true"></i>
                  </div>
                     <div class="clearfix"></div>
                     <a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}">
                        <div class="imagem-leilao" id="imagem-leilao">
                          <div class="image-destaque-lote">
                           <div class="img-dest-lote">

                           <img src="{{ URL('public/imagens/'.$lote->img_lote.'')}}"  alt="">
                           </div>
                           </div>
                           <div class="leilao-hover">
                              <i id="flutuar-lei" class="fa fa-gavel" aria-hidden="true"></i>
                           </div>
                        </div>
                     </a>
                     <div class="title-leilao tilte-leilao-lotes">
                        <h3><a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}"><small>LOTE:</small><br/>
                        {{ $lote->titulo_lote}} - ( {{ $lote->produtos->count() }} ) <br> 
                        <hr><small>Início em: {{ date('d/m/Y', strtotime($lote->dat_inicial)) }} 
                      <br/>Modalidade: <b>{{ $lote->modalidade->dsc_tpo_leilao }} </b></small>
                           </a>
                         </h3> 
                     </div>
                     <div class="botao-leilao-home">
                        <a href="{{ URL('detalhes/lote/'.$lote->cod_lote.'')}}" name="botao-ok" id="botao-leilao-home">Mais Detalhes</a>
                     </div>

                  </div>
               </div>
               @endforeach

            </div>

         </div>
      </div>
   </div>

   </div> <!-- fechando fixo -->

   <!--======= ANÚNCIO BODY =========-->
   <div class="section5">
      <div class="anuncio-body">
         <div class="container">
            <div class="col-xs-12">
               <div class="row">
                  <div class="ad-body">
                     <a href="{{ URL('contato')}}">
                        <img src="{{ URL('frameworks/cliente/images/ad-2.jpg')}}"  alt="logo" />
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <!--======= NOTÍCIAS =========-->

   <div class="section6">
      <div class="noticias">
         <div class="leiloes">
            <div class="container">
               <div class="col-xs-12">
                  <div class="row">
                     <div class="noticias-home">
                        <h3>Notícias</h3>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="row todas-noticias">  
                  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  @foreach($noticias as $noticia)
                  <div class="col-md-4 col-sm-6 noticia-page-single">
                    <div class="panel panel-default">
                      <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                          <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $noticia->cod_noticia}}" aria-expanded="false" aria-controls="{{ $noticia->cod_noticia}}">
                          <div class="imagem-noticia" id="imagem-noticia">
                                  <img src="{{ URL('public/imagens/'.$noticia->img_noticia.'')}}" alt="">
                                  <div class="noticia-hover">
                                    <i id="flutuar-not" class="fa fa-plus-square-o" aria-hidden="true"></i>
                                  </div>
                                </div>
                                <h3>
                            {{ $noticia->nom_noticia}}
                            </h3>
                          </a>
                        </h4>
                      </div>
                      <div id="{{ $noticia->cod_noticia}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                         {{ $noticia->dsc_noticia}}
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


<!-- Modal Login -->

<div class="modal fade" id="myLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><img src="{{ URL('frameworks/cliente/images/logo.png')}}" alt=""><h4 class="modal-title" id="myModalLabel"></h4></center>
      </div>
      <div class="modal-body">

      <form method="post" id="form_login">
  <div class="form-group">
    <label for="exampleInputEmail1">E-mail</label>
    <input type="email" class="form-control" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Senha</label>
    <input type="password" class="form-control" id="senha" placeholder="">
  </div>
      <div style="margin:15px auto; text-align:right;"><a href="#">Esqueci minha senha.</a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Limpar</button>
        <button type="submit" class="btn btn-primary">Entrar</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- modal streawng video -->

 <div class="modal fade" id="myModalAoVivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Leilão Ao Vivo</h4>
      </div>
      <div class="modal-body">
    No momento não há leilões ao vivo !
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


   <!--======= FOOTER =========-->
   <footer>
      <div class="footer">
         <div class="container">
            <div class="col-sm-12 col-md-4"> 
               <div class="logo-footer">
                     <a href="{{ URL('/')}}" title="logo">
                        <img src="{{ URL('frameworks/cliente/images/logo-footer.png')}}" alt="logo" />
                     </a>
                     <h2>Leiloeiro Oficial Paulo Bessa</h2>
                  <h3>Telefone: 33 98852-7310   <br/>contato@bessaleiloes.com.br</h3>
                   </div>
               </div>
            <div class="col-sm-6 col-md-4">
               <div class="menu-footer">
                  <ul>
                     <li><a href="{{ URL('privacidade')}}">POLÍTICA DE SEGURANÇA</a></li>
                     <li><a href="{{ URL('seguranca')}}">DICAS</a></li>
                     <li><a href="{{ URL('termos')}}">REGRAS</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="news-footer">
                     <a href="#" target="blank">
                        <img src="{{ URL('frameworks/cliente/images/ad-3.jpg')}}" alt="" />
                     </a>
               </div>   
            </div>

         </div>
      </div>
      <div class="assinatura">
         <center>COPYRIGHTS © 2016 BESSA LEILÕES. TODOS OS DIREITOS RESERVADOS</a></center>
         <div class="icons-footer">
            <ul class="social-icons-footer">
                  <li class="face"> <a href="https://www.facebook.com/bessaleiloes/"><i class="fa fa-facebook"></i></a></li>
                  <li class="twit"> <a href="https://www.instagram.com/bessaleiloes/"><i class="fa fa-instagram"></i></a></li>
            </ul>
         </div>
      </div>
   </footer>


<script type="text/javascript">
 $("#form_login").submit(function(e){
            e.preventDefault();

            var email = $("#email").val();
            var senha = $("#senha").val();
            var dataString = 'email='+email+'&senha='+senha;
            var urlconf = "{{ URL('logar')}}";
            
            $.ajax({
                type: "POST",
                url : urlconf,
                dataType: "json",
                data : dataString,
                beforeSend: function() {
                       //console.log("carregando");
                    },
                success: function(data) {

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;
                        url_retorno = "{{ URL('home')}}";

                        if(retorno_processo == "logado"){
                                alertify.notify(retorno_msg, 'success', 3);
                                setTimeout(function(){
                                  window.location.href = url_retorno;
                                 }, 10);              
                        }else{
                            alertify.notify(retorno_msg, 'error', 6);
                            }

                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function() {

                },
     });
  });
</script>


   
</body>
</html>

