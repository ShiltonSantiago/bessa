$("document").ready(function(){

        //criar conta
        $("#form_nova_conta").submit(function(e){
            e.preventDefault();

            var form  = $("#form_nova_conta");
            
            $.ajax({
                type: "POST",
                url : "criarnovaconta",
                dataType: 'json',
                data : form.serialize(),
                beforeSend: function() {
                       console.log("carregando");
                    },
                success: function(data) {
                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;


                        if(retorno_processo == "sucesso"){
                                alertify.notify(retorno_msg, 'success', 3);
                                setTimeout(function(){
                                    //window.location.href= 'home';
                                  window.location.href= 'home';
                                 }, 10); 
                        }else if(retorno_processo == "email"){
                                alertify.notify(retorno_msg, 'error', 3); 
                                console.log("ok");            
                        }else if(retorno_processo == "senha"){
                                alertify.notify(retorno_msg, 'error', 3);                 
                        }else{
                            alertify.notify(retorno_msg, 'error', 3);
                            }
            
                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function(data) {
                    console.log(data);

                },
     });
  });
}); 
