@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    <div class="col-xs-12">
      <div class="row">

  @if(\Auth::user()->ind_ativo == "N")

    <div class="alert alert-danger" role="alert">
     Enviamos uma mensagem de confirmação para {{ \Auth::user()->username }}, confira na sua caixa de e-mails e siga as instruções para concluir o seu cadastro.
     <br>
     Se você não recebeu a mensagem, por favor verifique na sua caixa de spam.
   </div>
@endif



@if(\Auth::user()->ind_aceito_documento == "F" || \Auth::user()->ind_aceito_documento == "P")

  @if(\Auth::user()->ind_ativo == "S")

{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'documento.store']) }}
   <fieldset>

   <!-- Form Name -->
   <legend>Para concluir o seu cadastro é necessário que você envie uma cópia dos documentos listados abaixo:</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif


    <div>
      <p>
        <span style="color:#000;">Pessoa física:</span><br/>
        a) Carteira de Identidade (RG) ou documento equivalente (documento de identidade expedido por entidades de classe, tais como OAB, CREA, CRM e outras, ou pelas Forças Armadas do Brasil);<br/>
        b) Cadastro de Pessoa Física (CPF);<br/>
        c) RG ou documento equivalente e nome e CPF do cônjuge, se for o caso;<br/>
        d) comprovante de residência em nome do arrematante (conta de água, luz ou telefone);<br/>
      </p>
      <p>
        <span style="color:#000;">Pessoa jurídica:</span><br/>
        a) comprovante de inscrição e de situação cadastral no Cadastro Nacional da Pessoa Jurídica (CNPJ);<br/>
        b) contrato social, até a última alteração, ou Declaração de Firma Individual;<br/>
        c) Carteira de Identidade (RG) ou documento equivalente (documento de identidade expedido por entidades de classe, tais como OAB, CREA, CRM e outras, ou pelas Forças Armadas do Brasil) e Cadastro de Pessoa Física (CPF) do representante legal ou do preposto da pessoa jurídica;
      </p>
	  
	  <h4><i class="fa fa-print" aria-hidden="true"></i><a href="#." id="btn_imprimir_contrato"> Imprimir Contrato</a></h4>

	  
    </div>


    <div class="col-md-8 col-md-offset-2">

<div class="form-group">
    <input type="hidden" value="{{ \Auth::user()->cod_cliente }}" name="cod_cliente" id="cod_cliente" >
  </div>

<div class="form-group">
    <label for="exampleInputFile">Selecione o tipo da documentação que deseja enviar </label>
     <select class="form-control" name="cod_tpo_documento" id="cod_tpo_documento" required="">
             <option value=""></option>
                       @foreach($tiposDocs as $docs)
              <option value="{{ $docs->cod_tpo_documento}}">{{ $docs->dsc_documento }}</option>
                      @endforeach
    </select>
</div>

  <div class="form-group">
     {{ Form::file('dsc_documento', ['class' => 'input-file','required' => 'required']) }}
    <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
  Enviar arquivos em formato: jpg, pdf ou doc.</p>
  </div>


  <hr>
  <p>Se você não recebeu o e-mail com a cópia do contrato para assinar, <a href="http://www/bessaleiloes.com.br/bessa/public/termo.pdf">Clique Aqui</a>.</p>

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Enviar documento',['class' => 'btn btn-primary']) }}
     </div>
   </div>
   @endif
</div>


<!-- Documentação aceita -->
@elseif(\Auth::user()->ind_aceito_documento == "A") 

    <div class="alert alert-success" role="alert">
        Cliente ativo, pode dar lances a vontade.
       </div>


@endif


      </div>
    </div>
  </div>

@if(\Auth::user()->ind_aceito_documento !="F") )

  <div class="container page-full">
    <div class="col-xs-12">
      <div class="row">
      <h3><p class="text-center"><strong>Documentos já enviados por você</strong></p></h3></legend>

      <table class="table table-striped">
   <thead>
      <tr>
        <th>Tipo de Documento</th>
         <th>Visualizar Documento</th>
         <th>Data de Envio</th>
         <th>Status</th>
      </tr>
   </thead>
   <tbody>
@foreach($documento as $doc)
      <tr>
       <td> {{ $doc->tipoDocumento->dsc_documento }}</td>
         <td><a href="{{ URL('public/documentos/'.$doc->dsc_documento.'')}}" target="_blank">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
              </a>
        </td>
        <td> {{  date('d/m/Y', strtotime( $doc->dat_envio ))  }}</td>
         <td>
          @if(\Auth::user()->ind_aceito_documento == "A")
            Documentação Aprovada
          @elseif(\Auth::user()->ind_aceito_documento == "N")  
            Documentação Reprovada
           @else
           Em análise
           @endif
            </td>
      </tr>
@endforeach
   </tbody>
</table>


      </div>
    </div>
  </div>

  @endif
  
  <!--dados de contrato-->
  
		
  <div id="div_conteudo_contrato" style="display:none;">

	<center><img src="http://www.bessaleiloes.com.br/frameworks/cliente/images/logo.png" /></center>
<h3 style='text-align: center;'>Termo de serviço</h3>
<br>
<br>
O presente <strong>CONTRATO DE ADESÃO</strong> descreve os <strong>TERMOS</strong> e <strong>CONDIÇÕES</strong> para a participação do usuário nos leilões disponíveis no “site”. Ao clicar nos botões de Pessoa Física ou Pessoa Jurídica, o usuário <strong>DECLARA ESTAR CIENTE E DE ACORDO</strong> com as condições estabelecidas neste contrato. Qualquer dúvida não hesite em contatar o Bessa Leilões através do atendimento “CONTATO”, ou ligando para o telefone: (33) 98852-7310.
<br><br>
1) Para estar apto a dar ofertas/lances para a aquisição dos bens (veículos, imóveis, máquinas, equipamentos e outros bens) expostos no “site” www.bessaleiloes.com.br , o usuário deverá ter capacidade civil para contratar, nos termos da legislação em vigor. Menores de idade de 18 anos e os que são considerados absolutamente incapazes para realizar quaisquer atos da vida civil são impedidos de adesão e negociação.
<br><br>
2) Para participação nos leilões divulgados neste “site”, o usuário deverá fornecer seus dados pessoais para cadastramento, sendo essencial, para tanto, preencher todos os campos de forma clara e precisa. Sempre que necessário, o cadastro do usuário deverá ser atualizado.
<br><br>
3) Ao se cadastrar o usuário indicará um nickname (apelido) para sua identificação quando der lances nos leilões oferecidos no site, bem como e-mail, uma senha pessoal e intransferível, a qual não poderá ser utilizada para outras finalidades não autorizadas.
<br><br>
4) O login a ser cadastrado pelo usuário para acesso ao site será o seu E-mail e não poderá guardar semelhança com o nome do “site” ou leiloeiro ou com o nome das empresas proprietárias dos bens divulgados no “site”, como também não poderá ser cadastrado login considerado ofensivo ou que contenha dados pessoais do usuário, alguma URL ou endereço eletrônico.
<br><br>
5) Caso o usuário não queira ser reconhecido pelos demais usuários do Site deverá optar em usar o nickname, caso contrário junto ao seu lance será informado o seu e-mail.
<br><br>
6) Para segurança do usuário, sua senha e dados serão transmitidos criptografados (Certificado de Segurança SSL - Secure Socket Layer).
<br><br>
7) O usuário se compromete a não divulgar a sua senha a terceiros. No caso de uso não autorizado da senha, o usuário deverá enviar, imediatamente, um e-mail à Central de Atendimento do site comunicando o fato, sob pena de incorrer nas perdas e danos cabíveis.
<br><br>
<strong>8) O usuário cadastrado no “site” autoriza expressamente a verificação de seus dados junto aos órgãos de proteção ao crédito. </strong>
<br><br>
9) Verificada inconsistência nos dados informados pelo usuário e/ou pendência financeira, a Central de Atendimento do Bessa Leilões entrará em contato via e-mail ou telefone.
<br><br>
<strong>10) Após feito o cadastro o usuário deverá enviar pelo próprio “site” seus documentos digitalizados, juntamente com o termo de compromisso assinado e com reconhecimento de firma ou autenticação. </strong>
<br><br>
<strong>11) Assim que concluso o cadastro a equipe do Bessa Leilões terá até 72 horas para analisar e HOMOLOGAR o cadastro. SOMENTE após homologação o usuário será liberado para participar dos leilões. </strong>
<br><br>
12) Os leilões serão realizados por Leiloeiro Oficial legalmente habilitado para o exercício das funções em data, horário e local previamente determinados e divulgados em editais publicados em jornais de grande circulação, no SITE, em Redes Sociais e através de Email Marketing.
<br><br>
13) Os lances poderão ser ofertados através do SITE www.bessaleiloes.com.br e/ou presencialmente (na data do encerramento do leilão).<strong> Os lances ofertados são IRREVOGÁVEIS e IRRETRATÁVEIS. O usuário é responsável por todas as ofertas registradas em seu nome, e se obriga pelo que os lances não podem ser anulados e/ou cancelados em nenhuma hipótese.</strong>
<br><br>
14) O usuário poderá ofertar mais de um lance para um mesmo bem, prevalecendo sempre o maior lance ofertado. O “site” www.bessaleiloes.com.br permite o recebimento de lances virtuais simultaneamente aos presenciais e em tempo real.
<br><br>
15) Próximo do fechamento do lote, em leilões na modalidade mista (presencial e online), caso algum lance seja recebido no último minuto do fechamento do lote, ou alguma oscilação de acesso internet no local do pregão presencial durante a disputa, poderá ser reaberto o cronômetro em 01 (um) minuto para o encerramento do lote, para que todos os usuários interessados tenham a oportunidade de efetuar novos lances.
<br><br>
16) O Bessa Leilões poderá limitar, cancelar ou suspender o cadastro definitivamente, de qualquer usuário que não cumprir as condições estabelecidas no presente contrato e ainda poderá impedir a participação do usuário nos leilões realizados por este leiloeiro. E o arrematante inadimplente não será admitido a participar de qualquer outro leilão divulgado no “site” www.bessaleiloes.com.br, pelo que seu cadastro ficará bloqueado. Caso sejam identificados cadastros vinculados a este cadastro bloqueado, os mesmos serão igualmente bloqueados.
<br><br>
<strong>17) O arrematante que não enviar seus documentos digitalizados em um prazo máximo de até 90 dias terá seu cadastro inativado.</strong>
<br><br>
18) Os bens (veículos, máquinas, equipamentos e outros bens) a serem levados a leilão estarão em exposição nos locais indicados na descrição de cada lote (no “site”), para visitação dos interessados, nos dias e horários determinados que antecederem os leilões OU por fotos (meramente ilustrativas) exibidas na “homepage” www.bessaleiloes.com.br.
<br><br>
<strong>19) Para aqueles usuários que arrematarem bens através do Leilão Online do “site” www.bessaleiloes.com.brautorizam através do aceite eletrônico deste contrato o LEILOEIRO designado em Edital para assinar os Termos de Arremate e Recibos em seu nome. </strong>
<br><br>
<strong>20) Não há custo algum para o usuário lançar sua(s) oferta(s) no “site” www.bessaleiloes.com.br, a não ser que seu lance seja vencedor, então arcará com a comissão que incidirá sobre o valor do bem e a ser pago juntamente com o valor do bem arrematado e despesas administrativa se houver, cabe ao arrematante o pagamento das despesas administrativas informadas nos editais e anexos, devendo ser paga diretamente ao leiloeiro. </strong>
<br><br>
21) Havendo inadimplência poderá o leiloeiro emitir titulo de crédito para a cobrança de valores e despesas encaminhando a protesto, por falta de pagamento, se for o caso, sem prejuízo da execução prevista no artigo 39, do Decreto nº. 21.981/32.
<br><br>
22) Os bens serão vendidos no estado em que se encontram, sendo que o vendedor se, no caso de veículo responsabiliza pelo pagamento de todos e quaisquer tributos, encargos e multas (inclusive o Imposto sobre a Propriedade de Veículos Automotores - IPVA e o Seguro Obrigatório), pendentes sobre os veículos apregoados, relativos a períodos anteriores à data da venda, EXCETO quando os tributos, encargos, multas, IPVA e o Seguro Obrigatório estejam expressamente previstos no lote, cujo pagamento será de responsabilidade do arrematante.
<br><br>
23) OS BENS SERÃO VENDIDOS NO ESTADO E CONSERVAÇÃO EM QUE SE ENCONTRAM, SEM GARANTIA, inclusive quanto a câmbio e motor (para veículos) que porventura não sejam originais de fábrica, ficando a sua regularização por conta do arrematante, isentando assim o Comitente Vendedor e o Leiloeiro que é mero mandatário, de quaisquer defeitos ou vícios ocultos.
<br><br>
24) Os lotes serão vendidos “um a um”, a quem oferecer maior lance, desde que o valor do lance seja igual ou superior ao preço mínimo determinado pelo vendedor. Todos os lances captados durante o leilão serão inseridos no “site”, possibilitando a todos os usuários o acompanhamento “ONLINE” do leilão.
<br><br>
25) O prazo de entrega da documentação dos veículos arrematados será informada na descrição dos respectivos lotes. A seu exclusivo custo o arrematante assume, a responsabilidade de transferência da propriedade do veículo para o seu nome, no prazo máximo de 30 (trinta) dias contados da data do recebimento dos documentos.
<br><br>
26) O arrematante declara estar ciente de que o veículo está sendo arrematado, no estado que se encontra, sem garantia, revisão e que não está coberto pelo prazo de garantia do fabricante e assim isentando o vendedor e o leiloeiro de qualquer responsabilidade, inclusive por vícios ou defeitos, ocultos ou não, considerando-se que o mesmo teve oportunidade de vistoriar o bem, conforme os editais.
<br><br>
27) O bem arrematado e pago, deverá, obrigatoriamente, ser retirado do pátio do Bessa Leilões, dentro do prazo máximo de 48 (quarenta e oito) horas, no endereço a ser informado na confirmação da arrematação sob pena de ser cobrada uma <strong>taxa de estadia diária de R$ 8,00 (oito reais)</strong>, a ser paga no ato da retirada.
<br><br>
28) As políticas de segurança estão disponíveis na seção “Política de Segurança” do “site”.
<br><br>
29) O Bessa Leilões não se responsabiliza por prejuízos ou quaisquer tipos de danos advindos das transações efetuadas entre os usuários e os vendedores, atuando sempre e tão somente como provedora de espaço virtual para divulgação online de bens, limitando-se a veicular, através de seu “site” específico, os dados dos bens fornecidos pelos vendedores.
<br><br>
30) Os problemas e dúvidas envolvidas com as transações efetuadas durante e depois do leilão serão dirimidas através do Bessa Leilões nos telefones disponíveis na seção CONTATO.
<br><br>
31) O Bessa Leilões a seu exclusivo critério, poderá cancelar qualquer oferta de compra, sempre que não for possível autenticar a identidade do usuário, ou caso este venha a descumprir as condições estabelecidas no presente instrumento.
<br><br>
32) Responderá o usuário criminalmente pelo uso de equipamento, programa ou procedimento que interferir no funcionamento do “site”.
<br><br>
33) Qualquer prejuízo eventualmente acarretado ao usuário, decorrente de dificuldades técnicas ou falhas no sistema da Internet, não poderá ser atribuída a responsabilidade à Bessa Leilões. O Bessa Leilões não garante o acesso contínuo de seus serviços, uma vez que a operação do “site” poderá sofrer interferências acarretadas por diversos fatores fora de seu controle.
<br><br>
34) O Bessa Leilões poderá, a seu livre arbítrio e a qualquer momento, acrescentar, extinguir ou alterar todos ou alguns dos serviços disponíveis no “site”, bem como alterar as condições constantes deste contrato, após a sua divulgação no “site”, ficando desde já estabelecido que as alterações passarão a vigorar no mínimo com antecedência de 05 (cinco) dias da realização do leilão.
<br><br>
35) Este contrato será regido pela legislação brasileira em vigor, ficando desde já eleito o Foro da Comarca de Governador Valadares/MG, como competente para dirimir toda e qualquer questão oriunda do seu cumprimento.
<br>
<br>
<br>

<img src='http://www.bessaleiloes.com.br/frameworks/cliente/images/assinatura.jpg' width='160' height='auto' />
<br>
<strong>Paulo Bessa</strong> <br>
LEILOEIRO OFICIAL
<br><br><br><br>
_______________________________
<br>
{{ \Auth::user()->nom_cliente }}<br>
{{ \Auth::user()->num_cpf}}

  </div>

        <script>
        document.getElementById('btn_imprimir_contrato').onclick = function() {
            var conteudo = document.getElementById('div_conteudo_contrato').innerHTML,
                tela_impressao = window.open('about:blank');

            tela_impressao.document.write(conteudo);
            tela_impressao.window.print();
            tela_impressao.window.close();
        };
        </script>
  

@stop
