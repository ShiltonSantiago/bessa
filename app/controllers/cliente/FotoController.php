<?php

namespace app\controllers\cliente;
use \app\models\cliente\AnuncioModel as AnuncioModel;
use \app\models\cliente\FotoModel as FotoModel;
use Intervention\Image\ImageManagerStatic as Image;
use \app\acme\ImagemAcme as ImagemAcme;

class FotoController extends \BaseController {
	

	//salvar nova foto
	public function store()
	{

		$fileFoto = \Input::file('dsc_foto');
		$bem = \Input::get('cod_produto');

		/*validacao*/
		$rules = ['dsc_foto' => 'required|image' ];
		
		/*mensagem personalizada*/
		$messages = \app\acme\MessagesAcme::Messages();
		$validator = \Validator::make(\Input::all(),$rules,$messages);
		
		if($validator->fails()){
			return \Redirect::back()->withErrors($validator->messages());
		}else{

			/*pegar extensao do arquivo*/
			$extensaoFoto = $fileFoto->getClientOriginalExtension();

			$imagem = new ImagemAcme(\Input::file('dsc_foto'));
			$imagem->setExtensoesAceitas(['jpeg','jpg']);

			if(!$imagem->extensaoPermitida()){
				return \Redirect::back()
				->with(['mensagem' => "<span class='text-danger'>* Extensão não aceita, o formato da imagem deve estar JPEG ou JPG.</span>"]);
	
			}else{

				/*renomear foto*/
				$fotoRenomeada = $imagem->renomearFoto();

				$caminhoFoto = 'public/imagens/'.$fotoRenomeada.'.'.$extensaoFoto;

				/*salva a foto na pasta*/
					$imagem->salvarFoto($caminhoFoto,['largura' => 800,'altura' => 600]);
					$dados = [ 'dsc_foto' => $fotoRenomeada.'.'.$extensaoFoto, 'cod_produto' => $bem ];
					$salvarFoto = new FotoModel($dados);
					$add = $salvarFoto->save();

			if($add){
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>Foto cadastrado com sucesso!</div>');
				}else{
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-error" role="alert">Erro ao salvar, tente novamente!</div>');
				}
		}

	}

}


	//lista as fotos 
	public function edit($id)
	{

		$bem =  AnuncioModel::where('cod_produto','=',$id)->get();

		$fotos = FotoModel::where('cod_produto','=',$id)->get();

		$data = [ 'fotos' => $fotos ,'bem' => $bem ];

		return \View::make('cliente.painel.fotos', $data);
	}
	
	//Excluir foto do anuncio
	 public function destroy($id)
		{
		
	
		$deletado = FotoModel::find($id);
		$deletado->delete();
		
		if($deletado){

				return \Redirect::back()->with('mensagem','<div class="text text-success">Excluido com sucesso!</div>');

		}else{
				return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao excluir, tente novamente!</div>');

		}


	}

	//Selecionar foto padrao do anuncio
	public function getFotoPadrao(){

		$bem = \Input::get('bem');
        $foto = \Input::get('foto');

        //seta todas as fotos para nao padrao
		$attributes = [ 'ind_foto_padrao' => "N" ];
        $alterado = FotoModel::where('cod_produto',$bem)->update($attributes);

        //seta a foto escolhida para padrão
        $attributes = [ 'ind_foto_padrao' => "S" ];
        $alterado = FotoModel::where('cod_produto',$bem)->where('cod_foto',$foto)->update($attributes);

        $retorno = array('msg' => utf8_encode('Alterado com sucesso !'));
        return $retorno; //envia array pro js
	}



}
