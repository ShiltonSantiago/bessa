@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    <div class="col-xs-12">
      <div class="row">

        <div class="col-xs-12 col-sm-6 texto-contato" style="margin-bottom:30px;">
            <p>Se precisar, entre em contato conosco. Estamos prontos para atendê-lo!</p>
          <ul class="con-det">    
            <li> 
              <i class="fa fa-map-marker"></i>
                      <h6>Localização</h6>
                      <p style="margin-left:40px;">Rua Prudente de Morais, 714, <br/>Sala 103, Centro, <br/>Governador Valadares/MG</p>
                    </li>
            <li>
              <i class="fa fa-envelope"></i>
                      <h6>E-mail</h6>
                      <p>contato@bessaleiloes.com.br</p>
                    </li>
            <li>
              <i class="fa fa-phone"></i>
                      <h6>Telefone</h6>
                      <p>33 <b>98852-7310</b></p>
                    </li>
          </ul>
        </div>  
        <div class="col-xs-12 col-sm-6 texto-leiloeiro">
        </div>

        <div class="col-xs-12 col-sm-6 texto-leiloeiro">
          <div role="form" class="wpcf7" id="wpcf7-f54-p8-o1" lang="pt-BR" dir="ltr">
            <form action="/site/contato/#wpcf7-f54-p8-o1" method="post" class="wpcf7-form" novalidate="novalidate">
              <p><span class="wpcf7-form-control-wrap your-name">
                <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required formcontact" aria-required="true" aria-invalid="false" placeholder="Nome" />
              </span> </p>
              <p><span class="wpcf7-form-control-wrap your-email">
                <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email formcontact" aria-required="true" aria-invalid="false" placeholder="E-mail" />
              </span> </p>
              <p><span class="wpcf7-form-control-wrap your-subject">
                <input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text formcontact" aria-invalid="false" placeholder="Assunto" />
              </span> </p>
              <p><span class="wpcf7-form-control-wrap your-message">
                <textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea formcontact" aria-invalid="false" placeholder="Mensagem"></textarea>
              </span> </p>
              <p><input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit formcontact-sub" /></p>
            </form>
          </div>      
        </div>

      </div>
    </div>
  </div>



  

@stop
