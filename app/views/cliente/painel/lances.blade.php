   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">


<form method="post" class="form-horizontal" id="form_validar_lance">
   <fieldset>

   <!-- Form Name -->
   <legend>Buscar Lances</legend>



   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Lote</label>  
     <div class="col-md-5">
     <select name="cod_lote"  id="cod_lote" class="form-control">
     <option value="">Selecione</option>

  @foreach($lotes as $lote)
         <option value="{{ $lote->cod_lote }}">
              {{ $lote->titulo_lote }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>

  

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cid_cidade">Bem</label>
     <div class="col-md-5">
       <select name="cod_produto" class="form-control" id="cod_produto" disabled="disabled">
       </select>
     </div>
   </div>

   </fieldset>
</form>


<div id="retorna">
  
</div>


      </div>
     </div>


<script type="text/javascript">

  $(document).ready(function(){
  $('select[name=cod_lote]').change(function () {
            var idLote = $(this).val();
            $('select[name=cod_produto]').empty();
            $('select[name=cod_produto]').append('<option value="0">Carregando...</option>');

            $.get('../carrega_lotes/' + idLote, function (cidades) {
              $('select[name=cod_produto]').empty();
                $('select[name=cod_produto]').append('<option value="0">Selecione</option>');
                $.each(cidades, function (key, value) {
                    $('select[name=cod_produto]').append('<option value=' + key + '>' + value + '</option>');
                });
                $('select[name=cod_produto]').removeAttr('disabled');
            });
        });
})

</script>



   @stop
