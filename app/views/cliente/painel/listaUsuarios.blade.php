@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Listagem de Clientes</h1>

<hr>

<table class="table table-striped" id="tabela_clientes">
                  <thead>
                     <tr>
                        <th>Nome</th>
                        <th>Editar</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($user as $users)
                     <tr>
                        <td>{{ $users->nom_cliente}}</td>
                         <td>
			              <a href="{{ URL('cliente/'.$users->cod_cliente.'/edit')}}">
			              <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
			          	</td> 
                     </tr>
                     @endforeach
                  </tbody>
               </table>


   </div>
  </div>



  

@stop
