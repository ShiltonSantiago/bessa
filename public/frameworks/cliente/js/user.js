$(document).ready(function(){
	
	var wrapper = $("#wrapper");
	var row = wrapper.find(".row");
	var tabela_administradores = row.find("#tabela-administradores");

	tabela_administradores.on('click','.btn-deletar-administrador' ,function(event){
		var id = $(this).attr('data-id');
		var elemento = event.currentTarget;

		if(confirm('Realmente deseja deletar esse administrador ?')){

			$.ajax({
				url: '/user/destroy/'+id,
				type: 'DELETE',
				beforeSend: function(){
					$(elemento).closest('td').append('<br />deletando registro');
				},
				success: function(data){

					/*pega a linha atual*/
					var linha = $(elemento).closest('tr');
					
					/*se deletou com sucesso*/
					if(data = 'deletado'){

						/*adiciona classe a linha deletada*/	
						$(linha).attr('class','deletado');
							
						/*depois de 1 segundo a linha desaparece*/	
						setTimeout(function(){
							$(linha).fadeOut('slow');
						},1000);
						
						setTimeout(function(){
							window.location.href= '/logoutAdmin';	
						},2000);		

					}

				}
			});

		}else{



		}
		event.preventDefault();
	});


});