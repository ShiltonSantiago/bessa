﻿@extends('cliente.layout')
@section('conteudoPainel')


<script language="Javascript">
window.onload = function () {
setTimeout('$( "#atualizar_val_lance" ).trigger( "click" )', 5000);
} 
</script>


<!-- Inserido por Junior, css slider fotos -->
<style>
  .detalhes-img {height: 355px; overflow: hidden;text-align: center; position: relative;background:#cdcdcd;}
  .detalhes-img img {height:355px;width: auto; position: relative;left: 50%;margin-left: -100%;}
</style>


<?php

// Função de porcentagem: N é N% de X
function porcentagem_nnx ( $valorOriginal ) {
   $valor = $valorOriginal; 
   $percentual = 5.0 / 100.0; // 5%
   $valor_final = $valor + ($percentual * $valor);
   $valor_final_convertido = number_format($valor_final ,2,',', '.' );

    return $valor_final_convertido;
}
?>


<!--======= CONTEÚDO DA PÁGINA =========-->

   <div class="container page-sidebar-single">
      <div class="col-sm-9 col-xs-12">
         <div class="conteudo-single-leilao">
		 @if (Auth::check() )
           @if(Auth::user()->ind_adm == "S")
				<a href="{{ URL('http://bessaleiloes.com.br/detalhes/anuncio/telao/'.$anuncio[0]->cod_produto.'')}}" target="_blank">
					<i class="fa fa-television fa-2x" aria-hidden="true"></i></a>
		   @endif
		  @endif
            <div class="col-xs-12">
               <div class="linha1 row">
                  <div class="ref-leilao col-sm-6">Cód.: {{ $anuncio[0]->cod_produto }}<br/>
                     <a href="{{ URL('detalhes/lote/'.$anuncio[0]->cod_lote.'')}}">Lote: {{ $anuncio[0]->titulo_lote }}</a>
                  </div>
                  <div class="lance-atual col-sm-6">Lance atual: R$ <span id="val_lance_atual">

                  @if(empty($lanceAtual))
                      0,00
                     @else
                  {{ number_format( $lanceAtual,2,',', '.' ) }}
                  @endif

                   </span> 
                     <br/>
                     <a href="#" id="atualizar_val_lance" data-produto="{{ $anuncio[0]->cod_produto }}">
                        <i class="fa fa-refresh" aria-hidden="true"> ATUALIZAR O VALOR DO LANCE</i>
                     </a>
                  </div>
                  <div class="clearfix"></div>
                  <div class="single-titulo-leilao"><h2>{{ $anuncio[0]->nom_produto }}</h2></div>
               </div>

               <div class="linha2 row">
                  <div class="col-sm-6 single-imagem-leilao">
                     <div class="row">
                        <div class="image-destaque-leilao">
                           <div class="col-xs-15" style="background:#fff;">

                        	   <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                              <!-- imagens -->
                              <div class="carousel-inner" role="listbox">

                               <!--  <div class="item active">
                                  <div class="detalhes-img">
                                    <a href="http://bessaleiloes.com.br/public/imagens/2258.jpg" data-lightbox="roadtrip">
                                      <img src="http://bessaleiloes.com.br/public/imagens/2258.jpg" alt="">
                                    </a>
                                  </div>
                                </div> -->

						{{-- */    $bolAtivo = true;    /* --}}

								@foreach($fotos as $foto)

                {{-- */   
                               if($bolAtivo)
                          {
                              $class = 'item active';
                              $bolAtivo = false;
                          }else{
                                $class = "item";
                          }

                /* --}}
                                <div class="{{ $class }}">
                                  <div class="detalhes-img">
                                    <a href="{{ URL('public/imagens/'.$foto->dsc_foto.'')}}" data-lightbox="roadtrip">
                                      <img src="{{ URL('public/imagens/'.$foto->dsc_foto.'')}}" alt="">
                                    </a>
                                  </div>
                                </div>


						@endforeach
								
                              </div>

                              <!-- Controles -->
                              <a style="width:50px" class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a style="width:50px" class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div>


                        	</div>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6 single-lances-leilao">
                     <div class="single-content-leilao row">
                        <h4>Descrição</h4>
                        
                        <p>

                           @if($anuncio[0]->ind_exibir_praca == "S")
                          1º Praça: 
                              <b><big>{{ date('d/m/Y', strtotime($anuncio[0]->dat_final_1)) }}</big></b> às {{ date('H:i', strtotime($anuncio[0]->dat_final_1)) }}h</span>
                          @else
                              Dia: 
                              <b><big>{{ date('d/m/Y', strtotime($anuncio[0]->dat_final_1)) }}</big></b> às {{ date('H:i', strtotime($anuncio[0]->dat_final_1)) }}h</span>
                          @endif
                              <br>
                           
                          @if($anuncio[0]->ind_exibir_praca == "S")
                              2º Praça: {{ date('d/m/Y', strtotime($anuncio[0]->dat_final_2)) }} às
                              {{ date('H:i', strtotime($anuncio[0]->dat_final_2)) }}h
                          @endif


                           </p>
                          <div class="local-evento">
                          <p>Local do Evento:</p> <b>{{ $anuncio[0]->dsc_local_lote }}</b>
                          </div>
                          <p>

                          {{ $anuncio[0]->dsc_produto }} 
                          </p>
                        
                     </div>
                     <div class="single-form-lance-leilao row">
                        <div class="preco-inicial col-xs-6">
                           <div class="row">
                           @if($anuncio[0]->ind_exibir_praca == "S")
                              <p>1º Praça Lance Inicial: <br/>R$<span> {{ number_format( $anuncio[0]->lan_inicial_1,2,',', '.' ) }} </span></p>
                           @else
                              <p>Lance Inicial: <br/>R$<span> {{ number_format( $anuncio[0]->lan_inicial_1,2,',', '.' ) }} </span></p>
                           @endif
                              
                             @if($anuncio[0]->ind_exibir_praca == "S")
                              <p>2º Praça Lance Inicial: <br/>R$<span> {{ number_format( $anuncio[0]->lan_inicial_2,2,',', '.' ) }} </span></p>
                              @endif
                           </div>
                        </div>
                        <div class="preco-atual col-xs-6">
                           <div class="row">
                              <p>Lance Atual: <br/> R$ <span id="val_lance_atual_2">
                                 @if(empty($lanceAtual))
                                    0,00
                                    @else
                                  {{ number_format( $lanceAtual ,2,',', '.' ) }}
                                 @endif
                              </span></p>
                           </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-xs-12 combo-junior">
                           
                     @if (Auth::check() )

                     @if( $anuncio[0]->ind_finalizado == "N" && $anuncio[0]->cod_status == "8" && Auth::user()->ind_adm == "S" && $statusBem[0]->cod_status <> 10)
                           
                        <form method="post" id="form_lance_input" data-cliente="{{\Auth::user()->cod_cliente}}" data-produto="{{ $anuncio[0]->cod_produto }}" >
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Digite o valor do seu lance</label>
                                      <div class="input-group">
                                        <div class="input-group-addon">R$ </div>
                                        <input type="text" name="val_lance_input" id="val_lance_input" class="form-control">
                                      </div>
                                 </div>
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                  </div>
                                
                           </form>

                          <form method="post" id="form_lance_select" data-cliente="{{\Auth::user()->cod_cliente}}" data-produto="{{ $anuncio[0]->cod_produto }}" >
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Selecione o valor do seu lance</label>
                                    <select name="val_lance_select" id="val_lance_select" class="form-control">
                                    @foreach($comboLances as $comboLance)
                                        <option value="{{ $comboLance }}">R$ {{ number_format( $comboLance ,2,',', '.' ) }} </option>
                                   @endforeach
                                </select>
                                 </div>
                                  <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                  </div>
                                
                           </form>
                        
                        @elseif($anuncio[0]->ind_finalizado == "N" && $anuncio[0]->cod_status == "8" && $statusBem[0]->cod_status <> 10)
						

                         <form method="post" id="form_lance_select" data-cliente="{{\Auth::user()->cod_cliente}}" data-produto="{{ $anuncio[0]->cod_produto }}" >
                                 <div class="form-group">
                                  <label for="exampleInputEmail1">Selecione o valor do seu lance</label>
                                    <select name="val_lance_select" id="val_lance_select" class="form-control">
                                    @foreach($comboLances as $comboLance)
                                        <option value="{{ $comboLance }}">R$ {{ number_format( $comboLance ,2,',', '.' ) }} </option>
                                   @endforeach
                                </select>

                                  <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Confirmar</button>
                                  </div>
                                </div>
                           </form>
                           

                           @else
                           <center>
                           <h5><span class="label label-danger">Item não disponível para lances</span></h5>
                           </center>
                         @endif


                     @else
                          <div class="col-xs-12" style="color:#fff;background:#002b5e; padding:15px;text-align:center; text-transform:uppercase;" data-toggle="modal" data-target="#myLogin">
                             Você precisa estar logado para participar
                          </div>
                     @endif
                          <div class="clearfix"></div>
                          <div class="visible-xs-block" style="background:#48c04e; padding: 10px 15px; margin-top:30px; margin-bottom:30px; color:#fff; border-radius:3px; text-align:center;">
                            <a style="padding-top:7px; padding-bottom:7px; color:#fff;" href='whatsapp://send?text=http://bessaleiloes.com.br/bessa/public<?php echo $_SERVER["REQUEST_URI"];?>'><i class="fa fa-whatsapp" aria-hidden="true"></i> COMPARTILHAR</a>
                          </div>
                        </div>
                     </div>
                  </div>
               </div>   

                 @if(Auth::check())
                    @if( $anuncio[0]->ind_finalizado == "N" && $anuncio[0]->cod_status == "8" && Auth::user()->ind_adm == "S" && $statusBem[0]->cod_status <> 10)
                        <form method="post" id="form_encerrar_lance"  data-produto="{{ $anuncio[0]->cod_produto }}" >
                               <div class="form-group">
                                     <button type="submit" class="btn btn-danger">Encerrar</button>
                              </div>
                       </form>
                  @endif
                @endif

               <div class="linha3 row">
                  <h4>Últimos Lances</h4>
                  <div class="conteudo-linha">
                  
                  @if( empty($lances) )

                     <h5><span class="label label-danger">Item ainda sem lances</span></h5>

                  @else

                  <table class="table table-striped" id="tabela_lance_ajax">
                  <thead>
                     <tr>
                        <th>Nº. do Lance</th>
                        <th>Lance (R$)</th>
                        <th>Lance + 5% (R$)</th>
                        <th>Usuário</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($lances as $lance)
                     <tr>
                        <th scope="row">{{ $lance->cod_lance}}</th>
                        <td>{{ number_format( $lance->valor_lance ,2,',', '.' ) }}</td> 
                        <td>{{ porcentagem_nnx($lance->valor_lance) }}</td>
                        <td>{{ $lance->nickname}}</td>
                     </tr>
                     @endforeach


                  </tbody>
               </table>

               @endif

                     <div class="clearfix"></div>
                  </div>
               </div><div class="clearfix"></div>

               <div class="linha4 row">
                  <h4>Observações adicionais</h4>
                  <div class="conteudo-linha">
                     <p>
                        {{ $anuncio[0]->obs_produto }}
                     </p>
                     <br/><br/>
					<div class="botoes-adicionais">
	                     <a href="{{ URL('detalhes/lote/'.$anuncio[0]->cod_lote.'')}}">
	                     	<i class="fa fa-location-arrow" aria-hidden="true"></i> Ver outros leilões do mesmo lote</a><div class="visible-xs-block"><br/></div>
	                     <a href="{{ URL('public/imagens/'.$anuncio[0]->pdf_lote.'')}}" target="_blank">
	                     	<i class="fa fa-file-pdf-o" aria-hidden="true"></i> Ver o edital</a>
                     </div>
                     <!-- <div class="botoes-adicionais">
                        <button type="button" onclick="alert('Conteúdo')">Ver outros leilões do mesmo lote</button>
                        <button type="button" onclick="alert('Conteúdo')">Visualizar condições de venda</button>
                        <button type="button" onclick="alert('Conteúdo')">Catálogo</button>
                        <button type="button" onclick="alert('Conteúdo')">Edital</button>
                     </div> -->
                  </div>
               </div>
               <div class="linha5 row">
                  <h4>Visitação</h4>
                  <div class="conteudo-linha">
                     {{ $anuncio[0]->dsc_visitacao }}
                  </div>
               </div>
               <div class="linha6 row">
                  <h4>Forma de pagamento</h4>
                  <div class="conteudo-linha">
                     {{ $anuncio[0]->dsc_forma_pag }}
                  </div>
               </div>
               <center><div class="small">Dados sujeitos a alteração. Vistoria física obrigatória, conforme condições de venda.</div></center>
            </div>
         </div>
      </div>
      <div class="clearfix visible-xs-block"></div>
      <div class="col-sm-3">
         <!-- SIDEBAR -->

        <div class="redes-sociais-lateral"> 
           <a href="http://www.facebook.com/sharer.php?u=http://bessaleiloes.com.br/bessa44/public/leiloeiro" target="blank"><span class="fa fa-facebook"></span></a>
           <a href="#" onclick="javascript:window.print();"><span class="fa fa-print"></span></a>
           <span class="fa fa-envelope-o"></span>

        </div>


        <div class="lista-lateral">
  	  
  				@if($anuncio[0]->url_video && $anuncio[0]->ind_exibir_video == "S")
  					  {{ $anuncio[0]->url_video }}
				 
  				@endif
  				
  				
  				
  <!-- NOVA BARRA LATERAL -->

            <div class="lista-proximos-leiloes" style="margin:30px 0;">
              
			  				<center><h6>Próximos bens desse lote </h6></center>
              
	@foreach($benslotes as $benslote)
              <div class="row">
                <hr>
                <a href="#." class="abre_proximo" data-produto="{{ $benslote->cod_produto }} "> 
                  <div class="col-md-4 col-sm-12 col-xs-3">
                    <div class="bens-da-lateral">
					@if(empty($benslote->fotos[0]))
					@else
                      <img src="{{ URL('public/imagens/'.$benslote->fotos[0]->dsc_foto.'')}}" alt="" width="100%" height="auto" >
					@endif
                    </div>
                  </div> 
                  <div class="col-md-8 col-sm-12 col-xs-9">
  				  
                    <div class="row">
                      <h6 style="text-transform:none !important;">
  					{{ $benslote->nom_produto }} 
                             / 
                             @if($benslote->ind_finalizado == "S")
                               Vendido
                             @else
                               Em oferta
                             @endif
  					           </h6>
                    </div>
                  </div>
                </a>
              </div>
		@endforeach
			  



            </div>

        </div>

      </div>


   </div>


   <!--======= FIM DO CONTEÚDO DA PÁGINA =========-->




    <script type="text/javascript">
        $(document).ready(function(){
              $("#val_lance_input").maskMoney({showSymbol:true, symbol:"", decimal:".", thousands:""});
        });
    </script>


@stop