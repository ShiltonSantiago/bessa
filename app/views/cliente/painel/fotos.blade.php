@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Fotos do bem: {{ $bem[0]->nom_produto }} </h1>

<hr>

@foreach($fotos as $foto)
<div class="col-md-2">
	<div class="panel panel-default">
		  <div class="panel-heading">
			{{ Form::open([ 'method' => 'DELETE', 'route' => [ 'fotos.destroy',$foto->cod_foto ] ]) }}
                        <button type="submit" class="btn btn-default">
                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Excluir
                          </button>
             {{ Form::close() }}
</div>
		  <div class="panel-body">
			<img width="100" height="100"  src="{{ URL('public/imagens/'.$foto->dsc_foto.'')}}">
        <div class="checkbox">
              <label>

               @if($foto->ind_foto_padrao == "S")
                   <input type="checkbox"  checked="checked" id="seleciona_img_padrao"  data-produto="{{ $bem[0]->cod_produto }}" data-foto="{{ $foto->cod_foto }}"> 
                @else
                  <input type="checkbox"  id="seleciona_img_padrao"  data-produto="{{ $bem[0]->cod_produto }}" data-foto="{{ $foto->cod_foto }}"> 
                @endif
                Imagem Inicial do Bem
              </label>
        </div>
		  </div>
	</div>
</div>
 @endforeach
 

@if($fotos->count() == 5)

<hr>
	
<div class="col-md-12">
		<div class="alert alert-danger" role="alert">Não é possivel inserir mais de 5 imagens por bem.</div>
</div>

	@else
		
<div class="col-md-12">
{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'POST', 'route' => 'fotos.store']) }}
   <fieldset>

   
   {{ Form::hidden('cod_produto', $bem[0]->cod_produto ) }}
   
   <!-- Form Name -->
   <legend>Cadastrar Foto</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

  

    <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_foto">Imagem</label>
     <div class="col-md-5">
     {{ Form::file('dsc_foto', ['class' => 'input-file']) }}
     {{ $errors->first('dsc_foto') }} 
     </div>
   </div>


   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}

</div>

@endif


   </div>
  </div>

@stop
