﻿   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">
<br/><br/>


{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['lotes.update',$lote->cod_lote]]) }}
   <fieldset>

   <!-- Form Name --><br><br>
   <legend>Alterar dados do  Lote</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Titulo do Lote</label>  
     <div class="col-md-5">
   {{ Form::text('titulo_lote', $lote->titulo_lote,['class' => 'form-control input-md']) }} {{ $errors->first('titulo_lote') }}
     </div>
   </div>

   <!-- Text input
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">1º Praça Data Inicial</label>  
     <div class="col-md-5">
      {{ Form::text('dat_inicial_1', $lote->dat_inicial_1,['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_inicial_1') }}
     </div>
   </div>-->

   <!-- Text input
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">2º Praça Data Inicial</label>  
     <div class="col-md-5">
      {{ Form::text('dat_inicial_2', $lote->dat_inicial_2,['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_inicial_2') }}
     </div>
   </div>-->

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Data leilão/1ª Praça</label>   
     <div class="col-md-5">
     {{ Form::text('dat_final_1', $lote->dat_final_1,['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_final_1') }}
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Data 2ª Praça</label>  
     <div class="col-md-5">
     {{ Form::text('dat_final_2', $lote->dat_final_2,['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_final_2') }}
     </div>
   </div>

       <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Exibir dados da 2º Praça ?</label>
     <div class="col-md-5">
 
 <select name="ind_exibir_praca" class="form-control">
        @if($lote->ind_exibir_praca == "S") 
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
       @else
       <option value="N"> Nao </option>
       <option value="S"> Sim </option>
      @endif
  </select>

     </div>
   </div>

      <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cid_cidade">Cidade</label>
     <div class="col-md-5">

       <select name="cidade"  id="cidade" class="form-control">
  @foreach($cidade as $cid)
         <option value="{{ $cid->cod_cidade }}" @if($cid->cod_cidade == $lote->cidade) 
              selected="selected" @endif >{{ $cid->nome_cidade }}
         </option>
 @endforeach 
  </select>

       </select>
     </div>
   </div>

   

   <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Imagem Lote</label>
     <div class="col-md-5">
     {{ Form::file('img_lote', ['class' => 'input-file']) }}
     </div>
   </div>

   

      <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_status">Status</label>
     <div class="col-md-5">

<select name="cod_status" class="form-control">
  @foreach($status as $sta)
         <option value="{{ $sta->cod_status }}" @if($sta->cod_status == $lote->cod_status) 
              selected="selected" @endif >{{ $sta->dsc_status }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>


      <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_status">Modalide</label>
     <div class="col-md-5">

<select name="cod_tpo_leilao" class="form-control">
  @foreach($modalidades as $modalidade)
         <option value="{{ $modalidade->cod_tpo_leilao }}" @if($modalidade->cod_tpo_leilao == $lote->cod_tpo_leilao) 
              selected="selected" @endif >{{ $modalidade->dsc_tpo_leilao }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>

    <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir</label>
     <div class="col-md-5">
 
 <select name="ind_exibir" class="form-control">
        @if($lote->ind_exibir == "S") 
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
       @else
       <option value="N"> Nao </option>
       <option value="S"> Sim </option>
      @endif
  </select>

     </div>
   </div>

   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_lote">Informações</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_lote', $lote->dsc_lote, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

    <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_local_lote">Local do Evento</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_local_lote', $lote->dsc_local_lote, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Ao Vivo</label>  
     <div class="col-md-5">
   {{ Form::text('url_video', $lote->url_video,['class' => 'form-control input-md']) }} {{ $errors->first('url_video') }}
     </div>
   </div>


       <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Exibir Video Ao Vivo ?</label>
     <div class="col-md-5">
 
 <select name="ind_exibir_video" class="form-control">
        @if($lote->ind_exibir_video == "S") 
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
       @else
       <option value="N"> Nao </option>
       <option value="S"> Sim </option>
      @endif
  </select>

     </div>
   </div>


    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Nome do Credor</label>  
     <div class="col-md-5">
       {{ Form::text('credor_nome', $lote->credor_nome,['class' => 'form-control input-md']) }} {{ $errors->first('credor_nome') }}
     </div>
   </div>

    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Agencia Credor</label>  
     <div class="col-md-5">
        {{ Form::text('credor_agencia', $lote->credor_agencia,['class' => 'form-control input-md']) }} {{ $errors->first('credor_agencia') }}
     </div>
   </div>

    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Conta Credor</label>  
     <div class="col-md-5">
      {{ Form::text('credor_conta', $lote->credor_conta,['class' => 'form-control input-md']) }} {{ $errors->first('credor_conta') }}
     </div>
   </div>




   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar alterações ',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}

<br/><br/>

      </div>
     </div>



  <script>

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});

$("#datetimepicker_format_change").on("click", function(e){
  $("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
  $.datetimepicker.setLocale($(e.currentTarget).val());
});

$('.datas').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:  '1986/01/05'
});

</script>



   @stop
