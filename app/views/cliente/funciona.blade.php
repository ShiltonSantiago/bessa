@extends('cliente.layout')
@section('conteudoPainel')


<!--======= CONTEÚDO DA PÁGINA =========-->

	<div class="index-page">
		<div class="container">
			<div class="clearfix"></div>
			<div class="como-participar">

				<div class="img-participar">
					<img src="{{ URL('frameworks/cliente/images/logo-icon.png')}}" alt="">
				</div>
				<div class="menu-participar">
					<ul>
						<li><a href="#o-que-e">O que é um leilão</a></li>
						<li><a href="#como-participar">Como participar</a></li>
						<li><a href="#dar-lance">Dar seu lance e comprar</a></li>
						<li><a href="#finalizar">Finalizar sua compra</a></li>
					</ul>
				</div>

				<div class="col-sm-10 col-sm-offset-1"> 
					<div id="o-que-e">
					<hr>
						<h3>Você sabe o que é um leilão?</h3>
						
						<p>É uma venda pública, onde vence que ofertar o maior lance acima do valor mínimo estipulado.<br/>
						E um lance, caso você não saiba, é o valor ofertado pelo interessado, podendo estar presente ou de forma <i>online</i>, através do site Bessa Leilões, em um bem que está sendo leiloado.</p>

					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1"> 
					<div id="como-participar">
					<hr>
						<h3>Conheça os passos para participar de um leilão</h3>
						
						<div class="passo">Passo 1 <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<div class="clearfix"></div>
						<p>Se você ainda não é cadastrado, acesse <a href="{{ URL('conta')}}"><span><i class="fa fa-lock"></i> Registrar</span></a> no canto superior esquerdo da página, preencha o formulário e valide seu cadastro aceitando as “Condições de Venda” gerais do leilão.</p>

						<div class="passo">Passo 2 <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<div class="clearfix"></div>
						<p>Em seguida, você será direcionado para a página de “Dados Cadastrais", onde deverá enviar uma cópia escaneada e legível dos seus documentos (CPF, RG, Comprovante de Endereço e o Termo de Adesão assinado e autenticado). Por fim, clique no botão "Enviar arquivos".</p>

						<div class="passo">Passo 3 <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<div class="clearfix"></div>
						<p>Após analisados e aceitos os documentos a Equipe de Atendimento do Bessa Leilões entrará em contato via e-mail ou telefone para informar que você foi habilitado para participar dos leilões.</p>
					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1"> 
					<div id="dar-lance">
					<hr>
						<h3>Dar o seu lance e comprar</h3>
						<div class="passo">Escolher o produto <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<p>Se você já é cadastrado, faça o login no site com seu usuário e senha. Encontre em nossa página inicial o bem que procura ou pesquise no campo "Buscar". Você pode localizar bens por categoria, modalidade, cidade e tipo. É tudo muito simples.</p>
						<div class="passo">Ofertar o lance <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<p>Depois de escolher o bem que deseja adquirir, clique em "Mais Detalhes" para ter acesso a todas as informações daquele produto. Você pode enviar o seu lance diretamente na pagina do bem.</p>
						<div class="passo">Arrematar <span class="fa fa-gavel" aria-hidden="true"></span></div>
						<p>Quando o leiloeiro bater o martelo, o maior lance arremata o bem.</p>

					</div>
				</div>

				<div class="col-sm-10 col-sm-offset-1"> 
					<div id="finalizar">
					<hr>
						<h3>Finalizar a comprar</h3>
						<p>Se você foi o vencedor e arrematou o bem, você receberá um e-mail de confirmação contendo as instruções para pagamento e retirada.<br/> 
						Acessando a página <a href="#"><span><i class="fa fa-money"></i> Meus Lances</span></a> você poderá acompanhar o <i>status</i> dos seus arremates.</p>
					</div>
				</div>


				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<!--======= FIM DO CONTEÚDO DA PÁGINA =========-->




@stop
