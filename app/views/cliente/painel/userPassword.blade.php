@extends('cliente.layout')

@section('conteudoPainel')

<div class="container">
    <div class='row'>
    <div class="col-md-8 col-md-offset-2">

{{ Form::open(['role' => 'form', 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['senha.update',$idUser] ]) }}

<fieldset>

<!-- Form Name -->
<legend><h3>Alterar Senha</h3></legend>

  @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

<div class="col-md-8 col-md-offset-2">
<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="SENHA">SENHA ATUAL</label>  
  <div class="col-md-6">
  {{ Form::password('atual',['class' => 'form-control']) }} {{ $errors->first('atual') }} 

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="SENHA">NOVA SENHA</label>  
  <div class="col-md-6">
  {{ Form::password('nova',['class' => 'form-control']) }} {{ $errors->first('nova') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-6 control-label" for="SENHA">CONFIRME A NOVA SENHA</label>  
  <div class="col-md-6">
  {{ Form::password('confirmNova',['class' => 'form-control']) }} {{ $errors->first('confirmNova') }} 
  </div>
</div>



<!-- Button -->
<div class="form-group">
  <label class="col-md-6 control-label" for="SALVAR"></label>
  <div class="col-md-6">
   {{ Form::submit('Salvar',['class' => 'btn btn-success']) }}
  </div>
</div>

</div>
</fieldset>
{{ Form::close() }}
    </div>
  </div><br/><br/>
</div>

@stop

    


