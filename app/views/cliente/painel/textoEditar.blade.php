   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">


{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['texto.update',$textos[0]->cod_informacao]]) }}
   <fieldset>

   <!-- Form Name -->
   <legend>Alterar Textos / Slides / Banners</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif
	
	
	@if($textos[0]->anexo == "N")

			   <!-- Select Basic -->
		   <div class="form-group">
			 <label class="col-md-4 control-label" for="cid_cidade">Tipo</label>
			 <div class="col-md-5">

			   <select name="cod_tpo_informacao"  id="cod_tpo_informacao" class="form-control">
		  @foreach($tiposSimples as $tipos)
		 				 <option value="{{ $tipos->cod_tpo_informacao }}" @if($tipos->cod_tpo_informacao == $textos[0]->cod_tpo_informacao) 
					  selected="selected" @endif >{{ $tipos->dsc_tpo_informacao }}
				 </option>
		 @endforeach 
		  </select>
		  

			   </select>
			 </div>
		   </div>

   
   @else
   
			  <!-- Select Basic -->
		   <div class="form-group">
			 <label class="col-md-4 control-label" for="cid_cidade">Tipo</label>
			 <div class="col-md-5">

			   <select name="cod_tpo_informacao"  id="cod_tpo_informacao" class="form-control">
		  @foreach($tiposUpload as $tiposUploder)
				 <option value="{{ $tiposUploder->cod_tpo_informacao }}" @if($tiposUploder->cod_tpo_informacao == $textos[0]->cod_tpo_informacao) 
					  selected="selected" @endif >{{ $tiposUploder->dsc_tpo_informacao }}
				 </option>
		 @endforeach 
		  </select>

			   </select>
			 </div>
		   </div>
   
   @endif 
   

   	@if($textos[0]->anexo == "S")
   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Link da Imagem</label>  
     <div class="col-md-5">
   {{ Form::text('link', $textos[0]->link,['class' => 'form-control input-md']) }} {{ $errors->first('link') }}
     </div>
   </div>
@endif
   
   @if($textos[0]->anexo == "S")
   <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Imagem</label>
     <div class="col-md-5">
     {{ Form::file('img_texto', ['class' => 'input-file']) }}
     </div>
   </div>
@endif

@if($textos[0]->anexo == "N")
   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-1 control-label" for="dsc_lote">Informações</label>
     <div class="col-md-11">                     
        {{ Form::textarea('dsc_informacao', $textos[0]->dsc_informacao, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>
@endif
   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar alterações ',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}



      </div>
     </div>



   @stop
