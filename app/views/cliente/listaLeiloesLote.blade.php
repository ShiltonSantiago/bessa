@extends('cliente.layout')

@section('conteudoPainel')



<div class="section4">
      <div class="leiloes">
         <div class="container">
     <div class="row todos-leiloes">

	 

         @if(empty($anuncios))
          <center>
                         <div class="alert alert-danger" role="alert">Este lote ainda não possui bens cadastrados.</div>
          </center>
         @else

         <center>
            <h4>{{ $anuncios[0]->lote->titulo_lote}}</h4>
            <h3><div style="width:100%;text-align:center;border-radius:15px;" class="estado-leilao-{{ $anuncios[0]->lote->status->dsc_icon}}">
            {{ $anuncios[0]->lote->status->dsc_status}} </div></h3>
            <br/><br/>
            <div class="observacao-lote" style="width:80%;text-align:center;margin-top:30px; margin-bottom:30px;">
               {{ $anuncios[0]->lote->dsc_lote}}    
            </div>
            <hr>
            <br/><br/>

         </center>

            @foreach($anuncios as $anuncio)

               <div class="col-sm-4 col-md-3 col-xs-12"> 
                  <div class="single-leilao-home">
                     <!--<div class="estado-leilao">
                     </div>
                     <div class="estado-leilao-icon">
                        <i class="fa fa-gavel" aria-hidden="true"></i>
                     </div>-->
                     <div class="clearfix"></div>
                     <a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}">
                        <div class="imagem-leilao" id="imagem-leilao">
                           <div class="image-destaque-lote">
                              @if(empty($anuncio->fotos[0]))
                                Imagem não cadastrada!
                             @else
                                 <img src="{{ URL('public/imagens/'.$anuncio->fotos[0]->dsc_foto.'')}}"  alt="">
                              @endif
                           </div>
                           <div class="leilao-hover">
                              <i id="flutuar-lei" class="fa fa-gavel" aria-hidden="true"></i>
                           </div>
                        </div>
                     </a>
                     <div class="title-leilao">
                        <h3><a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}">{{ $anuncio->nom_produto}}</a></h3>
                        @if( $anuncio->cod_status =="10")
                           <span class="label label-danger">Suspenso</span>
						@elseif( $anuncio->ind_finalizado == "S")
							<span class="label label-danger">Vendido</span>
                        @endif
                     </div>
                     <center>
                     <div class="info-leilao">
                     {{ $anuncio->dsc_home}}
                     <br>
                     {{ $anuncio->lote->titulo_lote}}
                     </div>
                     </center>
                     <div class="botao-leilao-home">
                        <a href="{{ URL('detalhes/anuncio/'.$anuncio->cod_produto.'')}}" name="botao-ok" id="botao-leilao-home">Mais Detalhes</a>
                     </div>
<div class="codigo-tipo-leilao">
                        <div class="codigo-leilao tipo-leilao">
                           <p>Cód: <b>{{ $anuncio->cod_produto}}</b></p>
                        
                        <p>{{ $anuncio->tipoLeilao->dsc_tpo_leilao}}</p>
                           <p>

              @if($anuncio->lote->ind_exibir_praca =="S")
                  1º Praça em: {{ date('d/m/Y', strtotime($anuncio->lote->dat_final_1)) }} às {{ date('H:i', strtotime($anuncio->lote->dat_final_1)) }}h
              @else
                  Dia do leilão: {{ date('d/m/Y', strtotime($anuncio->lote->dat_final_1)) }} às {{ date('H:i', strtotime($anuncio->lote->dat_final_1)) }}h
              @endif
                        <br>
                     
                   @if($anuncio->lote->ind_exibir_praca =="S")
                      2º Praça em: {{ date('d/m/Y', strtotime($anuncio->lote->dat_final_2)) }} às {{ date('H:i', strtotime($anuncio->lote->dat_final_2)) }}h
                          @endif</p>
                           </div>

                        <div class="clearfix"></div>
                     </div>
                  </div>
               </div>
               @endforeach
               
               @endif

            </div>
        </div>
      </div>
  </div>

            @stop