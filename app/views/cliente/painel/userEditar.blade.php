@extends('cliente.layout')

@section('conteudoPainel')



<div class="container">
    <div class='row'>
		<div class="col-md-offset-2 col-md-8">

<fieldset>

<!-- Form Name -->
<legend><h3>Alterar meus dados</h3></legend>

	@if(Session::has('mensagem'))
			{{ Session::get('mensagem') }}
		@endif

{{ Form::open(['role' => 'form', 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['cliente.update',$user->cod_cliente]]) }}

{{ Form::hidden('cod_tpo_pessoa', $user->cod_tpo_pessoa) }}


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email"> E-MAIL</label>  
  <div class="col-md-6">
  {{ Form::text('username',$user->username,array('class' => 'form-control','readonly' => 'readonly')) }} 
  {{ $errors->first('username') }}

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nom_cliente">SEU NOME</label>  
  <div class="col-md-6">
  {{ Form::text('nom_cliente',$user->nom_cliente,['class' => 'form-control']) }} {{ $errors->first('nom_cliente') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nom_cliente">APELIDO</label>  
  <div class="col-md-6">
  {{ Form::text('nickname',$user->nickname, ['class' => 'form-control']) }}
  {{ $errors->first('nickname') }}
  </div>
</div>

@if($user->cod_tpo_pessoa == "1")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nom_mae">NOME DA MAE</label>  
  <div class="col-md-6">
  {{ Form::text('nom_mae',$user->nom_mae,['class' => 'form-control']) }} {{ $errors->first('nom_mae') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">RG</label>  
  <div class="col-md-6">
  {{ Form::text('num_rg',$user->num_rg,['class' => 'form-control']) }} {{ $errors->first('num_rg') }}
  </div>
</div>

@endif

@if($user->cod_tpo_pessoa == "1")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">CPF</label>  
  <div class="col-md-6">
  {{ Form::text('num_cpf',$user->num_cpf, array('class' => 'form-control' , 'id' => 'cpf' ,'readonly' => 'readonly')) }} 
  {{ $errors->first('num_cpf') }}
  </div>
</div>

@endif

@if($user->cod_tpo_pessoa == "2")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">CNPJ</label>  
  <div class="col-md-6">
    {{ Form::text('cnpj',$user->cnpj, array('class' => 'form-control' , 'id' => 'cnpj' ,'readonly' => 'readonly')) }} 


  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">INSCRIÇÃO ESTADUAL</label>  
  <div class="col-md-6">
    {{ Form::text('insc_estadual',$user->insc_estadual, ['class' => 'form-control']) }}  

  {{ $errors->first('insc_estadual') }} 
  </div>
</div>
@endif

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">DATA NASCIMENTO</label>  
  <div class="col-md-6">
  {{ Form::text('dat_nasc',$user->dat_nasc,['class' => 'form-control','id' => 'data']) }} {{ $errors->first('dat_nasc') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">ATIVIDADE PROFISSIONAL</label>  
  <div class="col-md-6">
  {{ Form::text('atv_profissional',$user->atv_profissional,['class' => 'form-control']) }} {{ $errors->first('atv_profissional') }}
  </div>
</div>

@if($user->cod_tpo_pessoa == "2")

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">NOME EMPRESA</label>  
  <div class="col-md-6">
  {{ Form::text('nom_empresa',$user->nom_empresa,['class' => 'form-control']) }} {{ $errors->first('nom_empresa') }}
  </div>
</div>

@endif

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">CEP</label>  
  <div class="col-md-3">
  <input type="text" class="form-control" name="num_cep" value="{{ $user->num_cep }}" name="num_cep" id="num_cep" placeholder="CEP">
  </div>
   <div class="col-md-3">
      <a href="#." id="busca_cep">Buscar Endereco</a>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">ESTADO</label>  
  <div class="col-md-6">
   <input type="text" class="form-control" value="{{ $user->estado }}" name="estado" id="estado" placeholder="ESTADO" readonly="readonly">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">CIDADE</label>  
  <div class="col-md-6">
  <input type="text" class="form-control" value="{{ $user->cidade }}" name="cidade" id="cidade" placeholder="CIDADE" readonly="readonly">
  </div>
</div>


<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">ENDERECO</label>  
  <div class="col-md-6">
  <input type="text" class="form-control" value="{{ $user->end }}" name="end" id="end" placeholder="Endereço - Rua">
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">NUMERO</label>  
  <div class="col-md-6">
  {{ Form::text('num_end',$user->num_end,['class' => 'form-control']) }} {{ $errors->first('num_end') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">COMPLEMENTO</label>  
  <div class="col-md-6">
  {{ Form::text('complemento',$user->complemento,['class' => 'form-control']) }} {{ $errors->first('complemento') }}
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="num_rg">BAIRRO</label>  
  <div class="col-md-6">
  <input type="text" class="form-control" value="{{ $user->bairro }}" name="bairro" id="bairro" placeholder="BAIRRO">
  </div>
</div>




<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cel">TELEFONE FIXO</label>  
  <div class="col-md-4">
  {{ Form::text('num_telefone',$user->num_telefone,['class' => 'form-control' , 'id' => 'num_telefone']) }} {{ $errors->first('num_telefone') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cel">TELEFONE CELULAR</label>  
  <div class="col-md-4">
  {{ Form::text('num_celular',$user->num_celular,['class' => 'form-control', 'id' => 'num_celular']) }} {{ $errors->first('num_celular') }} 
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cel">ESTADO CIVIL</label>  
  <div class="col-md-4">
  <select name="cod_estado_civil" class="form-control">
  @foreach($civils as $civil)
         <option value="{{ $civil->cod_estado_civil }}" @if($civil->cod_estado_civil == $user->cod_estado_civil) 
              selected="selected" @endif >{{ $civil->dsc_estado_civil }}
         </option>
 @endforeach 
  </select>

 

  </div>
</div>





<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="SALVAR"></label>
  <div class="col-md-4">
   {{ Form::submit('Salvar',['class' => 'btn btn-success']) }}
  </div>
</div>

</fieldset>
{{ Form::close() }}
		</div>
	</div>
</div>

@stop




