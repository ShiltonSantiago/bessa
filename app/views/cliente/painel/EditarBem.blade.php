   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">

  <br/><br/>

{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['bens.update',$anuncio->cod_produto]]) }}
   <fieldset>

   <!-- Form Name -->
 
   <legend>Alterar dados do  Bem</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Nome</label>  
     <div class="col-md-5">
   {{ Form::text('nom_produto', $anuncio->nom_produto,['class' => 'form-control input-md']) }} {{ $errors->first('nom_produto') }}
     </div>
   </div>


  
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Texto da Home</label>  
     <div class="col-md-5">
      {{ Form::text('dsc_home',$anuncio->dsc_home,['class' => 'form-control input-md']) }} {{ $errors->first('dsc_home') }}
     </div>
   </div>


    <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Valor Lance Inicial 1º Praça</label>  
     <div class="col-md-5">
      <div class="input-group">
      <div class="input-group-addon">R$ </div>
      {{ Form::text('lan_inicial_1',$anuncio->lan_inicial_1,['class' => 'form-control input-md lance_inicial']) }} {{ $errors->first('lan_inicial_1') }}     </div>
     </div>
   </div>

   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Valor Lance Inicial 2º Praça</label>  
     <div class="col-md-5">
      <div class="input-group">
      <div class="input-group-addon">R$ </div>
      {{ Form::text('lan_inicial_2',$anuncio->lan_inicial_2,['class' => 'form-control input-md lance_inicial']) }} {{ $errors->first('lan_inicial_2') }}     </div>
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_categoria">Categoria</label>
     <div class="col-md-5">

      <select name="cod_categoria" class="form-control">
  @foreach($categoria as $cate)
         <option value="{{ $cate->cod_categoria }}" @if($cate->cod_categoria == $anuncio->cod_categoria) 
              selected="selected" @endif >{{ $cate->dsc_categoria }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>

        <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_status">Status</label>
     <div class="col-md-5">

<select name="cod_status" class="form-control">
  @foreach($status as $sta)
         <option value="{{ $sta->cod_status }}" @if($sta->cod_status == $anuncio->cod_status) 
              selected="selected" @endif >{{ $sta->dsc_status }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_tpo_leilao">Tipo de leilão</label>
     <div class="col-md-5">

 <select name="cod_tpo_leilao" class="form-control">
  @foreach($tipo as $tipos)
         <option value="{{ $tipos->cod_tpo_leilao }}" @if($tipos->cod_tpo_leilao == $anuncio->cod_tpo_leilao) 
              selected="selected" @endif >{{ $tipos->dsc_tpo_leilao }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>


   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Lote</label>  
     <div class="col-md-5">
     <select name="dsc_lote" class="form-control">
  @foreach($lote as $lot)
         <option value="{{ $lot->cod_lote }}" @if($lot->cod_lote == $anuncio->cod_lote) 
              selected="selected" @endif >{{ $lot->titulo_lote }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>



   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_incremento">Incremento</label>  
     <div class="col-md-5">
            {{ Form::text('dsc_incremento',$anuncio->dsc_incremento,['class' => 'form-control input-md']) }} {{ $errors->first('dsc_incremento') }}      
     </div>
   </div>


    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Número Processo</label>  
     <div class="col-md-5">
        {{ Form::text('num_processo', $anuncio->num_processo,['class' => 'form-control input-md']) }} {{ $errors->first('num_processo') }}
     </div>
   </div>
   
   <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Edital</label>
     <div class="col-md-5">
     {{ Form::file('pdf_lote', ['class' => 'input-file']) }}
     </div>
   </div>


   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_produto">Descrição</label>
     <div class="col-md-5">                     
            {{ Form::textarea('dsc_produto', $anuncio->dsc_produto, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="obs_produto">Observações</label>
     <div class="col-md-5">                     
     {{ Form::textarea('obs_produto', $anuncio->obs_produto, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>


   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_visitacao">Localização e Retirada</label>
     <div class="col-md-5">                     
        {{ Form::textarea('dsc_visitacao', $anuncio->dsc_visitacao, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Forma de pagamento</label>
     <div class="col-md-5">

<select name="cod_forma_pag" class="form-control">
  @foreach($pagamento as $pag)
         <option value="{{ $pag->cod_forma_pag }}" @if($pag->cod_forma_pag == $anuncio->cod_forma_pag) 
              selected="selected" @endif >{{ $pag->dsc_forma_pag }}
         </option>
 @endforeach 
  </select>

     </div>
   </div>

    <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir</label>
     <div class="col-md-5">
 
 <select name="ind_ativo" class="form-control">
        @if($anuncio->ind_ativo == "S") 
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
       @else
       <option value="N"> Nao </option>
       <option value="S"> Sim </option>
      @endif
  </select>

     </div>
   </div>



   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar alterações ',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}


<br/><br/>
      </div>
     </div>


<script>
  $( function() {
    $(".datas").datepicker({
            dateFormat: 'yy/mm/dd',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
          });
  } );
  </script>

  <script type="text/javascript">
        $(document).ready(function(){
              $(".lance_inicial").maskMoney({showSymbol:true, symbol:"", decimal:".", thousands:""});
        });
    </script>



   @stop
