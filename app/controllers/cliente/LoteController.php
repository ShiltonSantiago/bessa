<?php

namespace app\controllers\cliente;
use \app\models\cliente\LoteModel as LoteModel;
use \app\models\cliente\StatusModel as StatusModel;
use \app\models\cliente\ModalidadeModel as ModalidadeModel;
use \app\models\cliente\EstadoModel as EstadoModel;
use \app\models\cliente\CidadeModel as CidadeModel;


class LoteController extends \BaseController {


	//form de inserir novo lote
	public function index()
	{
		$cidades = CidadeModel::orderBy('nome_cidade')->where('cod_estado', '=', '11')->get()->lists('nome_cidade','cod_cidade');
		$status = StatusModel::orderBy('dsc_status')->whereIn('cod_status', array(8, 9, 10,11))->get()->lists('dsc_status','cod_status');
		$modalidades = ModalidadeModel::where('ind_ativo', '=', 'S')->orderBy('dsc_tpo_leilao')->get()->lists('dsc_tpo_leilao','cod_tpo_leilao');

		$data = [  'status' => $status , 'modalidades' => $modalidades , 'cidades' => $cidades ];

		return \View::make('cliente.painel.novo_lote')->with($data);

	}

	//listar todos lotes

	public function show()
	{
		$lotes = LoteModel::orderBy('ind_exibir','desc')->paginate(15);

		$data = [
					'lotes' => $lotes->getCollection(), 
					'links' => $lotes->links()
				];

		return \View::make('cliente.painel.lotes', $data);
	}



	//salvar novo lote
	public function store()
	{
		$fileFoto = \Input::file('img_lote');
		

		if(!empty($fileFoto)){
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
		}else{
			$fileNameFoto = "";
		}

					$dados = [
								'titulo_lote' => \Input::get('titulo_lote'),
								'img_lote' => $fileNameFoto,
								'cod_status' => \Input::get('cod_status'),
								'dsc_lote' => \Input::get('dsc_lote'),
								'cod_tpo_leilao' => \Input::get('cod_tpo_leilao'),
								'ind_exibir' => \Input::get('ind_exibir'),
								'url_video' => \Input::get('url_video'),
								'credor_nome' => \Input::get('credor_nome'),
								'credor_agencia' => \Input::get('credor_agencia'),
								'credor_conta' => \Input::get('credor_conta'),
								'dat_inicial_1' => \Input::get('dat_inicial_1'),
								'dat_final_1' => \Input::get('dat_final_1'),
								'dat_inicial_2' => \Input::get('dat_inicial_2'),
								'cidade' => \Input::get('cidade'),
								'ind_exibir_praca' => \Input::get('ind_exibir_praca'),
								'dsc_local_lote' => \Input::get('dsc_local_lote'),
								'dat_final_2' => \Input::get('dat_final_2'),
								'ind_exibir_video' => \Input::get('ind_exibir_video')
							
								
							];

			$salvarBem = new LoteModel($dados);
			$add = $salvarBem->save();

		if($add){
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>Lote cadastrado com sucesso!</div>');
				}else{
					return \Redirect::back()->with('mensagem',
						'<div class="alert alert-error" role="alert">Erro ao salvar, tente novamente!</div>');
				}

	}



	public function edit($id)
	{
		//recupera todos os dados do lote
		$lote  = LoteModel::find($id);
		$status = StatusModel::orderBy('dsc_status')->whereIn('cod_status', array(8, 9, 10,11))->get();
		$modalidades = ModalidadeModel::where('ind_ativo', '=', 'S')->orderBy('dsc_tpo_leilao')->get();
		$cidade = CidadeModel::where('cod_estado','=', '11')->orderBy('nome_cidade')->get();

		$data = [  'lote' => $lote , 'status' => $status , 'modalidades' => $modalidades , 'cidade' => $cidade   ];

		return \View::make('cliente.painel.EditarLote')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		//recupera foto do anuncio
		$lote  = LoteModel::find($id);

		$fileFoto = \Input::file('img_lote');
		

		if(empty($fileFoto)){
			$fileNameFoto = $lote->img_lote;
		}else {
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
				}

				$dados = [
								'titulo_lote' => \Input::get('titulo_lote'),
								'img_lote' => $fileNameFoto,
								'ind_exibir' => \Input::get('ind_exibir'),
								'dat_inicial_1' => \Input::get('dat_inicial_1'),
								'dat_final_1' => \Input::get('dat_final_1'),
								'dat_inicial_2' => \Input::get('dat_inicial_2'),
								'dat_final_2' => \Input::get('dat_final_2'),
								'dsc_lote' => \Input::get('dsc_lote'),
								'cod_tpo_leilao' => \Input::get('cod_tpo_leilao'),
								'url_video' => \Input::get('url_video'),
								'cod_status' => \Input::get('cod_status'),
								'credor_nome' => \Input::get('credor_nome'),
								'cidade' => \Input::get('cidade'),
								'credor_agencia' => \Input::get('credor_agencia'),
								'ind_exibir_praca' => \Input::get('ind_exibir_praca'),
								'dsc_local_lote' => \Input::get('dsc_local_lote'),
								'credor_conta' => \Input::get('credor_conta'),
								'ind_exibir_video' => \Input::get('ind_exibir_video')
								
							];

				$alterado = LoteModel::where('cod_lote',$id)->update($dados);

				if($alterado){
						return \Redirect::back()->with('mensagem',
						'<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>Lote alterado com sucesso!</div>');;
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			
			}

	


}
