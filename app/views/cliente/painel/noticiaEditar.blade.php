   @extends('cliente.layout')
   @section('conteudoPainel')

   <!--======= CONTEÚDO DA PÁGINA =========-->

     <div class="container">
       
      <div class="col-md-12">



{{ Form::open(['role' => 'form', 'files'=>true, 'class' => 'form-horizontal','method' => 'PUT', 'route' => ['noticia.update',$noticia->cod_noticia]]) }}
   <fieldset>

   <!-- Form Name -->
   <legend>Alterar dados da Noticia</legend>

    @if(Session::has('mensagem'))
      {{ Session::get('mensagem') }}
    @endif

   <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Titulo da Noticia</label>  
     <div class="col-md-5">
   {{ Form::text('nom_noticia', $noticia->nom_noticia,['class' => 'form-control input-md']) }} {{ $errors->first('nom_noticia') }}
     </div>
   </div>

   
   <!-- File Button --> 
   <div class="form-group">
     <label class="col-md-4 control-label" for="img_produto">Imagem</label>
     <div class="col-md-5">
     {{ Form::file('img_noticia', ['class' => 'input-file']) }}
     </div>
   </div>



    <!-- Select Basic -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="cod_forma_pag">Deseja Exibir</label>
     <div class="col-md-5">
 
 <select name="ind_ativo" class="form-control">
        @if($noticia->ind_ativo == "S") 
       <option value="S"> Sim </option>
       <option value="N"> Nao </option>
       @else
       <option value="N"> Nao </option>
       <option value="S"> Sim </option>
      @endif
  </select>

     </div>
   </div>


    <!-- Text input-->
   <div class="form-group">
     <label class="col-md-4 control-label" for="textinput">Data de início</label>  
     <div class="col-md-5">
      {{ Form::text('dat_cadastro', $noticia->dat_cadastro,['class' => 'form-control input-md datas']) }} {{ $errors->first('dat_cadastro') }}
     </div>
   </div>


   <!-- Textarea -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="dsc_lote">Informações</label>
     <div class="col-md-8">                     
        {{ Form::textarea('dsc_noticia', $noticia->dsc_noticia, ['class' => 'form-control','size' => '30x5']) }}
     </div>
   </div>

   <!-- Button -->
   <div class="form-group">
     <label class="col-md-4 control-label" for="salvar"></label>
     <div class="col-md-5">
        {{ Form::submit('Salvar alterações ',['class' => 'btn btn-primary']) }}
     </div>
   </div>


   </fieldset>
{{ Form::close() }}



      </div>
     </div>



<script>
  $( function() {
    $(".datas").datepicker({
            dateFormat: 'yy/mm/dd',
            dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
            dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
            dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
            monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
            monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
            nextText: 'Próximo',
            prevText: 'Anterior'
          });
  } );
  </script>

   @stop
