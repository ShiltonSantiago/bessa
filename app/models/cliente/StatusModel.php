<?php

namespace app\models\cliente;

class StatusModel extends \Eloquent{

	protected $table = 'tb_status';
	protected $guarded = [];
	protected $primaryKey = 'cod_status';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    
}

