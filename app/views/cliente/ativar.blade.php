@extends('cliente.layout')

@section('conteudoPainel')

<div class="container">
    <div class='row'>
      
      <br>

    @if($ativado == "S")
        
    <div class="alert alert-success" role="alert">Seu cadastro foi ativado com sucesso!</div>

    @else
    
     <div class="alert alert-error" role="alert">Entre em contato com conosco</div>
        
    @endif

  </div>
</div>
  

@stop
