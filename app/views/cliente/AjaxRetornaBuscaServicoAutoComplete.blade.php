<div class="panel panel-primary">
   <div class="panel-body">
      @if( empty(count($resultadoServicosHome)) )
      <i class="fa fa-frown-o" aria-hidden="true"></i> Serviço não encontrado, tente outro !
      @else
      @foreach($resultadoServicosHome as $resultadoServicos)
      <i class="fa fa-hand-paper-o" aria-hidden="true"></i>
      <a href="{{ URL('listar/categoria/'.$resultadoServicos->cod_servico.'')}}">{{ $resultadoServicos->nome_servico}}</a>
      <br>
      @endforeach
      @endif
   </div>
</div>


