<?php

namespace app\models\cliente;

class NoticiaModel extends \Eloquent{

	protected $table = 'tb_noticia';
	protected $guarded = [];
	protected $primaryKey = 'cod_noticia';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
    
}

