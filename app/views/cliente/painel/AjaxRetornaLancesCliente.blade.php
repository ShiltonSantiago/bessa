
<table class="table table-striped">
   <thead>
      <tr>
         <th><i class="fa fa-calendar" aria-hidden="true"></i> Data do Lance</th>
         <th>Valor do Lance</th>
         <th>Boleto de Arremate</th>
      </tr>
   </thead>
   <tbody>
   @foreach($lances as $lance)
      <tr>
        <td> {{ date('d/m/Y', strtotime($lance->created_at)) }} </td>
         <td>R$ {{ $lance->valor_lance}}</td>
         <td>
         	@if($lance->ind_arrematado == "S")
               Arrematado para o cliente
         	@else
         		Não Arrematado para o cliente
         	@endif
         </td>
      </tr>
@endforeach
   </tbody>
</table>