<?php

namespace app\models\cliente;

class LoteModel extends \Eloquent{

	protected $table = 'tb_lote';
	protected $guarded = [];
	protected $primaryKey = 'cod_lote';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }

	//relacionamentos produtos - 1 para muitos
	public function produtos(){
		return $this->hasMany('app\models\cliente\AnuncioModel', 'cod_lote', 'cod_lote');
	}

	//relacionamento modalidade - 1 para 1
	public function modalidade(){
		return $this->hasOne('app\models\cliente\ModalidadeModel', 'cod_tpo_leilao', 'cod_tpo_leilao');
	}

	//relacionamento status - 1 para 1
	public function status(){
		return $this->hasOne('app\models\cliente\StatusModel', 'cod_status', 'cod_status');
	}
}

