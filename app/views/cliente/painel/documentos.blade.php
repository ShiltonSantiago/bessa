@extends('cliente.layout')
@section('conteudoPainel')


<!--======= CONTEÚDO DA PÁGINA =========-->

   <div class="index-page">
      <div class="container">

        <table class="table table-striped" id="tabela_documentos">
                  <thead>
                     <tr>
                        <th>Cliente</th>
                        <th>Documento</th>
                        <th>Enviado em</th>
                        <th>Status</th>
                        <th>Data Status</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($clientes as $cliente)
                     <tr>
                        <td> <a href="#" data-cliente="{{ $cliente->cod_cliente }}" id="busca_dados_cliente" 
                              data-toggle="modal" data-target="#myModal"> {{ $cliente->nom_cliente }} </a> </td>
                        <td> 
                  @foreach($cliente->documentos as $doc)
                       <a href="{{ URL('public/documentos/'.$doc->dsc_documento.'')}}" target="_blank">
                                <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </a>
                   @endforeach
                        </td>

                     <td> {{ date('d/m/Y', strtotime( $doc->dat_envio )) }} </td> 

                    <td> 
                            @if ($cliente->ind_aceito_documento == "P")
                             <span class="label label-info">Aguardando aceite</span>
                          @elseif ($cliente->ind_aceito_documento == "N")
                              <span class="label label-danger">Documento irregular !</span>
                          @elseif ($cliente->ind_aceito_documento == "F")
                              <span class="label label-danger">Aguardando documento !</span>
                          @else
                              <span class="label label-success">Aprovado com sucesso !</span>
                          @endif
                         </td>

                         <td> 
                          @if (empty($cliente->dat_aprovacao_doc))
                            <a href="{{ URL('documentos/info/'.$cliente->cod_cliente.'/S')}}">
                            <i class="fa fa-thumbs-o-up" aria-hidden="true" data-documento="{{ $doc->cod_documento }}"></i> Aprovar</a> 
                            ou 
                            <a href="{{ URL('documentos/info/'.$cliente->cod_cliente.'/N')}}">
                            <i class="fa fa-thumbs-o-down" aria-hidden="true" data-documento="{{ $doc->cod_documento }}"></i> Reprovar</a>

                          @else
                              {{ date('d/m/Y', strtotime( $cliente->dat_aprovacao_doc )) }}
                          @endif
                        </td>


                     </tr>
                     @endforeach
                  </tbody>

               </table>

      </div>
   </div>





   <!-- Modal dados do cliente-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Dados Cliente</h4>
      </div>
      <div class="modal-body">
        
    <div id="retorna_dados_cliente"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>



   <!--======= FIM DO CONTEÚDO DA PÁGINA =========-->

@stop
