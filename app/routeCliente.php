<?php


Route::group(array('before' => 'filtro_menu'), function(){

//logoff cliente
		Route::get('sair','app\controllers\cliente\LoginClienteController@destroy');

//valida dados login
	    Route::resource('logar','app\controllers\cliente\LoginClienteController@getValidaLogin');

//esqueci senha
		Route::resource('/esqueciSenha', 'app\controllers\cliente\LoginClienteController@getEsqueci');

//Ajax busca home
		Route::get('busca/home/{valor}/','app\controllers\cliente\AjaxController@getListaServico');

//lista anuncios por categoria
		Route::get('listar/categoria/{valor}/','app\controllers\cliente\AnuncioController@getListAnuncioCategoria');


// listar leilao por lote
		Route::get('detalhes/lote/{valor}/','app\controllers\cliente\AnuncioController@getListaPorLote');

//salvar mensagem contato
		Route::get('/salvarContato','app\controllers\cliente\HomeController@getSalvarContato');

//busca cidades ajax
		Route::get('/carrega_cidades/{idEstado}','app\controllers\cliente\AjaxController@getCidades');

//busca servicos ajax
		Route::get('/carrega_servicos/{idCategoria}','app\controllers\cliente\AjaxController@getServicos');

//busca filtro avancado ajax
		Route::get('/buscarDadosAvancado/','app\controllers\cliente\AjaxController@getBuscaAvancada');

//atualizar valor do lance na pagina do item
		Route::get('/atualizaLance/','app\controllers\cliente\AjaxController@getLanceAtual');

//atualizar valor do lance na pagina do item usando o COMBO
		Route::get('/atualizaCombo/','app\controllers\cliente\AjaxController@getPossiveisLances');

//atualiza tabela que lista os lances via ajax
		Route::get('/atualizaTabelaLances/','app\controllers\cliente\AjaxController@getAjaxTabelaLances');

//listar detalhes anuncio completo
		Route::get('detalhes/anuncio/{valor}/','app\controllers\cliente\AnuncioController@getDetalhesAnuncio');

//listar detalhes anuncio para aparecer no telao/menos informações
		Route::get('detalhes/anuncio/telao/{valor}/','app\controllers\cliente\AnuncioController@getDetalhesAnuncioTelao');

//formulario de nota de venda
			Route::get('notavenda','app\controllers\cliente\AnuncioController@getNotaVenda');

//Gerar PDF de venda
			Route::get('nota_venda_pdf','app\controllers\cliente\AnuncioController@getNotaVendaPDF');

	 //Criar usuarios cliente 
		Route::resource('/cliente','app\controllers\cliente\UserController',['only' => ['edit','update','show', 'store'] ]);

//Index
		Route::resource('/', 'app\controllers\cliente\HomeController',['only' => ['index']]);

//contato
		Route::get('contato','app\controllers\cliente\InformacaoController@getContato');

//leiloeiro
		Route::get('leiloeiro','app\controllers\cliente\InformacaoController@getLeiloeiro');

// ativar cadastro
		Route::get('ativar/{id}','app\controllers\cliente\UserController@getAtivarCadastro');

//chama tela de nova conta
	    Route::get('conta/{id}','app\controllers\cliente\LoginClienteController@getConta');

 //noticia
		Route::get('noticias','app\controllers\cliente\InformacaoController@getNoticias');

// dicas
		Route::get('dicas','app\controllers\cliente\InformacaoController@getDicas');

//funciona
		Route::get('funciona','app\controllers\cliente\InformacaoController@getFunciona');

//regras
		Route::get('regras','app\controllers\cliente\InformacaoController@getRegras');

//seguranca
		Route::get('seguranca','app\controllers\cliente\InformacaoController@getSeguranca');

//duvidas
		Route::get('duvidas','app\controllers\cliente\InformacaoController@getDuvidas');


		Route::group(array('before' => 'auth'), function(){

	//buscar dados ajax do cliente
		Route::get('/buscarDadosCliente','app\controllers\cliente\AjaxController@getDadosCliente');

	//buscar todos os lances que um cliente deu pra um item
		Route::get('/buscarDadosLance','app\controllers\cliente\AjaxController@getDadosLances');

	//dar lance no bem
		Route::get('/darLance','app\controllers\cliente\AjaxController@getAplicarLance');

	//selecionar foto padrao do bem
		Route::get('selecionaFotoPadrao','app\controllers\cliente\FotoController@getFotoPadrao');


	//cadastrar bem
		Route::resource('/bens','app\controllers\cliente\AnuncioController',['only' => ['index','store','edit','update','show'] ]);

//cadastrar lote
		Route::resource('/lotes','app\controllers\cliente\LoteController',['only' => ['index','store','edit','update','show'] ]);
		
//cadastrar fotos
		Route::resource('/fotos','app\controllers\cliente\FotoController',['only' => ['store','edit','destroy'] ]);
		
//relatorios
		Route::resource('relatorio','app\controllers\cliente\RelatorioController',['only' => ['index','store','edit','destroy'] ]);

	//aceitar ou reprovar documentos
		Route::get('documentos/info/{id}/{status}','app\controllers\cliente\DocumentoController@getParecerDoc');

		Route::get('pdf','app\controllers\cliente\RelatorioController@getPDF');


//CRUD noticias
		Route::resource('/noticia','app\controllers\cliente\NoticiaController',['only' => ['index','edit','update','show', 'store','destroy'] ]);

//CRUD textos dos site
		Route::resource('texto','app\controllers\cliente\TextoController',['only' => ['index','edit','update','show', 'store','destroy'] ]);
		
//Documentos CRUD
		Route::resource('/documento','app\controllers\cliente\DocumentoController',['only' => ['index','store','edit','update','show'] ]);

//painel central  cliente
		Route::resource('/home','app\controllers\cliente\PainelClienteController',['only' => ['index'] ]);

//gerar boleto
		Route::get('gerar/boleto/{valor}/','app\controllers\cliente\AnuncioController@getGerarBoleto');

//gerar boleto com porcetagem
		Route::get('gerar/boleto2/{valor}/','app\controllers\cliente\AnuncioController@getGerarBoletoAlterado');

//adm validar os lances
		Route::get('lances/adm','app\controllers\cliente\AnuncioController@getValidarLances');

//busca lotes ajax
		Route::get('/carrega_lotes/{idLote}','app\controllers\cliente\AjaxController@getLotes');

//busca todos os lances de um bem ajax
		Route::get('carrega_lances','app\controllers\cliente\AjaxController@getLancesTotal');

//libera boleto ou confirma pagamento
		Route::get('confirmaFinal','app\controllers\cliente\AjaxController@getFinaliza');

//alterar senha do usuario 
		Route::resource('senha','app\controllers\cliente\PasswordClienteController');

//encerrar lances
		Route::get('/encerrar','app\controllers\cliente\AjaxController@getEncerrarLance');

//listar meus lances
		Route::get('lances/','app\controllers\cliente\AnuncioController@getListMeusLances');


	});

});




