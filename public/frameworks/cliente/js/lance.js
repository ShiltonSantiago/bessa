$(document).ready(function(){

function execAjax(params){
    var bolProgressbar = params.isProgressbar;
    if(params.isProgressbar){
            $( document ).ajaxSend(function() {
            // $('#carregando').modal('show', {
            //     keyboard: false
            // });
        });
    }

    $.ajax(params)
    .done(function(){
        // $( document ).ajaxStop(function() {
        //     $('#carregando').modal('hide');
        // });
    });
    
}
    
 $(document).on('click','#atualizar_val_lance',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var divLance = $("#val_lance_atual"); 
            var divLance2 = $("#val_lance_atual_2"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaLance",
                url : "../../atualizaLance",
                data : dataString,
                success : function(data){
                     divLance.html(data);
                     divLance2.html(data);
		     setTimeout('$("#atualizar_val_lance").trigger( "click" )', 5000);
                }
            });

    }),

 //combox lances
 $(document).on('click','#atualizar_val_lance',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var combo = $("#val_lance_select"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaCombo",
                url : "../../atualizaCombo",
                data : dataString,
                success : function(data){
                     var option = '<option value=""></option>';
                    $.each(data, function(i, obj){
                        //console.log(obj);
                  option += '<option value="'+obj.codigo+'">'+obj.preco+'</option>';
              })
                    combo.html(option).show();
                }
            });

    }),

 //atualiza tabela de lances na tela de detalhes via ajax
 $(document).on('click','#atualizar_val_lance',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var tabela = $("#tabela_lance_ajax"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaCombo",
                url : "../../atualizaTabelaLances",
                data : dataString,
                success : function(data){
                     tabela.html(data);
                }
            });

    }),
	
	//ajax para a pagina do telao
	
	 $(document).on('click','#atualizar_val_lance_telao',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var divLance = $("#val_lance_atual"); 
            var divLance2 = $("#val_lance_atual_2"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaLance",
                url : "../../../atualizaLance",
                data : dataString,
                success : function(data){
                     divLance.html(data);
                     divLance2.html(data);
		     setTimeout('$("#atualizar_val_lance_telao").trigger( "click" )', 5000);
                }
            });

    }),

 //combox lances
 $(document).on('click','#atualizar_val_lance_telao',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var combo = $("#val_lance_select"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaCombo",
                url : "../../../atualizaCombo",
                data : dataString,
                success : function(data){
                     var option = '<option value=""></option>';
                    $.each(data, function(i, obj){
                        //console.log(obj);
                  option += '<option value="'+obj.codigo+'">'+obj.preco+'</option>';
              })
                    combo.html(option).show();
                }
            });

    }),

 //atualiza tabela de lances na tela de detalhes via ajax
 $(document).on('click','#atualizar_val_lance_telao',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var tabela = $("#tabela_lance_ajax"); 
 
            var dataString = 'bem='+bem;

            execAjax({
                isProgressbar: false,
                //url : "atualizaCombo",
                url : "../../../atualizaTabelaLances",
                data : dataString,
                success : function(data){
                     tabela.html(data);
                }
            });

    }),

 //formulario do lance usando select
    $("#form_lance_select").submit(function(e){
            e.preventDefault();

            var valor = $("#val_lance_select option:selected").val();
            var cliente = $(this).attr('data-cliente');
            var bem = $(this).attr('data-produto');

            var dataString = 'valor_lance='+valor+'&cod_cliente='+cliente+'&cod_produto='+bem;
            
            execAjax({
                isProgressbar: false,
                url : "../../darLance",
                data : dataString,
                beforeSend: function() {
                       console.log("carregando");
                    },
                success: function(data) {

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;

                      if(retorno_processo == "success"){
                            location.reload();
                            alertify.notify(retorno_msg, 'success', 3);
                        }else{
                            alertify.notify(retorno_msg, 'error', 5);
                            }

                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function() {

                },
     });
            });

//formulario usando input
      $("#form_lance_input").submit(function(e){
            e.preventDefault();

            var valor = $("#val_lance_input").val();
            var cliente = $(this).attr('data-cliente');
            var bem = $(this).attr('data-produto');


            var dataString = 'valor_lance='+valor+'&cod_cliente='+cliente+'&cod_produto='+bem;
            
            execAjax({
                isProgressbar: false,
                url : "../../darLance",
                data : dataString,
                beforeSend: function() {
                       console.log("carregando");
                    },
                success: function(data) {

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;

                      if(retorno_processo == "success"){
                            location.reload();
                            alertify.notify(retorno_msg, 'success', 3);
                        }else{
                            alertify.notify(retorno_msg, 'error', 5);
                            }

                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function() {

                },
     });
            });

      $(document).on('click','.btn-ver-lance',function(){

            var bem = $(this).attr('data-bem');  
            var divLances = $("#retorna_dados_lance"); 

            var dataString = 'bem='+bem;
 
            execAjax({
                isProgressbar: false,
                url : "buscarDadosLance",
                data : dataString,
                success : function(data){
                     divLances.html(data)
                    
                }
            });

    });

      //listar lances de todos clientes
      $(document).on('change','#cod_produto',function(){

            var bem = $("#cod_produto").val(); 
            var divRetorno = $("#retorna"); 

            var dataString = 'bem='+bem;
 
            execAjax({
                isProgressbar: false,
                url : "../carrega_lances",
                data : dataString,
                success : function(data){
                     divRetorno.html(data)
                    
                }
            });

    });

      //liberar boleto ou confirmar pagamento
      $(document).on('click','.liberarAprovacao',function(){

            var codlance = $(this).attr('data-lance'); 
            var boleto = $(this).attr('data-pagou-boleto');
            
            var dataString = 'codlance='+codlance+'&boleto='+boleto;
 
            execAjax({
                isProgressbar: false,
                url : "../confirmaFinal",
                data : dataString,
                success : function(data){

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;

                      if(retorno_processo == "success"){
                            alertify.notify(retorno_msg, 'success', 5);
                             location.reload();
                        }else{
                            alertify.notify(retorno_msg, 'error', 5);
                            }
                    
                }
            });

    });

        $("#form_encerrar_lance").submit(function(e){
            e.preventDefault();

            var bem = $(this).attr('data-produto');

            var dataString = 'cod_produto='+bem;
            
            execAjax({
                isProgressbar: false,
                url : "../../encerrar",
                data : dataString,
                beforeSend: function() {
                       console.log("carregando");
                    },
                success: function(data) {

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;

                      if(retorno_processo == "success"){
                            location.reload();
                            alertify.notify(retorno_msg, 'success', 3);
                        }else{
                            alertify.notify(retorno_msg, 'error', 5);
                            }

                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function() {

                },
     });
            });

        //abrir proximo ite do lote
    $(document).on('click','.abre_proximo',function(){
            var $bem = $(this).attr('data-produto'); 
            var $url = 'http://bessaleiloes.com.br/bessa/public/detalhes/anuncio/'+$bem;
                location.href = $url;

    });

     $(document).on('click','#seleciona_img_padrao',function(e){
                e.preventDefault();

            var bem = $(this).attr('data-produto');  
            var foto = $(this).attr('data-foto'); 
 
            var dataString = 'bem='+bem+'&foto='+foto;

            execAjax({
                isProgressbar: false,
                //url : "selecionaFotoPadrao",
                url : "../../selecionaFotoPadrao",
                data : dataString,
                success : function(data){
                     retorno_msg = data.msg;
                     alertify.notify(retorno_msg, 'success', 6);
                     setTimeout(function(){
                                   window.location.reload();
                                 }, 200); 
                }
            });

    });


});










       
 




