<?php

namespace app\classes;

class Pagseguro {

    private $pagseguroConfig;
    private $nome;
    private $sobrenome;
    private $email;
    private $ddd;
    private $telefone;
    private $idReference;
    private $credentials;
    private $itemAdd=array();
    private $dados_cliente;


    public function __construct( $dadosCliente ){
        $this->dados_cliente = $dadosCliente;
        $this->pagseguroConfig = new \PagSeguroPaymentRequest();
        \PagSeguroLibrary::init();
    }

    private function dadosCompra(){

        $dadosCompra = $this->itemAdd;
        $this->pagseguroConfig->setSender(
            $this->nome . ' ' . $this->sobrenome, $this->email, $this->ddd, $this->telefone
        );

        $this->pagseguroConfig->setCurrency("BRL");
        $this->pagseguroConfig->setReference($this->idReference);
        $this->pagseguroConfig->setShippingAddress($this->dados_cliente->cep, $this->dados_cliente->rua, $this->dados_cliente->numero, '', $this->dados_cliente->bairro, $this->dados_cliente->cidade, strtoupper($this->dados_cliente->estado
        ));
        $this->pagseguroConfig->setShippingType(3);
        $this->pagseguroConfig->addItem($dadosCompra['id'], $dadosCompra['produto'],1, $dadosCompra['preco']);
    }

    public function enviarPagseguro(){
        $this->dadosCompra();
        $this->credentials = new \PagSeguroAccountCredentials(
            //'shilton.santiago@gmail.com', 'B221BFC18CAA468793F56FCABBBE7CFF'
            'financeiro@gvseminovos.com.br','63E1BA6AA50445A190E84D0AF9390E48'
        );

        $url = $this->pagseguroConfig->register($this->credentials);
        header('Location:'.$url);
        exit;
    }

    /**
     * Sets the value of nome.
     *
     * @param mixed $nome the nome
     *
     * @return self
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    public function setSobrenome($sobrenome)
    {
        $this->sobrenome = $sobrenome;

        return $this;
    }
    /**
     * Sets the value of email.
     *
     * @param mixed $email the email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setDdd($ddd)
    {
        $this->ddd = $ddd;

        return $this;
    }

    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;

        return $this;
    }

    public function setIdReference($idReference)
    {
        $this->idReference = $idReference;

        return $this;
    }

    public function setItemAdd($itemAdd)
    {
        $this->itemAdd = $itemAdd;

        return $this;
    }
}