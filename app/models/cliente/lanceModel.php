<?php

namespace app\models\cliente;

class lanceModel extends \Eloquent{

	protected $table = 'tb_lances';
	protected $guarded = [];
	protected $primaryKey = 'cod_lance';
	public $timestamps = true;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
	
		
	//cliente que deu lance
	public function dadoscliente() {
		return $this->hasOne('app\models\cliente\UserModel', 'cod_cliente', 'cod_cliente');
	}
}

