$(document).ready(function() {

    var wrapper = $("#wrapper");
    var row = wrapper.find(".row");
    var table_posts = row.find("#postTable");
    var tbody_lista_posts = table_posts.find("#listaPosts");
    var modalEditarPost = row.find("#modalEditarPost");
    var modal_body = modalEditarPost.find(".modal-body");
    var tituloTxt = modal_body.find("#tituloTxt");
    var tagsTxt = modal_body.find("#tagsTxt");
    var categoriasTxt = modal_body.find("#categoriasTxt");
    var slugTxt = modal_body.find("#slugTxt");
    var postTxt = modal_body.find("#postTxt");

   
   tbody_lista_posts.on('click','.btn-editar-post', function( event ){

        var id = $(this).attr('data-id');

        $.ajax({
            url: '/postJquery/'+id+'/edit',
            type: 'GET',
            dataType: 'json',
            success: function( data ){

                var countCategorias = data['categorias'].length;
                var select = '';
                var postCategoria = data['post'].tb_posts_categoria;
               
                tituloTxt.val( data['post'].tb_posts_titulo );
                tagsTxt.val( data['post'].tb_posts_tags );

                for( var i = 0; i < countCategorias; i++ ){
                   
                    var idcategoria = data['categorias'][i].id;
                    var selected = ( idcategoria == postCategoria ) ? 'selected="selected"' : '';

                   select += '<option value="'+idcategoria+'" '+selected+'>'+data['categorias'][i].tb_categorias_nome+'</option>';
                }

                categoriasTxt.append( select );

                modalEditarPost.modal({
                    show: 'true'
                });

            } 
        });

        event.preventDefault();
   });

});