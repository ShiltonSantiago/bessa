<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('/');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

//rota para carregar os combos de busca da home
Route::filter('filtro_menu', function()
{
		//combox home
		$cidades = \app\models\cliente\CidadeModel::orderBy('nome_cidade')->where('cod_estado', '=', '11')->get();
		$tipo = \app\models\cliente\TipoModel::where('ind_ativo', '=', 'S')->orderBy('dsc_tpo_leilao')->get();
		$categoria = \app\models\cliente\CategoriaModel::where('ind_ativo', '=', 'S')->orderBy('dsc_categoria')->get();
		$modalidades = \app\models\cliente\ModalidadeModel::where('ind_ativo', '=', 'S')->orderBy('dsc_tpo_leilao')->get();
		$noticias = \app\models\cliente\NoticiaModel::where('ind_ativo', '=', 'S')->orderBy('dat_cadastro', 'desc')->take(3)->get();
		
		
		//banners do site
		$anuncio1 = \app\models\cliente\TextoModel::where('cod_tpo_informacao', '=', '4')->take(1)->get();
		$anuncio2 = \app\models\cliente\TextoModel::where('cod_tpo_informacao', '=', '5')->take(1)->get();
		$anuncioHome = \app\models\cliente\TextoModel::where('cod_tpo_informacao', '=', '6')->take(1)->get();
		$anuncioRodape = \app\models\cliente\TextoModel::where('cod_tpo_informacao', '=', '7')->take(1)->get();
		$slides = \app\models\cliente\TextoModel::where('cod_tpo_informacao', '=', '12')->get();
		$ao_vivo = \app\models\cliente\LoteModel::where('ind_exibir_video', '=', 'S')->get();
		

		//leva pra sessao

		Session::set('ListaTipos', $tipo);
		Session::set('ListaCidades', $cidades);
		Session::set('ListaCategorias', $categoria);
		Session::set('ListaModalidades', $modalidades);
		Session::set('ListaNoticias', $noticias);
		
		Session::set('ListaAnuncio1', $anuncio1);
		Session::set('ListaAnuncio2', $anuncio2);
		Session::set('ListaAnuncioHome', $anuncioHome);
		Session::set('ListaAnuncioRodape', $anuncioRodape);
		Session::set('ListaSlides', $slides);
		Session::set('ListaAoVivos', $ao_vivo);
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
