<?php

namespace app\models\cliente;

class MultimidiaModel extends \Eloquent{

	protected $table = 'tb_multimidia';
	protected $guarded = [];
	protected $primaryKey = 'cod_multimidia';
	public $timestamps = false;

	public function __construct(array $attributes = [])
    {

    	if(\Auth::check()){
		    	//usuario logado
		        $this->setAttribute('cod_usuario_log', \Auth::user()->cod_cliente);
		        parent::__construct($attributes);
   		 }
    }
}

