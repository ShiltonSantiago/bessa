<?php

namespace app\acme;

class MessagesAcme{

	const REQUIRED = '<span class="text-danger">Esse campo é obrigatório</span>';
	const EMAIL = '<span class="text-danger">Digite um e-mail válido</span>';
	const UNIQUE = '<span class="text-danger">Já está sendo utilizado, informe outro</span>';
	const IMAGE = '<span class="text-danger">Extensão não aceita</span>';

	public static function Messages(){
		return [
			'required' => self::REQUIRED,
			'email' => self::EMAIL,
			'unique' => self::UNIQUE,
			'image' => self::IMAGE
		];
	}


}