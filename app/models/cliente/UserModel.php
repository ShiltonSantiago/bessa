<?php

namespace app\models\cliente;

class UserModel extends \Eloquent{

	protected $table = 'tb_cliente';
	protected $guarded = [];
	protected $primaryKey = 'cod_cliente';
	public $timestamps = true;

		//relacionamentos documentos - 1 para muitos
	public function documentos(){
		return $this->hasMany('app\models\cliente\DocumentoModel', 'cod_cliente', 'cod_cliente');
	}
}

