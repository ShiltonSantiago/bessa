<?php

namespace app\controllers\cliente;

use \app\models\cliente\NoticiaModel as NoticiaModel;
use \app\models\cliente\TextoModel as TextoModel;



class InformacaoController extends \BaseController {


	  public function getLeiloeiro()
    {
		$texto = TextoModel::where('cod_tpo_informacao', '=', '11')->get(); //leiloeiro

        $data  = [ 'texto' => $texto ];
		
        return \View::make('cliente.leiloeiro' ,$data);
    }


      public function getContato()
    {

        return \View::make('cliente.contato');
    }


      public function getRegras()
    {
		$texto = TextoModel::where('cod_tpo_informacao', '=', '3')->get(); //Regras

        $data  = [ 'texto' => $texto ];
        return \View::make('cliente.regras',$data);
    }

    public function getDicas()
    {
		$texto = TextoModel::where('cod_tpo_informacao', '=', '2')->get(); //Dicas

        $data  = [ 'texto' => $texto ];
		
        return \View::make('cliente.dicas',$data);
    }

      public function getNoticias()
    {
        $noticias = NoticiaModel::where('ind_ativo', '=', 'S')->orderBy('nom_noticia')->get();

        $data  = [ 'noticias' => $noticias ];

        return \View::make('cliente.noticias', $data);
    }

      public function getSeguranca()
    {
		$texto = TextoModel::where('cod_tpo_informacao', '=', '1')->get(); //Política de Privacidade

        $data  = [ 'texto' => $texto ];
		
        return \View::make('cliente.seguranca' ,$data);
    }


      public function getFunciona()
    {

        return \View::make('cliente.funciona');
    }



}















