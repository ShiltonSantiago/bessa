﻿
<!DOCTYPE html>
<html lang="pt_BR">
<head>
   <!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
   <!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
   <!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
   <!--[if (gte IE 9)|!(IE)]><!--><!--<![endif]-->
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Bessa Leilões</title>
   <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
   <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
   <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/css/lightbox.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/css/bootstrap.min.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/js/datetime/jquery.datetimepicker.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/style.css')}}">
   <link rel="stylesheet" type="text/css" href="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.css')}}">
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/external/jquery/jquery.js')}}"></script>
   <!-- <script src="https://code.jquery.com/jquery-3.1.0.js" integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk=" crossorigin="anonymous"></script> -->
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/modernizr.custom.29473.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/bootstrap.min.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/scripts.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cidades.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/login.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carrega_cep.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/busca_avancada.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/lance.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery-ui/jquery-ui.js')}}"></script>
   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/jquery.maskedinput.js')}}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/plugins/dataTables/jquery.dataTables.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/plugins/dataTables/dataTables.bootstrap.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaBens.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaLotes.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/tabelaTextos.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaDocumentos.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaRelatorio.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tabelaClientes.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/jquery.maskMoney.js') }}"></script>
   <script src="{{ URL::asset('frameworks/cliente/js/tinymce/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/tinyInit.js') }}"></script>
    <script src="{{ URL::asset('frameworks/cliente/js/datetime/jquery.datetimepicker.full.js') }}"></script>

   <script type="text/javascript">
      jQuery(function($){
       $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
       $("#num_telefone").mask("(99) 9999-9999");
       $("#num_celular").mask("(99) 99999-9999");
       $("#num_cep").mask('99999-999');
       $("#cpf").mask("999.999.999-99");
       $("#data").mask("99/99/9999");
       $("#cnpj").mask("99.999.999/9999-99");
    });
    </script>
  
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-83434705-1', 'auto');
  ga('send', 'pageview');

</script>
  
  <script type="text/javascript">
    function ligar_relogio(){
    UR_Nu = new Date;
    UR_Indhold = showFilled(UR_Nu.getHours()) + ":" + showFilled(UR_Nu.getMinutes()) + ":" + showFilled(UR_Nu.getSeconds());
    document.getElementById("relogio").innerHTML = UR_Indhold;
    setTimeout("ligar_relogio()",1000);
    }
    function showFilled(Value){return (Value > 9) ? "" + Value : "0" + Value;}
</script>
   
   <!-- JavaScript -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.7.1/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.min.css"/>

<!-- 
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.7.1/css/themes/bootstrap.rtl.min.css"/>
</head>
<body>



   <!--======= TOP BAR =========-->
    <div class="top-bar">
        <div class="container">
            <ul class="left-bar-side ul-conta">
                   <nav class="navbar nav-conta">
                    <div class="container">
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle menu-conta" data-toggle="collapse" data-target="#my1Navbar">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                      </div>
                      <div class="collapse navbar-collapse" id="my1Navbar">
                        <ul class="nav navbar-nav">   
            @include('cliente.menus')
                        </ul></div></div></nav>

            </ul>
            <ul class="hidden-xs right-bar-side social_icons">
                  <li id="relogio"><script>ligar_relogio();</script></li>
                  <li class="face"> <a href="https://www.facebook.com/bessaleiloes/" target="blank"><i class="fa fa-facebook"></i></a></li>
                  <li class="twit"> <a href="https://www.instagram.com/bessaleiloes/" target="blank"><i class="fa fa-instagram"></i></a></li>
         </ul>

        </div>
    </div>

   <!--======= HEADER =========-->
   <header>
      <div class="section1">
         <div class="container">

            <!--======= LOGO =========-->
            <div class="col-sm-4">
               <div class="row">
                  <div class="logo">
                           <a href="{{ URL('/')}}" title="logo">
                              <img src="{{ URL('frameworks/cliente/images/logo.png')}}"  alt="logo" />
                           </a>
                     </div>
                  </div>
               </div>


      <!--======= BANNER PUBLICIDADE HEADER =========-->        
              <!--======= SLIDER 1 =========-->
              <div class="col-sm-4 hidden-xs">
                    <div class="ad-header">
                  <div class="section2">
                    <div class="slide-principal">
                      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                          <div class="item active">
                            <img src="{{ URL('public/imagens/logo_mini_slide.jpg')}}" alt="Bessa">
                          </div>
                          <div class="item">
                         @foreach($anuncio1 = Session::get('ListaAnuncio1') as $anunc1)
                              <a href="{{$anunc1->link}}" target="blank">
                                <img src="{{ URL('public/imagens/'.$anunc1->img_texto.'')}}"  alt="" />
                              </a>
                        @endforeach
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    </div>
              </div>
        <!--======= SLIDER 2 =========-->
            <div class="col-sm-4 hidden-xs">
                <div class="ad-header">
                <div class="section2">
                  <div class="slide-principal">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <!-- Wrapper for slides -->
                      <div class="carousel-inner" role="listbox">
                        <div class="item active">
                          <img src="{{ URL('public/imagens/logo_mini_slide.jpg')}}" alt="Bessa">
                        </div>
                        <div class="item">
                          @foreach($anuncio2 = Session::get('ListaAnuncio2') as $anunc2)
                            <a href="{{$anunc2->link}}" target="blank">
                              <img src="{{ URL('public/imagens/'.$anunc2->img_texto.'')}}"  alt="" />
                            </a>
                         @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>

            <!--======= PESQUISA =========-->
            <div class="pesquisa-home col-xs-12">
              <div class="pesquisa-home-form ">
                  <form method="post" id="form_busca_avancada">
                    <div class="col-md-3">
                      <select class="form-control" name="cidade" id="cidade">
                        <option value="">Selecione a cidade</option>
                          @foreach($cidadesCombo = Session::get('ListaCidades') as $cidade)
                        <option value="{{ $cidade->cod_cidade}}">{{ $cidade->nome_cidade }}</option>
                            @endforeach
                      </select>
                    </div>
                    
                    <div class="col-md-2">
                      <select class="form-control" name="categoria" id="categoria">
                        <option value="">Categoria</option>
                          @foreach($categoriaCombo = Session::get('ListaCategorias') as $categ)
                        <option value="{{ $categ->cod_categoria}}"> {{ $categ->dsc_categoria}} </option>
                          @endforeach
                      </select>
                    </div>

                    <div class="col-md-2">
                      <select class="form-control" name="modalidade" id="modalidade">
                        <option value="">Modalidade</option>
                          @foreach( $modalidadesCombo = Session::get('ListaModalidades') as $modalidade)
                        <option value="{{ $modalidade->cod_tpo_leilao}}"> {{ $modalidade->dsc_tpo_leilao}} </option>
                          @endforeach
                      </select>
                    </div>


                    <div class="col-md-3">
                      <select class="form-control" name="tipo" id="tipo">
                        <option value="">Tipo</option>
                          @foreach( $tipoCombo = Session::get('ListaTipos') as $tipos)
                        <option value="{{ $tipos->cod_tpo_leilao }}">{{ $tipos->dsc_tpo_leilao }} </option>
                          @endforeach
                      </select>
                    </div>

                    <div class="col-md-1">
                     <input class="submit" type="submit" value="BUSCAR">
                    </div>
                    <div class="clearfix"></div>
                  </form>
              </div>
            </div>
          </div>  

      <!--======= NAV =========-->
      <nav class="navbar">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span> 
            </button>
          </div>
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
              <li><a href="{{ URL('/')}}">Home</a></li>
              <li><a href="{{ URL('funciona')}}">Como Participar</a></li>
               <li><a href="{{ URL('leiloeiro')}}">O Leiloeiro</a></li>
               <li><a href="{{ URL('noticias')}}">Notícias</a></li> 
               <li><a href="{{ URL('contato')}}">Fale Conosco</a></li> 
            </ul>
            <ul class="nav navbar-nav navbar-right">
               <li class="menu-ao-vivo" data-toggle="modal" data-target="#myModalAoVivo"><a href="#">Ao Vivo</a></li>
            </ul>
          </div>
        </div>
      </nav></div>
   </header>
   <!--======= FIM DO HEADER =========-->


  <!-- retorno do filtro avancado -->
  <div id="resultado_filtro_avancado"></div>


 <!--  pagina inicial que será removida no momento da busca avancada -->
  <div id="filtro_fixo">


  <div id="conteudo_principal">
            @yield('conteudoPainel')
     </div>  

   </div> <!-- fechando fixo -->

<!-- Modal Login -->

<div class="modal fade" id="myLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <center><img src="{{ URL('frameworks/cliente/images/logo.png')}}" alt=""><h4 class="modal-title" id="myModalLabel"></h4></center>
      </div>
      <div class="modal-body">

      <form method="post" id="form_login">
  <div class="form-group">
    <label for="exampleInputEmail1">E-mail</label>
    <input type="email" class="form-control" id="email" placeholder="">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Senha</label>
    <input type="password" class="form-control" id="senha" placeholder="">
  </div>
      <div style="margin:15px auto; text-align:right;"><a href="#">Esqueci minha senha.</a></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Limpar</button>
        <button type="submit" class="btn btn-primary">Entrar</button>
      </div>
      </form>
    </div>
  </div>
</div>


<!-- modal streawng video -->

 <div class="modal fade" id="myModalAoVivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Leilão Ao Vivo</h4>
      </div>
      <div class="modal-body">

	<table class="table">
	   <thead>
	      <tr>
	         <th>Nome</th>
	         <th>Logo</th>
	         <th></th>
	      </tr>
	   </thead>
	
	@foreach($Vivos = Session::get('ListaAoVivos') as $Vivo)
	   <tbody>
	      <tr>
	         <td>{{ $Vivo->titulo_lote}}</td>
	         <td><img class="center-block" src="{{ URL('public/imagens/'.$Vivo->img_lote.'')}}"  alt="" width="50"></td>
	         <td><a href="{{ URL('detalhes/lote/'.$Vivo->cod_lote.'')}}" name="botao-ok" id="botao-leilao-home">QueroParticipar</a></td>
	      </tr>
	   </tbody>
	@endforeach

	</table>



      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


   <!--======= FOOTER =========-->
   <footer>
      <div class="footer">
         <div class="container">
            <div class="col-sm-12 col-md-4"> 
               <div class="logo-footer">
                     <a href="{{ URL('/')}}" title="logo">
                        <img src="{{ URL('frameworks/cliente/images/logo-footer.png')}}" alt="logo" />
                     </a>
                     <h2>Leiloeiro Oficial Paulo Bessa</h2>
                  <h3>Telefone: 33 98852-7310   <br/>contato@bessaleiloes.com.br</h3>
                   </div>
               </div>
            <div class="col-sm-6 col-md-4">
               <div class="menu-footer">
                  <ul>
                     <li><a href="{{ URL('seguranca')}}">POLÍTICA DE PRIVACIDADE</a></li>
                     <li><a href="{{ URL('dicas')}}">DICAS</a></li>
                     <li><a href="{{ URL('regras')}}">REGRAS</a></li>
                  </ul>
               </div>
            </div>
            <div class="col-sm-6 col-md-4">
               <div class="news-footer">
                     <a href="#" target="blank">
					 @foreach($anuncioRodape = Session::get('ListaAnuncioRodape') as $anuncRodape)
						<a href="{{$anuncRodape->link}}"><img src="{{ URL('public/imagens/'.$anuncRodape->img_texto.'')}}" alt="" /></a>
                            @endforeach
  
                     </a>
               </div>   
            </div>

         </div>
      </div>
      <div class="assinatura">
         <center>COPYRIGHT © 2016 BESSA LEILÕES. TODOS OS DIREITOS RESERVADOS</a></center>
         <div class="icons-footer">
            <ul class="social-icons-footer">
                  <li class="face"> <a href="https://www.facebook.com/bessaleiloes/"><i class="fa fa-facebook"></i></a></li>
                  <li class="twit"> <a href="https://www.instagram.com/bessaleiloes/"><i class="fa fa-instagram"></i></a></li>
                  <li style="display: inline-block;position: relative; margin-left: 30px; top:-6px"><img src="{{ URL('frameworks/cliente/images/siteseguro.png')}}" width="120px" height="auto" alt="Site Seguro"></li>
            </ul>
         </div>
      </div>
   </footer>


<script type="text/javascript">
 $("#form_login").submit(function(e){
            e.preventDefault();

            var email = $("#email").val();
            var senha = $("#senha").val();
            var dataString = 'email='+email+'&senha='+senha;
            var urlconf = "{{ URL('logar')}}";
            
            $.ajax({
                type: "POST",
                url : urlconf,
                dataType: "json",
                data : dataString,
                beforeSend: function() {
                       //console.log("carregando");
                    },
                success: function(data) {

                    //retorna array do php pro data
                        retorno_msg = data.msg;
                        retorno_processo = data.processo;
                        url_retorno = "{{ URL('home')}}";

                        if(retorno_processo == "logado"){
                                alertify.notify(retorno_msg, 'success', 3);
                                setTimeout(function(){
                                  window.location.href = url_retorno;
                                 }, 10);              
                        }else{
                            alertify.notify(retorno_msg, 'error', 6);
                            }

                    },
                error: function(xhr) { // if error occured
                    alert("Error occured.please try again");
                    console.log(xhr.statusText + xhr.responseText);
                },

                complete: function() {

                },
     });
  });
</script>

   <script type="text/javascript" src="{{ URL('frameworks/cliente/js/carousel-swipe.js')}}"></script>
   <script>$("#carousel-example-generic").carousel({swipe:30});</script>

   <script src="{{ URL('frameworks/cliente/js/lightbox.js')}}"></script>
   <script>lightbox.option({
        'resizeDuration': 300,
        'wrapAround': true,
        'alwaysShowNavOnTouchDevices': true,
        'fadeDuration':300,
        'albumLabel': "Bessa Leilões | Foto %1 of %2"
      })
   </script>

   
</body>
</html>

