<?php

namespace app\controllers\cliente;
use \app\models\cliente\TextoModel as TextoModel;
use \app\models\cliente\TipoTextoModel as TipoTextoModel;


class TextoController extends \BaseController {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$textos = \DB::table('tb_informacao as info')
			    ->leftJoin('tb_tpo_informacao as tpo', 'tpo.cod_tpo_informacao', '=', 'info.cod_tpo_informacao')
     			->get();

     	$tiposSimples = TipoTextoModel::where("anexo","=","N")->get()->lists('dsc_tpo_informacao','cod_tpo_informacao');

     	$tiposUpload = TipoTextoModel::where("anexo","=","S")->get()->lists('dsc_tpo_informacao','cod_tpo_informacao');

		$data = [ 'textos' => $textos , 'tiposSimples' => $tiposSimples,'tiposUpload' => $tiposUpload ];

		return \View::make('cliente.painel.listaTextos')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	
		//recupera todos os dados
		
		$textos = \DB::table('tb_informacao as info')
			    ->leftJoin('tb_tpo_informacao as tpo', 'tpo.cod_tpo_informacao', '=', 'info.cod_tpo_informacao')
     			->where('info.cod_informacao','=',$id)
				->get();
		
		$tiposSimples = TipoTextoModel::where("anexo","=","N")->get();

     	$tiposUpload = TipoTextoModel::where("anexo","=","S")->get();

		$data = ['textos' => $textos , 'tiposSimples' => $tiposSimples, 'tiposUpload' => $tiposUpload  ];

		return \View::make('cliente.painel.textoEditar')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
				$texto = TextoModel::find($id);

				$fileFoto = \Input::file('img_texto');

				if(empty($fileFoto)){
			$fileNameFoto = $texto->img_texto;
				}else {
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
				}


				$attributes =  [   
					  		'dsc_informacao' => \Input::get('dsc_informacao'),
					  		'cod_tpo_informacao' => \Input::get('cod_tpo_informacao'),
					  		'link' => \Input::get('link'),
					  		'img_texto' => $fileNameFoto
					 ];

				$alterado = TextoModel::where('cod_informacao',$id)->update($attributes);

				if($alterado){
						return \Redirect::back()->with('mensagem','<div class="text text-success">Dados alterados com sucesso!</div>');
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			
	}

	public function store()
	{
			$fileFoto = \Input::file('img_texto');

			if(empty($fileFoto)){
						$fileNameFoto = ""; //inserir null
			}else{
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
			}

			$attributes =  [   
					  		'dsc_informacao' => \Input::get('dsc_informacao'),
					  		'cod_tpo_informacao' => \Input::get('cod_tpo_informacao'),
					  		'link' => \Input::get('link'),
					  		'img_texto' => $fileNameFoto
					 ];
			$salvarNovo = new TextoModel($attributes);
			$salvarNovo->save();

			if($salvarNovo){

				return \Redirect::back()->with('mensagem','<div class="text text-success">Texto cadastrado com sucesso!</div>');

		}else{
				return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao cadastrar, tente novamente!</div>');

		}


	}

	//remover texto
	public function destroy($id)
	{
	
		$deletado = TextoModel::find($id);
		$deletado->delete();
		
		if($deletado){

				return \Redirect::back()->with('mensagem','<div class="text text-success">Excluido com sucesso!</div>');

		}else{
				return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao excluir, tente novamente!</div>');

		}


	}
}
