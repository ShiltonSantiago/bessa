@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Listagem de Lotes</h1>

<hr>

<a href="{{ URL('lotes')}}"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>Cadastrar novo</a>

<hr>

<table class="table table-striped" id="tabela_lotes">
                  <thead>
                     <tr>
                        <th>Titulo</th>
                        <th>Data 1º Praça</th>
                        <th>Data 2º Praça</th>
                        <th>Imagem</th>
                        <th>Editar</th>
                        <th>Exibir</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($lotes as $lote)
                     <tr>
                        <td>{{ $lote->titulo_lote}}</td>
                        <td>
                         @if(!empty($lote->dat_inicial_1))
                           {{ date('d/m/Y - H:i', strtotime( $lote->dat_inicial_1 )) }}
                        @endif
                        </td>
                        <td>
                        @if(!empty($lote->dat_inicial_2))
                            {{ date('d/m/Y - H:i', strtotime( $lote->dat_inicial_2 )) }}
                        @endif
                        </td>
                        <td><img width="100" height="100" src="{{ URL('public/imagens/'.$lote->img_lote.'')}}"></td>
                        
                         <td>
			              <a href="{{ URL('lotes/'.$lote->cod_lote.'/edit')}}">
			              <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
			          	</td> 

                  <td>
                  @if( $lote->ind_exibir =='S')
                      Sim
                  @else
                      Não
                  @endif
                  </td>
                     </tr>
                     @endforeach
                  </tbody>
                   <tfoot>
            <tr>
               <td>{{ $links }}</td>
            </tr>
      </tfoot>
               </table>


   </div>
  </div>



  

@stop
