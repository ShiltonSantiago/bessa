<?php

namespace app\controllers\cliente;
use \app\models\cliente\DocumentoModel as DocumentoModel;
use \app\models\cliente\TipoDocumentoModel as TipoDocumentoModel;
use \app\models\cliente\UserModel as UserModel;


class PainelClienteController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tiposDocs = TipoDocumentoModel::where('ind_cobrar', '=', "S" )->orderBy('dsc_documento', 'asc')->get();

		$documento = DocumentoModel::where('cod_cliente', '=', \Auth::user()->cod_cliente )->orderBy('dat_envio', 'desc')->get();

        $data  = [ 'documento' => $documento , 'tiposDocs' => $tiposDocs  ];

		return \View::make('cliente.painel.home', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
