@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

  <div class="container page-full">
    
   <div class="col-md-12">
	
<h1>Listagem de bens</h1>

<hr>

<a href="{{ URL('bens')}}"><i class="fa fa-plus-circle fa-2x" aria-hidden="true"></i>Cadastrar novo</a>

<hr>

<table class="table table-striped">
                  <thead>
                     <tr>
                        <th>Codigo</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Imagem</th>
                        <th>Editar</th>
                        <th>Visualizar</th>
                     </tr>
                  </thead>

                  <tbody>
                     @foreach($anuncio as $anuncios)
                     <tr>
                        <td>{{ $anuncios->cod_produto}}</td>
                        <td>{{ $anuncios->nom_produto}}</td>
                        <td>{{ $anuncios->dsc_tpo_leilao}}</td>
                        <td><img width="100" height="100" src="{{ URL('public/imagens/'.$lote->img_lote.'')}}"></td>
                        
                        <td>
			              <a href="{{ URL('bens/'.$anuncios->cod_produto.'/edit')}}">
			              <i class="fa fa-pencil fa-2x" aria-hidden="true"></i></a>
			          	</td>
                        <td>
			              <a href="{{ URL('detalhes/anuncio/'.$anuncios->cod_produto.'')}}">
			              <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
			          	</td>
                     </tr>
                     @endforeach
                  </tbody>
                   <tfoot>
            <tr>
               <td>{{ $links }}</td>
            </tr>
      </tfoot>
               </table>


   </div>
  </div>



  

@stop
