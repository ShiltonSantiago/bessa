<?php

namespace app\controllers\cliente;
use \app\models\cliente\NoticiaModel as NoticiaModel;


class NoticiaController extends \BaseController {

//form de inserir nova noticia
	public function index()
	{

		return \View::make('cliente.painel.novo_noticia');

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		$noticias = NoticiaModel::orderBy('dat_cadastro', 'desc')->get();

		$data = [ 'noticias' => $noticias ];

		return \View::make('cliente.painel.listaNoticias')->with($data);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//recupera todos os dados
		$noticia = NoticiaModel::find($id);

		$data = ['noticia' => $noticia ];

		return \View::make('cliente.painel.noticiaEditar')->with($data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
				$noticia = NoticiaModel::find($id);

				$fileFoto = \Input::file('img_noticia');

				if(empty($fileFoto)){
			$fileNameFoto = $noticia->img_noticia;
				}else {
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
				}


				$attributes =  [   
					  		'nom_noticia' => \Input::get('nom_noticia'),
					  		'dsc_noticia' => \Input::get('dsc_noticia'),
					  		'ind_ativo' => \Input::get('ind_ativo'),
					  		'dat_cadastro' => \Input::get('dat_cadastro'),
					  		'img_noticia' => $fileNameFoto
					 ];

				$alterado = NoticiaModel::where('cod_noticia',$id)->update($attributes);

				if($alterado){
						return \Redirect::back()->with('mensagem','<div class="text text-success">Dados alterados com sucesso!</div>');
				}else{
				        return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao alterar, tente novamente!</div>');
				}

			
	}

	public function store()
	{

			$fileFoto = \Input::file('img_noticia');

			if(empty($fileFoto)){
						$fileNameFoto = ""; //inserir null
			}else{
					$destinationPath = 'public/imagens';
					$extensoes_permitidas = array('jpg', 'PNG', 'gif', 'bmp');
					$extension = $fileFoto->getClientOriginalExtension();
					$fileNameFoto = rand(1111,9999).'.'.$extension;			
					$fileFoto->move($destinationPath, $fileNameFoto);
			}

			$attributes =  [   
					  		'nom_noticia' => \Input::get('nom_noticia'),
					  		'dsc_noticia' => \Input::get('dsc_noticia'),
					  		'ind_ativo' => \Input::get('ind_ativo'),
					  		'dat_cadastro' => \Input::get('dat_cadastro'),
					  		'img_noticia' => $fileNameFoto
					 ];
			$salvarNovo = new NoticiaModel($attributes);
			$salvarNovo->save();

			if($salvarNovo){

				return \Redirect::back()->with('mensagem','<div class="text text-success">Noticia cadastrada com sucesso!</div>');

		}else{
				return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao cadastrar, tente novamente!</div>');

		}


	}

	//remover noticia
	public function destroy($id)
	{
		
	
		$deletado = NoticiaModel::find($id);
		$deletado->delete();
		
		if($deletado){

				return \Redirect::back()->with('mensagem','<div class="text text-success">Excluido com sucesso!</div>');

		}else{
				return \Redirect::back()->with('mensagem','<div class="text text-danger">Ocorreu um erro ao excluir, tente novamente!</div>');

		}


	}
}
