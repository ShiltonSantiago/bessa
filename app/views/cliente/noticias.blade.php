@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

	<div class="index-page">
		<div class="container">
			<div class="clearfix"></div>
			<div class="row todas-noticias">



<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

@foreach($noticias as $noticia)
<div class="col-md-4 col-sm-6 noticia-page-single">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#{{ $noticia->cod_noticia}}" aria-expanded="false" aria-controls="{{ $noticia->cod_noticia}}">
        <div class="imagem-noticia" id="imagem-noticia">
								<img src="{{ URL('public/imagens/'.$noticia->img_noticia.'')}}" alt="">
								<div class="noticia-hover">
									<i id="flutuar-not" class="fa fa-plus-square-o" aria-hidden="true"></i>
								</div>
							</div>
              <h3>
          {{ $noticia->nom_noticia}}
          </h3>
        </a>
      </h4>
    </div>
    <div id="{{ $noticia->cod_noticia}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
      <p> {{ $noticia->dsc_noticia}} </p>
      </div>
    </div>
  </div>
</div>
@endforeach
</div>

			</div>
		</div>
	</div>

	<!--======= FIM DO CONTEÚDO DA PÁGINA =========-->


@stop
