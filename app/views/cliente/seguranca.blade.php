@extends('cliente.layout')
@section('conteudoPainel')

<!--======= CONTEÚDO DA PÁGINA =========-->

	<div class="index-page">
		<div class="container">
			<div class="clearfix"></div>
			<div class="como-participar">

				<div class="img-participar">
					<img src="{{ URL('frameworks/cliente/images/logo-icon.png')}}" alt="">
				</div>
				<div class="menu-participar">
					<h2>POLÍTICA DE PRIVACIDADE</h2>
				</div>

				<div class="col-sm-10 col-sm-offset-1"> <hr>
					<div id="o-que-e">
					
					{{ $texto[0]->dsc_informacao}}
	
						
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>

	<!--======= FIM DO CONTEÚDO DA PÁGINA =========-->

@stop


